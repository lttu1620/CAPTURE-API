-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

ALTER TABLE `news_feeds`
	ADD COLUMN `image_url` VARCHAR(255) NULL COMMENT '画像URL' AFTER `comments_count`,
	ADD COLUMN `distribution_date` INT(11) NULL COMMENT '配信日' AFTER `image_url`,
	ADD COLUMN `favorite_count` INT(11) NULL COMMENT 'お気に入り数' AFTER `distribution_date`,
	CHANGE COLUMN `created` `created` INT(11) NOT NULL COMMENT '登録日時' AFTER `disable`,
	CHANGE COLUMN `updated` `updated` INT(11) NOT NULL COMMENT '更新日時' AFTER `created`;
ALTER TABLE `news_comments`
	ADD COLUMN `like_count` INT(11) NOT NULL DEFAULT '0' COMMENT 'いいね数' AFTER `comment_short`;

-- Dumping structure for table capture.news_feed_favorites
DROP TABLE IF EXISTS `news_feed_favorites`;
CREATE TABLE IF NOT EXISTS `news_feed_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '登録日時',
  `updated` int(11) DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='記事のお気に入り';

-- Dumping data for table capture.news_feed_favorites: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_feed_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_feed_favorites` ENABLE KEYS */;


-- Dumping structure for table capture.news_feed_reads
DROP TABLE IF EXISTS `news_feed_reads`;
CREATE TABLE IF NOT EXISTS `news_feed_reads` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) unsigned NOT NULL COMMENT '登録日時',
  `updated` int(11) unsigned NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='記事既読ログ';

-- Dumping data for table capture.news_feed_reads: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_feed_reads` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_feed_reads` ENABLE KEYS */;


-- Dumping structure for trigger capture.before_insert_news_feed_favorites
DROP TRIGGER IF EXISTS `before_insert_news_feed_favorites`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `before_insert_news_feed_favorites` BEFORE INSERT ON `news_feed_favorites` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_news_feed_reads
DROP TRIGGER IF EXISTS `before_insert_news_feed_reads`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `before_insert_news_feed_reads` BEFORE INSERT ON `news_feed_reads` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_news_feed_favorites
DROP TRIGGER IF EXISTS `before_update_news_feed_favorites`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `before_update_news_feed_favorites` BEFORE UPDATE ON `news_feed_favorites` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_news_feed_reads
DROP TRIGGER IF EXISTS `before_update_news_feed_reads`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
DELIMITER //
CREATE TRIGGER `before_update_news_feed_reads` BEFORE UPDATE ON `news_feed_reads` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
