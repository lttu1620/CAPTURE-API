-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

ALTER TABLE `companies`
	CHANGE COLUMN `background_movie` `introduction_movie` TEXT NULL COMMENT '動画URL' AFTER `thumbnail_img`;
ALTER TABLE `news_comments`
	ADD COLUMN `company_id` INT(11) NULL COMMENT '企業ID' AFTER `user_id`;
ALTER TABLE `companies`
	ADD COLUMN `background_img` VARCHAR(100) NULL DEFAULT NULL COMMENT '背景画像' AFTER `thumbnail_img`;
ALTER TABLE `login_logs`
	ALTER `news_feed_id` DROP DEFAULT;
ALTER TABLE `login_logs`
	CHANGE COLUMN `news_feed_id` `news_feed_id` INT(11) NULL COMMENT 'ニュースフィードID' AFTER `user_id`,
	ADD COLUMN `login_device` INT(2) NULL COMMENT 'デバイス' AFTER `share_type`;


-- Dumping structure for table capture.help_contents
CREATE TABLE IF NOT EXISTS `help_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'コンテンツID',
  `title` varchar(40) NOT NULL COMMENT 'ヘルプページ名',
  `description` varchar(255) DEFAULT NULL COMMENT '概要',
  `body` text COMMENT 'helpページのコンテンツ',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '有効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '作成日',
  `updated` int(11) DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ヘルプページ';

-- Dumping data for table capture.help_contents: ~0 rows (approximately)
/*!40000 ALTER TABLE `help_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `help_contents` ENABLE KEYS */;


-- Dumping structure for table capture.news_comment_category
CREATE TABLE IF NOT EXISTS `news_comment_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `category_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `news_comment_id` int(11) NOT NULL COMMENT 'コメントID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '登録日時',
  `updated` int(11) DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースカテゴリー';

-- Dumping data for table capture.news_comment_category: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_comment_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_comment_category` ENABLE KEYS */;


-- Dumping structure for table capture.news_comment_subcategory
CREATE TABLE IF NOT EXISTS `news_comment_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `subcategory_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `news_comment_id` int(11) NOT NULL COMMENT 'コメントID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '登録日時',
  `updated` int(11) DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースサブカテゴリー';

-- Dumping data for table capture.news_comment_subcategory: ~0 rows (approximately)
/*!40000 ALTER TABLE `news_comment_subcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_comment_subcategory` ENABLE KEYS */;


-- Dumping structure for trigger capture.before_insert_help_contents
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_help_contents` BEFORE INSERT ON `help_contents` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_news_comment_category
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_news_comment_category` BEFORE INSERT ON `news_comment_category` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_news_comment_subcategory
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_news_comment_subcategory` BEFORE INSERT ON `news_comment_subcategory` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_help_contents
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_help_contents` BEFORE UPDATE ON `help_contents` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_news_comment_category
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_news_comment_category` BEFORE UPDATE ON `news_comment_category` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_news_comment_subcategory
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_news_comment_subcategory` BEFORE UPDATE ON `news_comment_subcategory` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
