-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

ALTER TABLE `news_feed_categories`
	ALTER `news_comment_id` DROP DEFAULT;
ALTER TABLE `news_feed_categories`
	CHANGE COLUMN `news_comment_id` `news_feed_id` INT(11) NOT NULL COMMENT 'コメントID' AFTER `category_id`;
ALTER TABLE `news_feed_subcategories`
	ALTER `news_comment_id` DROP DEFAULT;
ALTER TABLE `news_feed_subcategories`
	CHANGE COLUMN `news_comment_id` `news_feed_id` INT(11) NOT NULL COMMENT 'コメントID' AFTER `subcategory_id`;
ALTER TABLE `users`
	ADD COLUMN `deparment_id` INT(11) NULL DEFAULT NULL AFTER `university_id`;


-- Dumping structure for table capture.company_categories
CREATE TABLE IF NOT EXISTS `company_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `company_id` int(11) NOT NULL COMMENT '企業ID',
  `category_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '登録日時',
  `updated` int(11) DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='会社業界';

-- Dumping data for table capture.company_categories: ~3 rows (approximately)
/*!40000 ALTER TABLE `company_categories` DISABLE KEYS */;
INSERT INTO `company_categories` (`id`, `company_id`, `category_id`, `disable`, `created`, `updated`) VALUES
	(1, 1, 1, 0, 1418270555, 1418270555),
	(2, 2, 2, 0, 1418270975, 1418270975),
	(3, 2, 1, 1, 1418272256, 1418281126);
/*!40000 ALTER TABLE `company_categories` ENABLE KEYS */;


-- Dumping structure for table capture.company_subcategories
CREATE TABLE IF NOT EXISTS `company_subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `company_id` int(11) NOT NULL COMMENT '企業ID',
  `subcategory_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) DEFAULT NULL COMMENT '登録日時',
  `updated` int(11) DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社サブ業界';

-- Dumping data for table capture.company_subcategories: ~0 rows (approximately)
/*!40000 ALTER TABLE `company_subcategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_subcategories` ENABLE KEYS */;


-- Dumping structure for table capture.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'コンテンツID',
  `name` varchar(40) DEFAULT NULL COMMENT '名前',
  `email` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `tel` varchar(40) DEFAULT NULL COMMENT '電話番号',
  `address` text COMMENT '住所',
  `website` text COMMENT 'webサイトURL',
  `subject` varchar(200) DEFAULT NULL COMMENT 'タイトル',
  `content` text COMMENT '内容',
  `created` int(11) DEFAULT NULL COMMENT '作成日',
  `updated` int(11) DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='問い合わせテーブル';

-- Dumping data for table capture.contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;


-- Dumping structure for trigger capture.before_insert_company_categories
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_company_categories` BEFORE INSERT ON `company_categories` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_company_subcategories
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_company_subcategories` BEFORE INSERT ON `company_subcategories` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_contacts
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_contacts` BEFORE INSERT ON `contacts` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_company_categories
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_company_categories` BEFORE UPDATE ON `company_categories` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_company_subcategories
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_company_subcategories` BEFORE UPDATE ON `company_subcategories` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_contacts
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_contacts` BEFORE UPDATE ON `contacts` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
