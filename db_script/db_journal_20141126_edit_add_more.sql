-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

ALTER TABLE `user_recruiters`
	ADD COLUMN `is_admin` TINYINT(1) NOT NULL DEFAULT '0' AFTER `thumbnail_img`,
	ADD COLUMN `is_approved` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_admin`;



-- Dumping structure for table capture.preset_comments
DROP TABLE IF EXISTS `preset_comments`;
CREATE TABLE IF NOT EXISTS `preset_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'カテゴリID',
  `comments` varchar(40) NOT NULL COMMENT 'コメント名',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='コメントプリセット';

-- Dumping data for table capture.preset_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `preset_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `preset_comments` ENABLE KEYS */;


-- Dumping structure for table capture.user_activations
DROP TABLE IF EXISTS `user_activations`;
CREATE TABLE IF NOT EXISTS `user_activations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT '`users`テーブル id',
  `email` varchar(255) NOT NULL COMMENT 'メールアドレス',
  `password` varchar(40) NOT NULL COMMENT 'パスワード',
  `token` varchar(40) NOT NULL COMMENT 'トークン',
  `expire_date` int(11) NOT NULL COMMENT 'トークンの期限',
  `regist_type` varchar(20) DEFAULT NULL COMMENT '登録タイプ',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '有効フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='メールアクティベーション';

-- Dumping data for table capture.user_activations: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_activations` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_activations` ENABLE KEYS */;


-- Dumping structure for trigger capture.before_insert_preset_comments
DROP TRIGGER IF EXISTS `before_insert_preset_comments`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_preset_comments` BEFORE INSERT ON `preset_comments` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_user_activations
DROP TRIGGER IF EXISTS `before_insert_user_activations`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_user_activations` BEFORE INSERT ON `user_activations` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_preset_comments
DROP TRIGGER IF EXISTS `before_update_preset_comments`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_preset_comments` BEFORE UPDATE ON `preset_comments` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_user_activations
DROP TRIGGER IF EXISTS `before_update_user_activations`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_user_activations` BEFORE UPDATE ON `user_activations` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
