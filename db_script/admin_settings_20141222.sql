/*
Navicat MySQL Data Transfer

Source Server         : my_db
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : Capture

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2014-12-22 13:34:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_settings`
-- ----------------------------
DROP TABLE IF EXISTS `admin_settings`;
CREATE TABLE `admin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `disable` tinyint(1) DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of admin_settings
-- ----------------------------
DROP TRIGGER IF EXISTS `before_insert_user_settings_copy`;
DELIMITER ;;
CREATE TRIGGER `before_insert_user_settings_copy` BEFORE INSERT ON `admin_settings` FOR EACH ROW SET 
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `before_update_user_settings_copy`;
DELIMITER ;;
CREATE TRIGGER `before_update_user_settings_copy` BEFORE UPDATE ON `admin_settings` FOR EACH ROW SET 
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
