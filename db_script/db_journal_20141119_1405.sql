-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table capture.admins
DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理者ID',
  `name` varchar(40) NOT NULL COMMENT '管理者名',
  `login_id` varchar(40) NOT NULL COMMENT 'ログインID',
  `password` varchar(40) NOT NULL COMMENT 'パスワード',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザテーブル';

-- Data exporting was unselected.


-- Dumping structure for table capture.campuses
DROP TABLE IF EXISTS `campuses`;
CREATE TABLE IF NOT EXISTS `campuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'キャンパスID',
  `university_id` int(11) NOT NULL COMMENT '大学ID',
  `name` varchar(40) NOT NULL COMMENT 'キャンパス名',
  `address` text COMMENT '住所',
  `access_method` text COMMENT 'アクセス方法',
  `prefecture_id` int(2) DEFAULT NULL COMMENT '都道府県ID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='キャンパスマスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'カテゴリID',
  `name` varchar(40) NOT NULL COMMENT 'カテゴリ名',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カテゴリーテーブル';

-- Data exporting was unselected.


-- Dumping structure for table capture.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '会社ID',
  `name` varchar(120) NOT NULL COMMENT '会社名',
  `kana` varchar(255) DEFAULT NULL COMMENT '会社名かな',
  `address` text COMMENT '会社住所',
  `description_short` varchar(255) DEFAULT NULL COMMENT '会社説明文-短い',
  `description` text COMMENT '会社説明文',
  `thumbnail_img` varchar(100) DEFAULT NULL COMMENT 'サムネイル画像',
  `background_movie` text COMMENT '動画URL',
  `corporate_url` text COMMENT '企業サイトURL',
  `corporate_facebook` varchar(100) DEFAULT NULL COMMENT '企業Facebookアカウント',
  `corporate_twitter` varchar(100) DEFAULT NULL COMMENT '企業Twitterアカウント',
  `corporate_entry` text COMMENT 'エントリーページ',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社マスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.company_user_view_logs
DROP TABLE IF EXISTS `company_user_view_logs`;
CREATE TABLE IF NOT EXISTS `company_user_view_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体 「0=アプリ通常起動,1=facebook,2=twitter,3=line,4=tadacopy,5=push通知」',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社担当者ページビューログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.company_view_logs
DROP TABLE IF EXISTS `company_view_logs`;
CREATE TABLE IF NOT EXISTS `company_view_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体 「0=アプリ通常起動,1=facebook,2=twitter,3=line,4=tadacopy,5=push通知」',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社ビューログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学部ID',
  `university_id` int(11) NOT NULL COMMENT '大学ID',
  `name` varchar(80) NOT NULL COMMENT '学部名',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学部マスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_categories
DROP TABLE IF EXISTS `follow_categories`;
CREATE TABLE IF NOT EXISTS `follow_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `category_id` int(11) NOT NULL COMMENT 'カテゴリーサイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カテゴリーフォロー';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_category_logs
DROP TABLE IF EXISTS `follow_category_logs`;
CREATE TABLE IF NOT EXISTS `follow_category_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `category_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `unfollow` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アンフォローフラグ　「」0=follow,1=unfollow',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='フォローログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_companies
DROP TABLE IF EXISTS `follow_companies`;
CREATE TABLE IF NOT EXISTS `follow_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `company_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社follow';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_company_logs
DROP TABLE IF EXISTS `follow_company_logs`;
CREATE TABLE IF NOT EXISTS `follow_company_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `company_id` int(11) NOT NULL COMMENT '会社ID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `unfollow` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'アンフォローフラグ　「0=follow,1=unfollow　」',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='フォローログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_subcategories
DROP TABLE IF EXISTS `follow_subcategories`;
CREATE TABLE IF NOT EXISTS `follow_subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `subcategory_id` int(11) NOT NULL COMMENT 'サブカテゴリーID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  `disable` int(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カテゴリーフォロー';

-- Data exporting was unselected.


-- Dumping structure for table capture.follow_subcategory_logs
DROP TABLE IF EXISTS `follow_subcategory_logs`;
CREATE TABLE IF NOT EXISTS `follow_subcategory_logs` (
  `id` int(11) NOT NULL COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `subcategory_id` int(11) NOT NULL COMMENT 'サブカテゴリーID',
  `unfollow` tinyint(1) NOT NULL COMMENT 'アンフォローフラグ　「」0=follow,1=unfollow',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='サブカテゴリーfollowログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.login_logs
DROP TABLE IF EXISTS `login_logs`;
CREATE TABLE IF NOT EXISTS `login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体　「0=アプリ通常起動,1=facebook,2=twitter,3=line,4=tadacopy,5=push通知」',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ログインログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_comments
DROP TABLE IF EXISTS `news_comments`;
CREATE TABLE IF NOT EXISTS `news_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `comment` text NOT NULL COMMENT 'コンテンツ',
  `comment_short` varchar(255) NOT NULL COMMENT 'コンテンツショート',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースコメントマスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_comment_likes
DROP TABLE IF EXISTS `news_comment_likes`;
CREATE TABLE IF NOT EXISTS `news_comment_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) DEFAULT NULL COMMENT 'ニュースフィードID',
  `news_comment_id` int(11) DEFAULT NULL COMMENT 'ニュースコメントID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースいいね';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_contents
DROP TABLE IF EXISTS `news_contents`;
CREATE TABLE IF NOT EXISTS `news_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `content` text NOT NULL COMMENT 'コンテンツ',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースコンテンツマスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_content_view_logs
DROP TABLE IF EXISTS `news_content_view_logs`;
CREATE TABLE IF NOT EXISTS `news_content_view_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体 「0=アプリ通常起動,1=facebook,2=twitter,3=line,4=tadacopy,5=push通知」',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースディテールビューログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_feeds
DROP TABLE IF EXISTS `news_feeds`;
CREATE TABLE IF NOT EXISTS `news_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `title` varchar(255) NOT NULL COMMENT 'タイトル',
  `short_content` varchar(255) NOT NULL COMMENT 'コンテンツ',
  `detail_url` text NOT NULL COMMENT '記事URL',
  `likes_count` int(11) NOT NULL DEFAULT '0' COMMENT 'いいね数',
  `comments_count` int(11) NOT NULL DEFAULT '0' COMMENT 'コメント数',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースニュードマスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_feed_likes
DROP TABLE IF EXISTS `news_feed_likes`;
CREATE TABLE IF NOT EXISTS `news_feed_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `news_site_id` int(11) NOT NULL COMMENT 'ニュースサイトID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースいいね';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_feed_view_logs
DROP TABLE IF EXISTS `news_feed_view_logs`;
CREATE TABLE IF NOT EXISTS `news_feed_view_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体 「0=アプリ通常起動,1=facebook,2=twitter,3=line,4=tadacopy,5=push通知」',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ログインログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.news_sites
DROP TABLE IF EXISTS `news_sites`;
CREATE TABLE IF NOT EXISTS `news_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `name` varchar(255) NOT NULL COMMENT 'サイト名',
  `introduction_text` varchar(255) DEFAULT NULL COMMENT 'サイト紹介文',
  `thumbnail_img` varchar(255) DEFAULT NULL COMMENT 'サムネイル画像',
  `feed_url` text COMMENT 'サイトfeedURL',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ニュースサイトマスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.push_messages
DROP TABLE IF EXISTS `push_messages`;
CREATE TABLE IF NOT EXISTS `push_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `company_id` int(11) NOT NULL COMMENT '会社ID',
  `message` varchar(40) NOT NULL COMMENT 'メッセージ',
  `android_icon` varchar(10) DEFAULT NULL COMMENT 'アイコン（Android）',
  `ios_sound` int(1) DEFAULT '0' COMMENT 'サウンド（iOS）',
  `send_reservation_date` int(11) DEFAULT NULL COMMENT '送信予定日時',
  `rich_url` varchar(255) DEFAULT NULL COMMENT 'RichUrl',
  `is_sent` tinyint(1) NOT NULL DEFAULT '0' COMMENT '送信済みフラグ',
  `sent_date` int(11) DEFAULT NULL COMMENT '送信日',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='プッシュメッセージmst';

-- Data exporting was unselected.


-- Dumping structure for table capture.push_message_open_logs
DROP TABLE IF EXISTS `push_message_open_logs`;
CREATE TABLE IF NOT EXISTS `push_message_open_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `message_id` int(11) NOT NULL COMMENT 'メッセージID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='プッシュメッセージ開封ログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.push_message_send_logs
DROP TABLE IF EXISTS `push_message_send_logs`;
CREATE TABLE IF NOT EXISTS `push_message_send_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `message_id` int(11) NOT NULL COMMENT 'メッセージID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='プッシュメッセージ送信ログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.share_logs
DROP TABLE IF EXISTS `share_logs`;
CREATE TABLE IF NOT EXISTS `share_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サイトID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `news_feed_id` int(11) NOT NULL COMMENT 'ニュースフィードID',
  `share_type` int(2) NOT NULL DEFAULT '0' COMMENT 'シェア媒体　「」1=facebook,2=twitter,3=line,5=push通知',
  `created` int(11) NOT NULL COMMENT '登録日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='シェアログ';

-- Data exporting was unselected.


-- Dumping structure for table capture.subcategories
DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'サブカテゴリID',
  `category_id` int(11) NOT NULL COMMENT 'カテゴリーID',
  `name` varchar(40) NOT NULL COMMENT 'カテゴリ名',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='サブカテゴリテーブル';

-- Data exporting was unselected.


-- Dumping structure for table capture.universities
DROP TABLE IF EXISTS `universities`;
CREATE TABLE IF NOT EXISTS `universities` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '大学ID',
  `name` varchar(45) NOT NULL COMMENT '大学名',
  `kana` varchar(90) NOT NULL COMMENT '大学名（かな）',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '登録日時',
  `updated` int(11) NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大学マスタ';

-- Data exporting was unselected.


-- Dumping structure for table capture.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' ユーザID',
  `app_id` varchar(13) NOT NULL COMMENT '一般公開ユーザID',
  `name` varchar(40) NOT NULL COMMENT '名前',
  `nickname` varchar(40) DEFAULT NULL COMMENT 'ニックネーム',
  `university` int(11) DEFAULT NULL COMMENT '大学ID',
  `campus_id` int(11) DEFAULT NULL COMMENT 'キャンパスID',
  `enrollment_year` int(4) DEFAULT NULL COMMENT '入学年度',
  `sex_id` tinyint(1) NOT NULL COMMENT '性別iD',
  `is_company` tinyint(1) NOT NULL DEFAULT '0' COMMENT '会社担当者フラグ',
  `is_ios` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'iOSユーザ',
  `is_android` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Androidユーザ',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザMst';

-- Data exporting was unselected.


-- Dumping structure for table capture.user_facebook_informations
DROP TABLE IF EXISTS `user_facebook_informations`;
CREATE TABLE IF NOT EXISTS `user_facebook_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `username` varchar(40) NOT NULL COMMENT 'facebookユーザID',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザFacebookテーブル';

-- Data exporting was unselected.


-- Dumping structure for table capture.user_profiles
DROP TABLE IF EXISTS `user_profiles`;
CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT 'user_id',
  `email` varchar(255) NOT NULL COMMENT 'email',
  `password` varchar(40) NOT NULL COMMENT 'パスワード',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザテーブル';

-- Data exporting was unselected.


-- Dumping structure for table capture.user_recruiters
DROP TABLE IF EXISTS `user_recruiters`;
CREATE TABLE IF NOT EXISTS `user_recruiters` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '会社担当ID',
  `company_id` int(11) NOT NULL COMMENT '会社ID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `introduction_text` varchar(255) DEFAULT NULL COMMENT '自己紹介文',
  `thumbnail_img` varchar(255) DEFAULT NULL COMMENT 'サムネイル画像',
  `disable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '無効フラグ',
  `created` int(11) NOT NULL COMMENT '作成日',
  `updated` int(11) NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会社担当者マスタ';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
