-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

ALTER TABLE `companies`
ADD COLUMN `ower_user_id` INT(11) NULL COMMENT 'オーナーユーザID' AFTER `corporate_entry`,
ADD COLUMN `name_english` VARCHAR(255) NULL COMMENT '英語表記' AFTER `ower_user_id`,
ADD COLUMN `introduction_what_txt` TEXT NULL COMMENT 'なにをやるのか？' AFTER `name_english`,
ADD COLUMN `introduction_what_img1` TEXT NULL COMMENT 'なにをやるのか？画像１' AFTER `introduction_what_txt`,
ADD COLUMN `introduction_what_img2` TEXT NULL COMMENT 'なにをやるのか？画像２' AFTER `introduction_what_img1`,
ADD COLUMN `introduction_why_txt` TEXT NULL COMMENT 'なぜやるのか？' AFTER `introduction_what_img2`,
ADD COLUMN `introduction_why_img1` TEXT NULL COMMENT 'なぜやるのか？' AFTER `introduction_why_txt`,
ADD COLUMN `introduction_why_img2` TEXT NULL COMMENT 'なぜやるのか？' AFTER `introduction_why_img1`,
ADD COLUMN `introduction_how_txt` TEXT NULL COMMENT 'どうやってやるのか？' AFTER `introduction_why_img2`,
ADD COLUMN `introduction_how_img1` TEXT NULL COMMENT 'どうやってやるのか？画像1' AFTER `introduction_how_txt`,
ADD COLUMN `introduction_how_img2` TEXT NULL COMMENT 'どうやってやるのか？画像2' AFTER `introduction_how_img1`,
ADD COLUMN `introduction_likethis_txt` TEXT NULL COMMENT 'こんなことやります。' AFTER `introduction_how_img2`,
ADD COLUMN `introduction_likethis_img1` TEXT NULL COMMENT 'こんなことやります。画像1' AFTER `introduction_likethis_txt`,
ADD COLUMN `introduction_likethis_img2` TEXT NULL COMMENT 'こんなことやります。画像2' AFTER `introduction_likethis_img1`;

-- Dumping structure for table capture.company_settings
CREATE TABLE IF NOT EXISTS `company_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `value` text,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='企業設定テーブル';

-- Dumping data for table capture.company_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `company_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_settings` ENABLE KEYS */;


-- Dumping structure for table capture.group_settings
CREATE TABLE IF NOT EXISTS `group_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `value` text,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='グループ設定テービル';

-- Dumping data for table capture.group_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `group_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_settings` ENABLE KEYS */;


-- Dumping structure for table capture.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `data_type` varchar(10) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `default_value` text,
  `value` text,
  `pattern_url_rule` varchar(100) DEFAULT NULL,
  `disable` tinyint(1) DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='設定テーブル\r\n';

-- Dumping data for table capture.settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


-- Dumping structure for table capture.user_settings
CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `value` text,
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザ設定テーブル';

-- Dumping data for table capture.user_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_settings` ENABLE KEYS */;


-- Dumping structure for trigger capture.before_insert_company_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_company_settings` BEFORE INSERT ON `company_settings` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_group_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_group_settings` BEFORE INSERT ON `group_settings` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_settings` BEFORE INSERT ON `settings` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_insert_user_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_insert_user_settings` BEFORE INSERT ON `user_settings` FOR EACH ROW SET
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_company_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_company_settings` BEFORE UPDATE ON `company_settings` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_group_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_group_settings` BEFORE UPDATE ON `group_settings` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_settings` BEFORE UPDATE ON `settings` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger capture.before_update_user_settings
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `before_update_user_settings` BEFORE UPDATE ON `user_settings` FOR EACH ROW SET
	new.updated = UNIX_TIMESTAMP()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
