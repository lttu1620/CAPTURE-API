<?php echo !empty($name) ? $name : ''?>様<br />
<br />
お問い合わせ頂き、誠にありがとうござます。<br />
お問い合せ内容を確認次第、ご返答いたしますので、<br />
今しばらくお待ちくださいますよう、よろしくお願い申し上げます。<br />
<br />
<?php echo !empty($name) ? $name : ''?> 様から頂いた内容は下記のとおりです。<br />
<br />
---------------<br />
【お名前】<?php echo !empty($name) ? $name : ''?><br />
【Eメール】<?php echo !empty($email) ? $email : ''?><br />
【電話番号】<?php echo !empty($tel) ? $tel : ''?><br />
【住所】<?php echo !empty($address) ? $address : ''?><br />
【URL】<?php echo !empty($website) ? $website : '' ?><br />
【件名】<?php echo !empty($subject) ? $subject : ''?><br />
【内容】<?php echo !empty($content) ? nl2br($content) : ''?><br />
<br />
---------------<br />
<?php echo !empty($server_host) ? $server_host : ''?> / <?php echo !empty($server_ip) ? $server_ip : ''?><br />
<?php echo !empty($company_name) ? $company_name : ''?> / <?php echo !empty($company_id) ? $company_id : ''?> / <?php echo !empty($user_id) ? $user_id : ''?><br />
capture-news.jp<br />