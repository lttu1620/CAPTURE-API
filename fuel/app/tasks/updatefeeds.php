<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;
use Fuel\Core\Package;

/**
 * Import news feeds from Rss
 * 
 * @package             Tasks
 * @create              Fer 02 2015
 * @version             1.0
 * @author             <Tuancd>
 * @run                 php oil refine updatefeeds
 * @run                 FUEL_ENV=test php oil refine updatefeeds
 * @run                 FUEL_ENV=production php oil refine updatefeeds
 * @copyright           Oceanize INC
 */
class UpdateFeeds {

    public static function run() {
        Cli::write("BEGIN PROCESSING . . . . ! \n");
        include(APPPATH . '/classes/lib/simple_html_dom.php');

        try {
            // Get list feed has from news_feed_import_logs has status = 2 (import fail or feed has track change)
            $options = array(
                'where'=>array('status' => 2),
                'limit'=>100);
            $feedList = \Model_News_Feed_Import_Log::find('all', $options);
            if (!empty($feedList)) {
                foreach ($feedList as $item) {
                    $detailURL = $item['detail_url'];
                    // Start Block
                    \LogLib::info('BEGIN block update information for feed: ', __METHOD__, $detailURL);
                    Cli::write("-BEGIN block update information for feed: {$detailURL}\n");

                    //Get Information of feed to update [titele | image]
                    // Get Image for feed
                    $str = @file_get_contents($detailURL);
                    if ($str !== FALSE && !empty($str)) {
                        $str = html_by_tag($str, "<head", "</head>");
                        // Detect and convert encoding to utf-8
                        preg_match_all('~charset=\"{0,1}([-a-z0-9_]+)\"{0,1}~i', $str, $charset);
                        $encoding =$charset[1];
                        if (!empty($encoding) && is_string($encoding) && strtolower($encoding) !== "utf-8" ) {
                            $str = mb_convert_encoding($str, 'utf-8', $encoding);
                        }
                        $html = str_get_html($str);
                        $image_url = "";
                        $title = "";
                        foreach ($html->find('meta[property=og:image]') as $element) {
                            $image_url = !empty($element->attr['content']) ? $element->attr['content'] : "";
                            break;
                        }
                        if (!\Lib\Util::url_exists($image_url))
                            $image_url = "";

                        // Get title
                        foreach ($html->find('meta[property=og:title]') as $element) {
                            $title = !empty($element->attr['content']) ? $element->attr['content'] : "";
                            break;
                        }
                        if (empty($title)) {
                            foreach ($html->find('title') as $element) {
                                $title = !empty($element->innertext) ? $element->innertext : "";
                                break;
                            }
                        }

                        //Update infomation for feed
                        $newsfeed = \Model_News_Feed::find($item['news_feed_id']);
                        if (empty($newsfeed)) {
                            \LogLib::info("     + Feed :" . $item['news_feed_id'] . "- {$detailURL} not exists in system.", 
                                __METHOD__, $detailURL);
                            Cli::write("\t+Feed: " . $item['news_feed_id'] . "- {$detailURL} not exists in system. \n");
                        } else {
                            $newsfeed->set('title', $title);
                            $newsfeed->set('image_url', $image_url);
                            if ($newsfeed->save()) {
                                \LogLib::info("     + Feed :" . $item['news_feed_id'] . "- {$detailURL} update SUCCESSFULL.",
                                    __METHOD__, $detailURL);
                                Cli::write("\t+Feed: " . $item['news_feed_id'] . "- {$detailURL} update SUCCESSFULL. \n");
                                // Update status for News_feed_import_logs 
                                \Model_News_Feed_Import_Log::add_update(array(
                                    'id' => $item['id'],
                                    'status' => 1
                                ));
                            } else {
                                \LogLib::info("     + Feed :" . $item['news_feed_id'] . "- {$detailURL} update FAIL.", 
                                    __METHOD__, $detailURL);
                                Cli::write("\t+Feed: " . $item['news_feed_id'] . "- {$detailURL} update FAIL. \n");
                            }
                        }
                    }

                    // End Block
                    \LogLib::info('END block update information for feed: ', __METHOD__, $detailURL);
                    Cli::write("-END bock update information for feed: {$detailURL} \n");
                    Cli::write("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
                        . "+++++++++++++++++++++++++++++++++++++++++++++\n");
                }
            } else {
                \LogLib::info('Has no feed to processing !!', __METHOD__, null);
                Cli::write("- Has no feed to processing !!  \n");
            }
            Cli::write("PROCESSING DONE . . . . ! \n");
        } catch (Exception $exc) {

            \LogLib::error('Has error while processing.', __METHOD__, $item);
            Cli::write("Has error while processing.");
        }
    }

}
