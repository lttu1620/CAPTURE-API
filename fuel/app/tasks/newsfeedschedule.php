<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;
use Fuel\Core\Package;

/**
 * Import news feeds from Rss
 * 
 * @package             Tasks
 * @create              Apr 03 2015
 * @version             1.0
 * @author             <Tuancd>
 * @run                 php oil refine newsfeedschedule
 * @run                 FUEL_ENV=test php oil refine newsfeedschedule
 * @run                 FUEL_ENV=production php oil refine NewsFeedSchedule
 * @copyright           Oceanize INC
 */
class NewsFeedSchedule {

    public static function run() {
        Cli::write("BEGIN PROCESSING . . . . ! \n");
    
        try {
            // Get list feed in news_feed_schedules  has finish_date < now()

            $feedList = \Model_News_Feed::get_list_news_feed_process_in_schedule(array('time' => time()));
            //var_dump($feedList); exit;
            if (!empty($feedList)) {
                $countSD = 0;
                $countFD = 0;
                $listupdatestart = array();
                $listupdatefinish = array();
                foreach ($feedList as $item) {
                    $is_public =  intval($item['is_public']);
                    $detailURL = $item['detail_url'];                    
                    // Start Block 
                    switch ($is_public) {
                        case 0:
                            if($item['start_date'] < time()){
                                $options = array('where' => array(
                                    'news_feed_id' => $item['id'],
                                    'disable'      => 0
                                ));
                                if ($item['comments_count'] > 0 ||
                                        !empty(\Model_News_Feed_Category::find('first', $options))) {
                                    $countSD ++;
                                    $listupdatestart[] = $item['id'];                                                            
                                }
                            }
                            break;
                        
                        case 1:    
                            if($item['finish_date'] < time()){
                                $countFD ++;
                                $listupdatefinish[] = $item['id'];
                            }
                        break;
                    }
                    // End Block
                }
                // Update to DB 
                if($countSD > 0){
                    $param = array(
                        'news_feed_id'  => $listupdatestart,
                        'is_public'     => 1,
                        'flag'          => 1
                     );
                    \Model_News_Feed::multi_update_is_public($param);
                     //Cli::write("-Update Is_public [0] -> [1] for {$countSD} feeds \n");
                    //\LogLib::info('Update Is_public [0] -> [1] for {$countSD} feeds', __METHOD__, $countSD);
                }
                if($countFD > 0){
                    $param = array(
                        'news_feed_id'  => $listupdatefinish,
                        'is_public'     => 0,
                        'flag'          => 2
                     );
                    //var_dump(expression)($param);
                    \Model_News_Feed::multi_update_is_public($param);
                     //Cli::write("-Update Is_public [1] -> [0] for {$countFD} feeds \n");
                    //\LogLib::info('Update Is_public [1] -> [0] for {$countFD} feeds', __METHOD__, $countFD);
                }
            Cli::write("\n\n");
            Cli::write("-------------------------------------------------------------- \n");
            Cli::write("PROCESSING update Is_public [0] -> [1]: {$countSD}  feeds \n");
            Cli::write("PROCESSING update Is_public [1] -> [0]: {$countFD}  feeds \n");
            Cli::write("-------------------------------------------------------------- \n");

            } else {
                \LogLib::info('Has no feed to processing !!', __METHOD__, null);
                Cli::write("- Has no feed to processing !!  \n");
            }
            Cli::write("PROCESSING DONE . . . . ! \n");
        } catch (Exception $exc) {

            \LogLib::error('Has error while processing.', __METHOD__, $item);
            Cli::write("Has error while processing.");
        }
    }

}
