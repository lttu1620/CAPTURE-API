<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;

/**
 * Description of news_site_rss
 * RUN BATCH:
 * php oil refine NewsSitesRss
 * FUEL_ENV=test php oil refine NewsSitesRss
 * FUEL_ENV=production php oil refine NewsSitesRss
 * @author tuancd
 */
class NewsSitesRss
{

    protected static $_channel_date = 'channel_date', $_item_date = 'item_date';
    protected static $date_format_array = array(
        0 => array(
            'channel_date' => 'dc:date',
            'item_date' => 'dc:date'
        ),
        1 => array(
            'channel_date' => 'lastBuildDate',
            'item_date' => 'pubDate'
        ),
        2 => array(
            'channel_date' => 'pubDate',
            'item_date' => 'pubDate'
        ),
        3 => array(
            'channel_date' => 'distribution_date',
            'item_date' => 'distribution_date'
        )
    );

    public static function run()
    {
        //return self::test();        
        try {
            LogLib::info("Start batch NewsSitesRss", __METHOD__);
            Cli::write(date('Y-m-d H:i:s') . " - Begin Processing . . . . ! \n");
            include(APPPATH . '/classes/lib/simple_html_dom.php');
             
            $t = time() - Config::get('time_batch', true);
            $param = array('time' => $t);

            // Get list site and link rss
            $lsturl = \Model_News_Sites_Rss::get_list($param);
            $countFeed = 0;
            $news_feed_url = array();
            foreach ($lsturl[1] as $url) {
                try {
                    $count = 0;
                    Cli::write("----------- BEGIN  " . $url['url'] . " ----------- \n \n");
                    // Handler link if link die
                    $str = null;
                    $str = @file_get_contents($url['url']);
                    if ($str === FALSE) {
                        Cli::write("link: " . $url['url'] . " UNAVAILABLE!!  \n");
                    } else {
                        $html = null;
                        $html = str_get_html($str);
                        // Get textDate of channel first and get TagDate detect
                        list($textDate, $tagDate) = self::getTagDate($str);
                        //get distribution_date 
                        $distribution_date = strtotime($textDate);
                        $title_tag = !empty($url['tag_item_title']) ? $url['tag_item_title'] : "title";
                        $link_tag = !empty($url['tag_item_link']) ? $url['tag_item_link'] : "link";
                        $description_tag = !empty($url['tag_item_description']) ? $url['tag_item_description'] : "description";
                        $date_tag = !empty($url['tag_item_date']) ? $url['tag_item_date'] : $tagDate[self::$_item_date];

                        // Add new image_tag
                        $image_tag = !empty($url['tag_item_image']) ? $url['tag_item_image'] : "";

                        foreach ($html->find('item') as $article) {                            
                            try {
                                $link = self::getContentByTag($article->innertext, $link_tag);
                                if (in_array($link, $news_feed_url)) {
                                    continue;
                                } else {
                                    $news_feed_url[] = $link;
                                }
                                $title = self::getContentByTag($article->innertext, $title_tag, true);                                
                                $description = self::getContentByTag($article->innertext, $description_tag, true);
                                $publicDate = self::getContentByTag($article->innertext, $date_tag, true);
                                $image_url = !empty($image_tag) ? self::getContentImageByTag($article->innertext, $image_tag) : "";

                                $news_feeds_param = array(
                                    'news_site_id' => $url['news_site_id'],
                                    'title' => $title,
                                    'short_content' => $description,
                                    'detail_url' => $link,
                                    'distribution_date' => empty($publicDate) ? $distribution_date : strtotime($publicDate),
                                    'id' => $url['id'],
                                    'image_url' => $image_url
                                );
                                $result = \Model_News_Feed::add_update_for_batch($news_feeds_param);
                                if ($result === false || $result === 0) {
                                    Cli::write(" Import fail: " . $link . " \n");
                                } elseif ($result === true) {
                                    Cli::write("\t-link: $link is Exists \n");
                                } else if ($result > 0) {
                                    $count++;
                                    $countFeed++;
                                    Cli::write("\t-Title: $title \n");
                                    Cli::write("\t-link: $link \n");
                                    Cli::write("\t-Description: $description \n");
                                    Cli::write("\t-Image_url: $image_url \n \n");
                                    LogLib::info("Import feed {$link}", __METHOD__, $article);
                                }
                            } catch (Exception $exc) {
                                LogLib::error($exc->getMessage(), __METHOD__, $article);
                                Cli::write("error $count \n\n");
                            }
                        }
                    }
                    Cli::write("----------- END  " . $url['url'] . " ----------- \n \n");
                } catch (Exception $exc) {
                    Cli::write("Has error while processing data from url " . $url['url'] . "\n");
                }
            }
            Cli::write(date('Y-m-d H:i:s') . " - Processing Done . . . . !. Add new successful $countFeed feeds \n");
        } catch (Exception $exc) {
            Cli::write("Has error while processing.");
        }
    }

    static function getContentByTag($stringText = "", $tag = "", $isPlanText = false)
    {
        try {
            preg_match("/<$tag>(.*)<\/$tag>/", $stringText, $match);
            if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                return "";
            } else {
                $string = "";
                if ($isPlanText) {
                    $string = str_replace("<![CDATA[", "", $match[1]);
                    $string = strip_tags(str_replace("]]>", "", $string));
                } else {
                    $string = $match[1];
                }
                if (($_index = strpos($string, "</$tag>")) !== false) {
                    $string = substr($string, 0, $_index);
                }
                preg_match("/&#[xX][a-fA-F0-9]+/", $string, $match);
                if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                    return trim($string);
                } else {
                    return html_entity_decode(trim($string), ENT_QUOTES);
                }
            }
        } catch (Exception $exc) {
            return "";
        }
    }

    static function getContentImageByTag($stringText = "", $tag = "")
    {
        try {
            preg_match("/<$tag>(.*)<\/$tag>/", $stringText, $match); //Macth by format [<aa> html text </aa>]
            if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR) {
                return "";
            } else {
                if (sizeof($match) == 0) {
                    preg_match("/<$tag(.*)\/>/", $stringText, $match); // Macth by format [<ab src="http://abcd.com/d/e/1.jpg" />]
                    if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                        return "";
                    } else {
                        $string = $match[1];
                        preg_match("/&#[xX][a-fA-F0-9]+/", $string, $match);
                        if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                            $string = trim($string);
                        } else {
                            $string = html_entity_decode(trim($string), ENT_QUOTES);
                        }
                        // detect image url
                        preg_match("/((?:https?\:\/\/)(?:[a-zA-Z]{1}(?:[\w\-]+\.)+(?:[\w]{2,5}))(?:\:[\d]{1,5})?\/(?:[^\s\/]+\/)*(?:[^\s]+.(?:jpe?g|gif|png))(?:\?\w+=\w+(?:&\w+=\w+)*)?)/", $string, $match);
                        if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0 ) {
                            return "";
                        } else {
                            return $match[1];
                        }
                    }
                } else {
                    $string = $match[1];
                    if (($_index = strpos($string, "</$tag>")) !== false) {
                        $string = substr($string, 0, $_index);
                    }
                    preg_match("/&#[xX][a-fA-F0-9]+/", $string, $match);
                    if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                        return trim($string);
                    } else {
                        return html_entity_decode(trim($string), ENT_QUOTES);
                    }
                }
            }
        } catch (Exception $exc) {
            return "";
        }
    }

    static function getTagDate($str)
    {
        $max = sizeof(self::$date_format_array);
        for ($i = 0; $i < $max; $i++) {
            $text = self::getContentByTag($str, self::$date_format_array[$i]['channel_date']);
            if (!empty($text)) {
                return list($textDate, $tagDate) = array($text, self::$date_format_array[$i]);
                break;
            }
        }
    }
    
    public static function test()
    {
        $news_feed_url = array();
        try {
            for($i = 0; $i < 10; $i++) {
                $link = 'http://rss.rssad.jp/rss/artclk/9FLW5bfpNz.D/45d674cc0e2bed109dd8bca50cd48a4e?ul=vC_XoyoJEbhxkX7lko1EgbqYKmtpZICVefq33ljPd6vv5xxLPqmB0awkFBqj1d3MY2tyqTs';
                if (in_array($link, $news_feed_url)) {
                    continue;
                } else {
                    $news_feed_url[] = $link;
                }
                $url['news_site_id'] = 41;
                $url['id'] = 11; 
                $title = '支持率急低下の安倍政権“維持可能性”を検証する - 高橋洋一の俗論を撃つ！';                
                $description = '安倍政権の支持率が低下している。7月には初めて支持率が不支持率を下回った。今後も懸念材料が多くあるが、持ちこたえることはできるのか。過去歴代政権のデータと照らし合わせて見てみよう。';
                $publicDate = 'Fri, 31 Jul 2015 11:20:30 +0900';
                $image_url = "http://diamond.jp/mwimgs/8/a/-/img_8a8cb560be885abfed87d71fc83ebb9d679739.jpg";
                $news_feeds_param = array(
                    'news_site_id' => $url['news_site_id'],
                    'title' => $title,
                    'short_content' => $description,
                    'detail_url' => $link,
                    'distribution_date' => strtotime($publicDate),
                    'id' => $url['id'],
                    'image_url' => $image_url
                );
                $result = \Model_News_Feed::add_update_for_batch($news_feeds_param);
                if ($result === false || $result === 0) {
                    Cli::write(" Import fail: " . $link . " \n");
                } elseif ($result === true) {
                    Cli::write("\t-link: $link is Exists \n");
                } else if ($result > 0) {                   
                    Cli::write("\t-Title: $title \n");                                    
                }
            }
        } catch (Exception $exc) {
            LogLib::error($exc->getMessage(), __METHOD__);
            Cli::write("error $count \n\n");
        }
    }

}
