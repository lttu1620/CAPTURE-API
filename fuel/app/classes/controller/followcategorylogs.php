<?php

/**
 * Controller for actions on follow category logs
 *
 * @package Controller
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowCategoryLogs extends \Controller_App
{
    /**
     * Get list or FollowCategoryLog
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\FollowCategoryLogs_List::getInstance()->execute();
    }

    /**
     * Add new FollowCategoryLogs
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowCategoryLogs_Add::getInstance()->execute();
    }

}