<?php

/**
 * Controller_NewsFeeds - Controller for actions on NewsFeeds
 *
 * @package Controller
 * @created 2014-11-24
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_NewsFeeds extends \Controller_App
{

    /**
     *  Get list NewsFeeds by action POST
     * 
     * @return boolean 
     */
    public function post_list()
    {
        return \Bus\NewsFeeds_List::getInstance()->execute();
    }

    /**
     *  Get list NewsFeeds by action GET
     * 
     * @return boolean 
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
    /**
     *  Get detail NewsFeeds by action POST
     * 
     * @return boolean 
     */
    public function post_detail()
    {
        return \Bus\NewsFeeds_Detail::getInstance()->execute();
    }

    /**
     *  Get detail NewsFeeds by action GET
     * 
     * @return boolean 
     */
    public function get_detail()
    {
        return $this->post_detail();
    }

    /**
     *  Update disable field NewsFeeds
     * 
     * @return boolean 
     */
    public function post_disable()
    {
        return \Bus\NewsFeeds_Disable::getInstance()->execute();
    }

    /**
     *  Update or add news NewsFeeds
     * 
     * @return boolean 
     */
    public function post_addupdate()
    {
        return \Bus\NewsFeeds_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get uu of NewsFeeds by action POST
     * 
     * @return boolean 
     */
    public function post_uu()
    {
       
        return \Bus\Reports_NewsFeedsUu::getInstance()->execute();
    }

    /**
     *  Get uu of NewsFeeds by action GET
     * 
     * @return boolean 
     */
    public function get_uu()
    {
        return $this->post_uu();
    }

    /**
     *  Get pv of NewsFeeds by action POST
     * 
     * @return boolean 
     */
    public function post_pv()
    { 
        return \Bus\Reports_NewsFeedsPv::getInstance()->execute();
    }

    /**
     *  Get uu of NewsFeeds by action GET
     * 
     * @return boolean 
     */
    public function get_pv()
    {
        return $this->post_pv();
    }

    /**
     *  Update Counter of NewsFeeds by action POST
     * 
     * @return boolean 
     */
    public function post_updateCounter()
    {
        return \Bus\NewsFeeds_UpdateCounter::getInstance()->execute();
    }
    /**
     *  Test Action 
     * 
     * @return boolean
     */
     public function action_test()
    {
        return \Bus\NewsFeeds_Test::getInstance()->execute();
    }

    /**
     *  Update is_public field NewsFeeds
     *
     * @return boolean
     */
    public function post_public()
    {
        return \Bus\NewsFeeds_Public::getInstance()->execute();
    }

    //schedule
    /**
     *  Update Set startdate and enddate to NewsFeeds
     *
     * @return boolean
     */
    public function post_schedule()
    {
        return \Bus\NewsFeeds_Schedule::getInstance()->execute();
    }


    /**
     *  Check news_feed isvalid to change
     *
     * @return boolean
     */
    public function post_isvalidpublic()
    {
        return \Bus\NewsFeeds_IsvalidPublic::getInstance()->execute();
    }

    /**
     * Set public for tadacopy
     *
     * @return boolean
     */
    public function action_isTadacopy()
    {
        return \Bus\NewsFeeds_IsTadacopy::getInstance()->execute();
    }
}
