<?php

/**
 * Controller for actions on Campuses
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Campuses extends \Controller_App
{
     /**
     * Get all Campuses
     *  
     * @return boolean    
     */
    public function action_all() {
        return \Bus\Campuses_All::getInstance()->execute();
    }
 
    /**
     * Get detail Campuses
     *  
     * @return boolean    
     */
    public function action_detail() {
        return \Bus\Campuses_Detail::getInstance()->execute();
    }
    
    /**
     * Get List Campuses by conditions
     *  
     * @return boolean    
     */
    public function action_list() {
        return \Bus\Campuses_List::getInstance()->execute();
    }
    
    /**
     * Update disable field for Campuses
     *  
     * @return boolean    
     */
    public function action_disable() {
        return \Bus\Campuses_Disable::getInstance()->execute();
    }

    /**
     * Update or add new infomaion for Campuses
     *  
     * @return boolean    
     */
    public function action_addUpdate() {
        return \Bus\Campuses_AddUpdate::getInstance()->execute();
    }

}