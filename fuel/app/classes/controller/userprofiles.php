<?php

/**
 * Controller_UserProfiles
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_UserProfiles extends \Controller_App
{
    /**
     *  Get detail profile of User
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\UserProfiles_Detail::getInstance()->execute();
    }

    /**
     *  Get detail profile of User by email
     * 
     * @return boolean 
     */
    public function action_detailByEmail() {
        return \Bus\UserProfiles_DetailByEmail::getInstance()->execute();
    }

    /**
     *  Get list profile of User
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\UserProfiles_List::getInstance()->execute();
    }

    /**
     *  Update or add new detail profile for User
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\UserProfiles_AddUpdate::getInstance()->execute();
    }

    /**
     *  Update password field for UserProfile
     * 
     * @return boolean 
     */
    public function action_updatePassword() {
        return \Bus\UserProfiles_UpdatePassword::getInstance()->execute();
    }

    /**
     *  Change password field for UserProfile
     * 
     * @return boolean 
     */
    public function action_changePassword() {
        return \Bus\UserProfiles_changePassword::getInstance()->execute();
    }
    
    /**
     *  Get email of admin company
     * 
     * @return boolean 
     */
    public function post_companyadminemail() {
        return \Bus\UserProfiles_CompanyAdminEmail::getInstance()->execute();
    }
}