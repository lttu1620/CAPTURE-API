<?php

/**
 * Controller for actions on Help Content
 *
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_HelpContents extends \Controller_App
{
    /**
     *Get detail for HelpContents by action GET
     *  
     * @return boolean
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     * Get detail for HelpContents by action POST
     *  
     * @return boolean
     */
    public function post_detail() {
        return \Bus\HelpContents_Detail::getInstance()->execute();
    }

    /**
     *  
     * @return boolean Get list of HelpContents by action GET
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  
     * @return boolean Get list of HelpContents by action POST
     */
    public function post_list() {
        return \Bus\HelpContents_List::getInstance()->execute();
    }

    /**
     *  
     * @return boolean Update disable field for HelpContents 
     */
    public function action_disable() {
        return \Bus\HelpContents_Disable::getInstance()->execute();
    }

    /**
     *  
     * @return boolean Update or add new HelpContents
     */
    public function action_addUpdate() {
        return \Bus\HelpContents_AddUpdate::getInstance()->execute();
    }

}