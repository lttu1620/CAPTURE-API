<?php

/**
 * Controller for actions on Post
 *
 * @package Controller
 * @created 2015-08-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Posts extends \Controller_App
{
    /**
     * Add info for Post
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\Posts_Add::getInstance()->execute();
    }

    /**
     * Update info for Post
     *
     * @return boolean
     */
    public function action_update()
    {
        return \Bus\Posts_Update::getInstance()->execute();
    }

    /**
     * Get list Post (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Posts_List::getInstance()->execute();
    }

    /**
     * Disable/Enable list Post
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Posts_Disable::getInstance()->execute();
    }

    /**
     * Get detail Post
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Posts_Detail::getInstance()->execute();
    }

    /**
     * Public/Private a post
     *
     * @return boolean
     */
    public function action_isPublic()
    {
        return \Bus\Posts_IsPublic::getInstance()->execute();
    }
}
