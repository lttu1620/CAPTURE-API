<?php

/**
 * Controller for actions on company category
 *
 * @package Controller
 * @created 2014-12-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_CompanyCategories extends \Controller_App
{
    /**
     * Get detail of CompanyCategory
     *  
     * @return boolean   
     */
    public function action_detail() {
        return \Bus\CompanyCategories_Detail::getInstance()->execute();
    }

    /**
     * Get List of CompanyCategory y conditions
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\CompanyCategories_List::getInstance()->execute();
    }

    /**
     * Update disable field for CompanyCategory
     *  
     * @return boolean    
     */
    public function action_disable() {
        return \Bus\CompanyCategories_Disable::getInstance()->execute();
    }

    /**
     * Add new CompanyCategory
     *  
     * @return boolean    
     */
    public function action_add() {
        return \Bus\CompanyCategories_Add::getInstance()->execute();
    }

    /**
     * Get all of CompanyCategory
     *  
     * @return boolean   
     */
    public function action_all() {
        return \Bus\CompanyCategories_All::getInstance()->execute();
    }

}