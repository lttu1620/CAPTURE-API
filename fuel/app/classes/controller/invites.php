<?php

/**
 * Controller for actions on Invite
 *
 * @package Controller
 * @created 2015-07-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Invites extends \Controller_App
{
    /**
     * Add or update info for Invite
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Invites_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Invite (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Invites_List::getInstance()->execute();
    }

    /**
     * Disable/Enable list Invite
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Invites_Disable::getInstance()->execute();
    }

    /**
     * Update status Invite
     *
     * @return boolean
     */
    public function action_status()
    {
        return \Bus\Invites_Status::getInstance()->execute();
    }
}
