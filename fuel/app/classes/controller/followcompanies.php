<?php

/**
 * Controller for actions on follow company
 *
 * @package Controller
 * @created 2014-12-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowCompanies extends \Controller_App
{
    
    /**
     * Get detail of FollowCompany by action GET
     *  
     * @return boolean
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     * Get detail of FollowCompany by action GET
     *  
     * @return boolean
     */
    public function post_detail() {
        return \Bus\FollowCompanies_Detail::getInstance()->execute();
    }

    /**
     * Get list of FollowCompany by action GET
     *  
     * @return boolean
     */
    public function get_list() {
        return $this->post_list();
    }
    
    /**
     * Get list of FollowCompany by action POST
     *  
     * @return boolean
     */
    public function post_list() {
        return \Bus\FollowCompanies_List::getInstance()->execute();
    }

    /**
     * Update disable field for FollowCompany 
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\FollowCompanies_Disable::getInstance()->execute();
    }

    /**
     * Add new FollowCompanies
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowCompanies_Add::getInstance()->execute();
    }

}