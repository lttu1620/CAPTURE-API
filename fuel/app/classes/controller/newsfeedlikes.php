<?php

/**
 * Controller for actions on News Feed Likes
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsFeedLikes extends \Controller_App
{
    /**
     *  Get detail NewsFeedLikes
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\NewsFeedLikes_Detail::getInstance()->execute();
    }

    /**
     *  Get lisst NewsFeedLikes
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\NewsFeedLikes_List::getInstance()->execute();
    }

    /**
     *  Update disable field NewsFeedLikes
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsFeedLikes_Disable::getInstance()->execute();
    }

    /**
     * Add new NewFeedLikes 
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\NewsFeedLikes_Add::getInstance()->execute();
    }

}