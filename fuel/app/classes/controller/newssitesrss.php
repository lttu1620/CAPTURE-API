<?php

/**
 * Controller for actions on News Comments
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsSitesRss extends \Controller_App
{
    /**
     *  Get detail of NewsSitesRss by action GET
     * 
     * @return boolean 
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     *  Get detail of NewsSitesRss by action POST
     * 
     * @return boolean 
     */
    public function post_detail() {
        return \Bus\NewsSitesRss_Detail::getInstance()->execute();
    }

    /**
     *  Get list of NewsSitesRss by action GET
     * 
     * @return boolean 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list of NewsSitesRss by action GET
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\NewsSitesRss_List::getInstance()->execute();
    }

    /**
     *  Update disable field for NewsSitesRss
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsSitesRss_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new NewsSitesRss
     * 
     * @return boolean 
     */
    public function action_addUpdate() {
        return \Bus\NewsSitesRss_AddUpdate::getInstance()->execute();
    }

}