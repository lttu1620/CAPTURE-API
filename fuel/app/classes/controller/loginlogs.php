<?php

/**
 * Controller for actions on login logs
 *
 * @package Controller
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_LoginLogs extends \Controller_App
{
    /**
     * Get list of loginLogs by action GET
     *  
     * @return boolean 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list of loginLogs by action POST
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\LoginLogs_List::getInstance()->execute();
    }

    /**
     *  Add new LoginLogs 
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\LoginLogs_Add::getInstance()->execute();
    }

}