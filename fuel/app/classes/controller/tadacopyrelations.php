<?php

/**
 * Controller for actions on Tada copy Relation
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_TadaCopyRelations extends \Controller_App
{
    /**
     *  Write log after calling API of TadaCopy
     * 
     * @return boolean 
     */
	public function action_externalRelationLogs()
	{
		return \Bus\TadaCopyRelations_ExternalRelationLogs::getInstance()->execute();
	}

    /**
     *  Get list detail NewsFeed for TadaCopy
     * 
     * @return boolean 
     */
	public function action_newsList()
	{
		return \Bus\TadaCopyRelations_NewsList::getInstance()->execute();
	}

    /**
     *  Get news detail for tada copy (API 2)
     * 
     * @return boolean 
     */
	public function action_newsDetail()
	{
		return \Bus\TadaCopyRelations_NewsDetail::getInstance()->execute();
	}

    /**
     *  Get company detail tada copy relation
     * 
     * @return boolean 
     */
	public function action_companyDetail()
	{
		return \Bus\TadaCopyRelations_CompanyDetail::getInstance()->execute();
	}

    /**
     *  Get company user list tada copy relation
     * 
     * @return boolean 
     */
	public function action_companyUserList()
	{
		return \Bus\TadaCopyRelations_CompanyUserList::getInstance()->execute();
	}

    
    /**
     *  Apply like from tada copy relation
     * 
     * @return boolean 
     */
	public function action_newsCommentLike()
	{
		return \Bus\TadaCopyRelations_NewsCommentLike::getInstance()->execute();
	}
}