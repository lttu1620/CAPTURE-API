<?php

/**
 * Controller_Companies
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Companies extends \Controller_App
{
    /**
     * Get detail of Company by action GET
     *  
     * @return boolean
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     * Get detail of Company by action POST
     *  
     * @return boolean    
     */
    public function post_detail() {
        return \Bus\Companies_Detail::getInstance()->execute();
    }

    /**
     * Get list of Company
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Companies_List::getInstance()->execute();
    }

    /**
     * Update disable field for Company
     *  
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Companies_Disable::getInstance()->execute();
    }

    /**
     * Update or add new information for Company
     *  
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Companies_AddUpdate::getInstance()->execute();
    }
    
    /**
     * Get all of Company
     *  
     * @return boolean
     */
    public function action_all() {
        return \Bus\Companies_All::getInstance()->execute();
    }
    
    /**
     * Update counter for company
     *  
     * @return boolean 
     */
    public function action_updateCounter() {
        return \Bus\Companies_UpdateCounter::getInstance()->execute();
    }
    
}