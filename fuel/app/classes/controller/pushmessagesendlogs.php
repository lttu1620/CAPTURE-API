<?php

/**
 * Controller for actions on push message send logs
 *
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_PushMessageSendLogs extends \Controller_App
{
    /**
     *   Get list PushMessageSendLogs by action GET
     * 
     * @return boolean
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list PushMessageSendLogs by action POST
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\PushMessageSendLogs_List::getInstance()->execute();
    }

    /**
     *  Get detail PushMessageSendLogs by action GET
     * 
     * @return boolean 
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     *  Get detail PushMessageSendLogs by action POST
     * 
     * @return boolean 
     */
    public function post_detail() {
        return \Bus\PushMessageSendLogs_Detail::getInstance()->execute();
    }

    /**
     *  Add new MessageSendLogs
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\PushMessageSendLogs_Add::getInstance()->execute();
    }

}