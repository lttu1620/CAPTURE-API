<?php

/**
 * Controller for actions on User Facebook Information
 *
 * @package Controller
 * @created 2014-11-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserFacebookInformations extends \Controller_App
{
    /**
     *  Get detail facebook Infomation of user
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\UserFacebookInformations_Detail::getInstance()->execute();
    }

    /**
     *  Update or add new detail facebook Infomation for user
     * 
     * @return boolean 
     */
    public function action_addUpdate() {
        return \Bus\UserFacebookInformations_AddUpdate::getInstance()->execute();
    }

}