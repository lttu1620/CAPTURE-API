<?php

/**
 * Controller_CompanyViewLogs - Controller for actions on CompanyViewLogs
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_CompanyViewLogs extends \Controller_App
{

     /**
     * Get list CompanyViewLogs  by action POST
     *  
     * @return boolean
     */
    public function post_list()
    {
        return \Bus\CompanyViewLogs_List::getInstance()->execute();
    }
    /**
     * Get list CompanyViewLogs  by action GET
     *  
     * @return boolean
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
    /**
     * Update or add new information for CompanyViewLogs  by action POST
     *  
     * @return boolean
     */ 
    public function post_addupdate()
    {
        return \Bus\CompanyViewLogs_AddUpdate::getInstance()->execute();
    }
    
}
