<?php

/**
 * Controller for actions on follow subcategory
 *
 * @package Controller
 * @created 2014-12-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowSubcategories extends \Controller_App
{
    /**
     * Get detail of FollowSubcategories 
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\FollowSubcategories_Detail::getInstance()->execute();
    }
    
    /**
     * Get list of FollowSubcategories 
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\FollowSubcategories_List::getInstance()->execute();
    }

    /**
     * Update disable field for FollowSubcategories 
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\FollowSubcategories_Disable::getInstance()->execute();
    }

    /**
     * Add new FollowSubcategories 
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowSubcategories_Add::getInstance()->execute();
    }

}