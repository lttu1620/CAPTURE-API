<?php

/**
 * <Controller for actions on start logs>
 *
 * @package Controller
 * @created 2015-01-08
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_StartLogs extends \Controller_App
{
    /**
     *  Get list Startlogs by action GET
     * 
     * @return boolean 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list Startlogs by action POST
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\StartLogs_List::getInstance()->execute();
    }

    /**
     *  Add new Startlogs
     * @return boolean 
     */
    public function action_add() {
        return \Bus\StartLogs_Add::getInstance()->execute();
    }

}