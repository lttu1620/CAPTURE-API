<?php

/**
 * Controller_NewsFeedViewLogs - Controller for actions on NewsFeedViewLogs
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_NewsFeedViewLogs extends \Controller_App
{
     /**
     *  Get list of NewsFeedViewLogs by action POST
     * 
     * @return boolean 
     */
    public function post_list()
    {
        return \Bus\NewsFeedViewLogs_List::getInstance()->execute();
    }
    
    /**
     *  Get list of NewsFeedViewLogs by action GET
     * 
     * @return boolean 
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
    /**
     *  Update or add new NewsFeedViewLogs
     * 
     * @return boolean 
     */
     public function post_addupdate()
    {
        return \Bus\NewsFeedViewLogs_AddUpdate::getInstance()->execute();
    }
    
}
