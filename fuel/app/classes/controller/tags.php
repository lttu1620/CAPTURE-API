<?php

/**
 * Controller for actions on Tag
 *
 * @package Controller
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Tags extends \Controller_App
{
    /**
     *  Update or add new Tags
     * 
     * @return boolean 
     */
    public function action_addUpdate()
    {
        return \Bus\Tags_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get detail of Tags
     * 
     * @return boolean 
     */
    public function action_detail()
    {
        return \Bus\Tags_Detail::getInstance()->execute();
    }

    /**
     *  Get all Tags
     * 
     * @return boolean 
     */
    public function action_all()
    {
        return \Bus\Tags_All::getInstance()->execute();
    }

    /**
     *  Get list Tag by condition
     * 
     * @return boolean 
     */
    public function action_list()
    {
        return \Bus\Tags_List::getInstance()->execute();
    }

    /**
     *  Update disable field for Tags
     * 
     * @return boolean 
     */
    public function action_disable()
    {
        return \Bus\Tags_Disable::getInstance()->execute();
    }

    /**
     *  Update counter for Tags
     * 
     * @return boolean 
     */
    public function action_updateCounter()
    {
        return \Bus\Tags_UpdateCounter::getInstance()->execute();
    }
}