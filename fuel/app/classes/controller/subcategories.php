<?php

/**
 * Controller_Subcategories - Controller for actions on Subcategory
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_SubCategories extends \Controller_App
{

    /**
     *  Get list SubCategories by action POST
     * 
     * @return boolean 
     */
    public function post_list()
    {
        return \Bus\SubCategories_List::getInstance()->execute();
    }

    /**
     *  Get detail SubCategories by action POST
     * 
     * @return boolean 
     */
    public function post_detail()
    {
        return \Bus\SubCategories_Detail::getInstance()->execute();
    }

    /**
     *  Update disable field SubCategories by action POST
     * 
     * @return boolean 
     */
    public function post_disable()
    {
        return \Bus\SubCategories_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new SubCategories by action POST
     * 
     * @return boolean 
     */
    public function post_addupdate()
    {
        return \Bus\SubCategories_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get all SubCategories by action POST
     * 
     * @return boolean 
     */
    public function action_all()
    {
        return \Bus\Subcategories_All::getInstance()->execute();
    }
}
