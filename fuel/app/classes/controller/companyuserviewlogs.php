<?php

/**
 * Controller_CompanyUserViewLogs - Controller for actions on CompanyUserViewLogs
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_CompanyUserViewLogs extends \Controller_App
{

    /**
     * Get list CompanyUserViewLogs  by action POST
     *  
     * @return boolean
     */
    public function post_list()
    {
        return \Bus\CompanyUserViewLogs_List::getInstance()->execute();
    }
     /**
     * Get list CompanyUserViewLogs  by action GET
     *  
     * @return boolean
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
     /**
     * Update or add new infomation for CompanyUserViewLogs  by action POST
     *  
     * @return boolean
     */ 
    public function post_addupdate()
    {
        return \Bus\CompanyUserViewLogs_AddUpdate::getInstance()->execute();
    }
    
}
