<?php

use Fuel\Core\Image;
/**
 * Controller_Test
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Test extends \Controller_Rest {

    /**
     *  
     * @return boolean Action index of TestController
     */
    public function action_index() {
        $offset = timezone_offset_get( new DateTimeZone(date_default_timezone_get()), new DateTime() );
        $timezone = sprintf( "%s%02d:%02d", ( $offset >= 0 ) ? '+' : '-', abs( $offset / 3600 ), abs( $offset % 3600 ) );
        echo 'Web time: ' . date('Y-m-d H:i:s') . ' (' . $timezone . ')';
        
        $time = \DB::query("SELECT now() AS time, @@session.time_zone AS time_zone;")->execute()->offsetGet(0);
        echo '<br/>DB time: ' . $time['time'] . ' (' . $time['time_zone'] . ')';
       
        exit;
    }
    
     /**
     *  
     * @return boolean Action ServerInfo of TestController
     */
    public function action_serverinfo() {
        include_once APPPATH . "/config/auth.php";
        p($_SERVER, 1);
    }

    /**
     *  
     * @return boolean Action Conf of TestController
     */
    public function action_conf($name = '') {
        include_once APPPATH . "/config/auth.php";
        p(\Config::get($name), 1);
    }

    /**
     *  
     * @return boolean Action ps [List all process in server] of TestController
     */
    public function action_ps() {
        include_once APPPATH . "/config/auth.php";
        p(\Bus\Systems_Ps::getInstance()->execute(), 1);
    }
    
    /**
     *  
     * @return boolean Action facebooksdk of TestController
     */
    public function action_facebooksdk() {
        @session_start();
        $tokenName = 'TOKEN';
        //\Cookie::set($tokenName, 'CAAIwGJIYZCBABAItd3ZBxIj6rEeXcyTIvgWrUHV6MnmDTLg4oZAYhRizDwFUgUOnM0mSOl436nEpU2e777qbuvbkzdvtnXyOBS8evKagYj5lESrSZBAgXowylsWUGKhrijXlmfxZA9v5Bxtq8GkqeJtDV2OYoxAZAesVxuHMu710M1SFlwgdvdZBQQLNZCwveZAbZAZALbGzGvFve8S4lS5zWro');
        if (\Input::get('reset')) {
            \Cookie::delete($tokenName);
        }        
        FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));
        $helper = new FacebookRedirectLoginHelper(\Uri::current());
        try {
            $session = $helper->getSessionFromRedirect();
            if (isset($session)) {
                \Cookie::set($tokenName, $session->getToken(), 60 * 60 * 24 + time(), '/');
                Response::redirect(\Uri::current());
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch (\Exception $ex) {
            // When validation fails or other local issues
        }
        if (\Cookie::get($tokenName)) {
            $info = \Model_User::login_facebook_by_token(array(
                'token' => \Cookie::get($tokenName)
            ), 1);
            echo '<pre>' . print_r($info, 1) . '</pre>';
        } else {
            echo '<a href="' . $helper->getLoginUrl(array('scope' => 'email')) . '">Login</a>';
        }        
        exit;
    }

    /**
     *  
     * @return boolean Action gcm [send notification for android device] of TestController
     */
    public function action_gcm() {
        exit;
        $param['google_regid'] = array('APA91bGMabtIPO2utQmO8rV2hEKG2OkJFwXWpSLhkujWgVoOueHl3glimoqi13ygtf3Fywv0U5akBbQhEguDSIVzTfDTMnn_EwBuM6okc2rqW32kzlcZBsn-xtc0dJnpXwsPESO6AQ9rJ7L730OvWOF5iP70qpiu6w');
        $param['message'] = 'xxxxxx';
        d(\Gcm::sendMessage($param), 1);
    }

    /**
     *  
     * @return boolean Action apns [Send notification for IOS devive] of TestController
     */
    public function action_apns() {
        exit;
        $param['apple_regid'] = '2113afb08aff63d79b3e39d6c5b4871344496a3c14b23410ee316f5b785fa52a';
        $param['message'] = 'HUYさん、ありがとうございます';
        $param['ios_sound'] = 1;
        d(\Apns::sendMessage($param), 1);
    }

    /**
     *  
     * @return boolean Action update password of TestController
     */
    public function action_updatepasswords() {
        exit;
        // Get all user_profiles
        list($total, $userProfiles) = \Model_User_Profile::get_list(array());
        if (!empty($userProfiles)) {
            d('Start to update password to user_profile');
            foreach ($userProfiles as $userProfile) {
                //Update password to user_profile 
                \Model_User_Profile::add_update(array(
                    'id' => $userProfile['id'],
                    'password' => '123456',
                    'email' => $userProfile['email']
                    ), true);
            }
            d('Updated password to user_profile');
        }

        // Get all admins
        list($total, $admins) = \Model_Admin::get_list(array());
        if (!empty($admins)) {
            d('Start to update password to admins');
            foreach ($admins as $admin) {
                //Update password to admin
                \Model_Admin::update_password(array(
                    'id' => $admin['id'],
                    'password' => '123456',
                    'login_id' => $admin['login_id'],
                    ));
            }
            d('Updated password to admins', 1);
        }
    }
    
    /**
     *  
     * @return boolean Action trigger of TestController
     */
    public function action_trigger() {
        include_once APPPATH . "/config/auth.php";
        $name = Input::get('name', '');
        $data = \Model_Common::trigger($name);
        p($data, 1);
    }

    /**
     *  
     * @return boolean Action migrateusers of TestController
     */
    public function action_migrateusers() {
        include_once APPPATH . "/config/auth.php";
        $users = \Model_User::get_user_notification();
        if (!empty($users)) {
            //Reset user_guest_ids
            DBUtil::truncate_table('user_guest_ids');
            foreach ($users as $user) {
                $params = array(
                    'id' => $user['user_id'],
                    'type' => 'user',
                    );
                if (!empty($user['apple_regid'])) {
                    $params['device_id'] = $user['apple_regid'];
                } elseif (!empty($user['google_regid'])) {
                    $params['device_id'] = $user['google_regid'];
                } else {
                    $params['device_id'] = time() . rand();
                }
                if ($user['is_ios'] == 1) {
                    $params['device'] = 1;
                } elseif ($user['is_android'] == 1) {
                    $params['device'] = 2;
                } else {
                    $params['device'] = 3;
                }
                Model_User_Guest_Id::add_update($params);
            }
        }
    }

    public function action_cropimage()
    {
         $background_color='#3b82d3';
        $imagePath='image/car.gif';
        list($width, $height) = getimagesize($imagePath);
        $new_width = $height * 16 / 9;
        $new_height = $height;

        // Create new image with backbround color $background_color
        $im = imagecreatetruecolor($new_width, $new_height)
                or die('Cannot Initialize new GD image stream');
        $color = \Lib\Util::rgb2hex2rgb($background_color);
        $pos=strrpos($imagePath, ".");
        $imagePathTemp  =  substr_replace($imagePath, '__'.rand(1,100000)."__TEMP", $pos, 0);

        $color_background = imagecolorallocatealpha($im, $color['red'], $color['green'], $color['blue'],127);
        imagefill($im, 0, 0, $color_background); 
         $fileType = exif_imagetype($imagePath);
         switch ($fileType) {
            case IMAGETYPE_JPEG:
                 # code for jpg type
                  // Save the image
                    imagejpeg($im,$imagePathTemp);
                    // Load image above save to merge image first
                    $dest   = imagecreatefromjpeg($imagePathTemp);
                    $src    = imagecreatefromjpeg($imagePath);
                    imagecopymerge($dest, $src, 
                                ($new_width - $width)/2, 
                                ($new_height- $height)/2, 
                                0, 
                                0, 
                                $width, 
                                $height, 100);
                     // Output and free from memory
                    imagejpeg($dest,$imagePath);
                    imagedestroy($im);
                    imagedestroy($src);
                    imagedestroy($dest);
                    
                 break;

            case IMAGETYPE_GIF:
                 # code for jpg type
                    // Save the image
                    imagegif($im,$imagePathTemp);
                    // Load image above save to merge image first
                    $dest   = imagecreatefromgif($imagePathTemp);
                    $src    = imagecreatefromgif($imagePath);
                    imagecopymerge($dest, $src, 
                                ($new_width - $width)/2, 
                                ($new_height- $height)/2, 
                                0, 
                                0, 
                                $width, 
                                $height, 100);
                    imagegif($dest,$imagePath);
                     // Output and free from memory
                    imagedestroy($im);
                    imagedestroy($src);
                    imagedestroy($dest);
                    
                 break;

             case IMAGETYPE_PNG:
                # code for png type
                // Save the image
                imagepng($im,$imagePathTemp);
                // Load image above save to merge image first
                $dest   = imagecreatefrompng($imagePathTemp);
                $src    = imagecreatefrompng($imagePath);
                imagecopy($dest, $src, 
                            ($new_width - $width)/2, 
                            ($new_height- $height)/2, 
                            0, 
                            0, 
                            $width, 
                            $height);
                 // Output and free from memory

                imagepng($dest,$imagePath);
                imagedestroy($im);
                imagedestroy($src);
                imagedestroy($dest);
                break;
             default:
                    static::errorOther(static::ERROR_CODE_OTHER_1, 'image TYPE', __('Not support file Type: {$fileType}'));
                    return false;
                 break;
         }
        // Check to delete image TEMP
         if(file_exists($imagePathTemp)){
            unlink($imagePathTemp);
         }
         return $imagePath;

   
   /* list($width, $height) = getimagesize('image/anonymous_avatar.gif');
        $new_width = $height * 16 /9;
        $new_height = $height;

        $im = @imagecreate($new_width,  $new_height)
                or die('Cannot Initialize new GD image stream');
        $color = \Lib\Util::rgb2hex2rgb('#c7c81b');
        imagecolorallocate($im, $color['red'], $color['green'], $color['blue']);
        // Save the image
        imagepng($im, 'image/imagefilledrectangle.png');
        // Load image above save to merge image first
        $dest = imagecreatefrompng('image/imagefilledrectangle.png');
        $src= imagecreatefromgif('image/anonymous_avatar.gif');
        list($width, $height) = getimagesize('image/anonymous_avatar.gif');
        imagecopymerge($dest, $src, 
            ($new_width - $width)/2, 
            ($new_height- $height)/2, 
            0, 
            0, 
            $width, 
            $height, 100);

        // Output and free from memory
        header('Content-Type: image/gif');
        imagepng($dest,'image/aaaa.png');
        imagedestroy($im);
        imagedestroy($src);*/
    }
    
     public function action_clean(){
        echo md5('http://rss.rssad.jp/rss/artclk/9FLW5bfpNz.D/45d674cc0e2bed109dd8bca50cd48a4e?ul=vC_XoyoJEbhxkX7lko1EgbqYKmtpZICVefq33ljPd6vv5xxLPqmB0awkFBqj1d3MY2tyqTs');
        exit;
        include_once APPPATH . "/config/auth.php";
        \DB::query("DELETE FROM user_facebook_informations WHERE user_id NOT IN (SELECT user_id FROM user_profiles)")->execute();
        \DB::query("DELETE FROM users WHERE id NOT IN (SELECT user_id FROM user_profiles)")->execute();
        echo 'OK';
        exit;
    }
    
    
}
