<?php

/**
 * Controller_Universities
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Universities extends \Controller_App
{
    /**
     *  Get detail of Universities
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Universities_Detail::getInstance()->execute();
    }

    /**
     *  Get all  Universities
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\Universities_All::getInstance()->execute();
    }

    /**
     *  Get list Universities by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Universities_List::getInstance()->execute();
    }

    /**
     *  Update disable field for Universities
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Universities_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new Universities
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Universities_AddUpdate::getInstance()->execute();
    }

}