<?php

/**
 * Controller for actions on follow company logs
 *
 * @package Controller
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowCompanyLogs extends \Controller_App
{
    /**
     * Get list of FollowCompanyLog 
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\FollowCompanyLogs_List::getInstance()->execute();
    }

    /**
     * Add new  FollowCompanyLog
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowCompanyLogs_Add::getInstance()->execute();
    }

}