<?php

/**
 * Controller_NewsFeedReadLogs - Controller for actions on NewsFeedReadLogs
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_NewsFeedReadLogs extends \Controller_App
{
    /**
     *   Get list NewsFeedReadLogs by action POST
     * 
     * @return boolean
     */
    public function post_list()
    {
        return \Bus\NewsFeedReadLogs_List::getInstance()->execute();
    }
    /**
     *  Get list NewsFeedReadLogs by action GET
     * 
     * @return boolean 
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
    /**
     *  Update or add new NewsFeedReadLogs
     * 
     * @return boolean 
     */
     public function post_addupdate()
    {
        return \Bus\NewsFeedReadLogs_AddUpdate::getInstance()->execute();
    }
    
}
