<?php

use Model\Common;

/**
 * Controller_Common_Index
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Common_Index extends \Controller_App {

    /**
     * Action index   
     *  
     * @author Le Tuan Tu
     * @return void 
     */
    public function action_index() {
        echo Common::add(10, 20);
    }

}
