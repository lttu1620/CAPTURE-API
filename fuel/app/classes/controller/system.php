<?php

/**
 * <Controller for systems>
 *
 * @package Controller
 * @created 2015-01-14
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */

//include(APPPATH . '/classes/lib/aws/aws-autoloader.php'); 

//use Aws\Ses\SesClient;

class Controller_System extends \Controller_App {

    /**
     *  Delete all cache of system
     * 
     * @return boolean 
     */
    public function action_deletecache() {
        return \Bus\Systems_DeleteCache::getInstance()->execute();
    }
    
    /**
     *  Run bacth in \tasks
     * 
     * @return boolean 
     */
    public function action_runbatch() {
        return \Bus\Systems_RunBatch::getInstance()->execute();
    }
    
    /**
     *  Get list processing of server
     * ˙
     * @return boolean 
     */
    public function action_ps() {
        return \Bus\Systems_Ps::getInstance()->execute();
    }
    
    /**
     *  Test send email of system
     * 
     * @return boolean 
     */
    public function action_email() {
        try
        {           
//            $to = 'thailvn@email.com';
//            if (Input::get('debug') == 1) {  
//                include(APPPATH . '/classes/lib/php-aws-ses-master/ses.php'); 
//                $con = new SimpleEmailService("AKIAJ5DQYYMFTZCYBR5A", "AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr", 'email-smtp.us-west-2.amazonaws.com');
//                $con->enableVerifyHost(false);
//                $con->enableVerifyPeer(false);
//                $con->verifyEmailAddress($to);
//            }            
//            if (Input::get('debug') == 2) {
//                include(APPPATH . '/classes/lib/php-aws-ses-master/ses.php'); 
//                $con = new SimpleEmailService("AKIAJ5DQYYMFTZCYBR5A", "AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr", 'email-smtp.us-west-2.amazonaws.com');
//                $con->enableVerifyHost(false);
//                $con->enableVerifyPeer(false);
//                $con->listVerifiedEmailAddresses();
//                $m = new SimpleEmailServiceMessage();
//                $m->addTo($to);
//                $m->setFrom($to);
//                $m->setSubject('Hello, world!');
//                $m->setMessageFromString('This is the message body.');
//                d($con->sendEmail($m), 1);
//            }
//            if (Input::get('debug') == 3) {
//                include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailService.php');
//                include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailServiceMessage.php');
//                include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailServiceRequest.php');
//                $con = new SimpleEmailService('AKIAJ5DQYYMFTZCYBR5A', 'AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr', 'email-smtp.us-west-2.amazonaws.com');
//                $con->enableVerifyHost(false);
//                $con->enableVerifyPeer(false);
//                 $con->verifyEmailAddress($to);
//                exit;
//                
//                $m = new SimpleEmailServiceMessage();
//                $m->addTo('thailvn@gmail.com');
//                $m->setFrom('Thai <diennguyen2002@gmail.com>');
//                $m->setSubject('You have got Email at ' . date('Y-m-d H:i'));
//                $m->setMessageFromString('Your message');
//                $ses->sendEmail($m);
//            }
//            
//            if (Input::get('debug') == 4) {
//                try { 
//                    $client = SesClient::factory(array(                    
//                        'region'   => 'us-west-2',
//                        'key'      => 'AKIAJ5DQYYMFTZCYBR5A',
//                        'secret'   => 'AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr'
//                    )); 
//
//                    $client->verifyEmailIdentity( array(
//                        'EmailAddress' => $to
//                    ));
//                }
//                catch( Exception $e )
//                {
//                    p($e);
//                }
//                exit;
//                $emailSentId = $client->sendEmail(array(
//                    // Source is required
//                    'Source' => 'developer.php.vn@gmail.com',
//                    // Destination is required
//                    'Destination' => array(
//                        'ToAddresses' => array($to)
//                    ),
//                    // Message is required
//                    'Message' => array(
//                        // Subject is required
//                        'Subject' => array(
//                            // Data is required
//                            'Data' => 'SES Testing',
//                            'Charset' => 'UTF-8',
//                        ),
//                        // Body is required
//                        'Body' => array(
//                            'Text' => array(
//                                // Data is required
//                                'Data' => 'My plain text email',
//                                'Charset' => 'UTF-8',
//                            ),
//                            'Html' => array(
//                                // Data is required
//                                'Data' => '<b>My HTML Email</b>',
//                                'Charset' => 'UTF-8',
//                            ),
//                        ),
//                    ),
//                    'ReplyToAddresses' => array('atem.vn@gmail.com' ),
//                    'ReturnPath' => 'atem.vn@gmail.com'
//                ));
//                d($emailSentId, 1);
//                
//                $result = $client->sendEmail(array(
//                    // Source is required
//                    'Source' => $to,
//                    // Destination is required
//                    'Destination' => array(
//                        'ToAddresses' => array($to),
//                        //'CcAddresses' => array('string', ... ),
//                        //'BccAddresses' => array('string', ... ),
//                    ),
//                    // Message is required
//                    'Message' => array(
//                        // Subject is required
//                        'Subject' => array(
//                            // Data is required
//                            'Data' => 'Hello world',
//                            'Charset' => 'utf-8',
//                        ),
//                        // Body is required
//                        'Body' => array(
//                            'Text' => array(                               
//                                'Data' => 'Hello',                               
//                            ),
//                            //'Html' => array(
//                                // Data is required
//                                //'Data' => 'string',                               
//                            //),
//                        ),
//                    ),
//                    'ReplyToAddresses' => array(),
//                    'ReturnPath' => '',
//                ));
//                d($result, 1);
//            }
//            
//            Package::load('phpmailer');
//            $from = "thailvn@email.com";
//            $mail = new PHPMailer();
//            $mail->IsSMTP(true); // SMTP
//            $mail->SMTPAuth = true;  // SMTP authentication
//            $mail->Mailer = "smtp";
//            $mail->Host= "tls://email-smtp.us-west-2.amazonaws.com"; // Amazon SES
//            $mail->Port = 465;  // SMTP Port
//            $mail->Username = "AKIAJ5DQYYMFTZCYBR5A";  // SMTP  Username
//            $mail->Password = "AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr";  // SMTP Password
//            $mail->SetFrom($from, 'Thai');            
//            $mail->Subject = $subject = 'Hello world!';
//            $mail->MsgHTML($body = 'This is message content');
//            $address = $to = 'atem.vn@gmail.com';
//            $mail->AddAddress($address, $to);
//            d($mail->Send(), 1);           


//            include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailService.php');
//            include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailServiceMessage.php');
//            include(APPPATH . '/classes/lib/php-aws-ses-master/src/SimpleEmailServiceRequest.php');
//            $ses = new SimpleEmailService('AKIAJ5DQYYMFTZCYBR5A', 'AsACdSqFXoca3e6MHwARA1c7yI7cIc10GmJU57Lt7UGr', 'email-smtp.us-west-2.amazonaws.com');
//            $m = new SimpleEmailServiceMessage();
//            $m->addTo('thailvn@gmail.com');
//            $m->setFrom('Thai <diennguyen2002@gmail.com>');
//            $m->setSubject('You have got Email at ' . date('Y-m-d H:i'));
//            $m->setMessageFromString('Your message');
//            $ses->sendEmail($m);
//            p($ses, 1);            
        } 
        catch (\Exception $e)
        {
            p($e, 1);
        }
        exit;
    }
}
