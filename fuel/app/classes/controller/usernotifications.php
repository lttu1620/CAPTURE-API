<?php

/**
 * Controller for actions on User Notification
 *
 * @package Controller
 * @created 2014-12-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserNotifications extends \Controller_App
{
    /**
     *  Registion notification for user
     * 
     * @return boolean 
     */
	public function action_register()
	{
		return \Bus\UserNotifications_Register::getInstance()->execute();
	}

    /**
     *  Get list User Notificaions by condition
     * 
     * @return boolean 
     */
	public function action_list()
	{
		return \Bus\UserNotifications_List::getInstance()->execute();
	}

    /**
     *  Unregistion notification for user
     * 
     * @return boolean 
     */
	public function action_unregister()
	{
		return \Bus\UserNotifications_Unregister::getInstance()->execute();
	}
}