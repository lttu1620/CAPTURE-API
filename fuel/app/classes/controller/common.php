<?php

/**
 * Controller_Common
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Common extends \Controller_App {
    
    /**
     * Update disable field common
     *  
     * @return boolean    
     */
    public function action_disable() {

        echo Common::add(15, 20);
    }

}
