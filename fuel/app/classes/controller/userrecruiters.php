<?php

/**
 * Controller_UserRecruiters - Controller for actions on User_Recruiters
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_UserRecruiters extends \Controller_App
{

    /**
     *  Get list user recruiter by condition
     * 
     * @return boolean 
     */
    public function action_list()
    {
        return \Bus\UserRecruiters_List::getInstance()->execute();
    }

    /**
     *  Get detail of user recruiter
     * 
     * @return boolean 
     */
    public function action_detail()
    {
        return \Bus\UserRecruiters_Detail::getInstance()->execute();
    }

    /**
     *  Update disable field for user recruiter
     * 
     * @return boolean 
     */
    public function action_disable()
    {
        return \Bus\UserRecruiters_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new UserRecruiter
     * 
     * @return boolean 
     */
    public function action_addupdate()
    {
        return \Bus\UserRecruiters_AddUpdate::getInstance()->execute();
    }

    /**
     *  Update isAdmin field of user recruiter
     * 
     * @return boolean 
     */
    public function action_isAdmin()
    {
        return \Bus\UserRecruiters_ChangeAdmin::getInstance()->execute();
    }

    /**
     *  Update isApproved field of user recruiter
     * 
     * @return boolean 
     */
    public function action_isApproved()
    {
        return \Bus\UserRecruiters_ChangeApproved::getInstance()->execute();
    }
    
    /**
     *  Login for UserRecruiter
     * 
     * @return boolean 
     */
    public function action_login() {
        return \Bus\UserRecruiters_Login::getInstance()->execute();
    }
    
    /**
     *  Login facebook for user recruiter
     * 
     * @return boolean 
     */
    public function action_fblogin() {
        return \Bus\UserRecruiters_FbLogin::getInstance()->execute();
    }
    
    /**
     *  Get token of user recruiter when login by facebook account
     * 
     * @return boolean 
     */
    public function action_fblogintoken() {
        return \Bus\UserRecruiters_FbLoginToken::getInstance()->execute();
    }
}
