<?php

/**
 * Controller_Categories - Controller for actions on Category
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Categories extends \Controller_App
{
    
     /**
     * Get list Categories by action POST
     *  
     * @return boolean    
     */
    public function post_list()
    {
        return \Bus\Categories_List::getInstance()->execute();
    }

    /**
     * Get list Categories by action GET
     *  
     * @return boolean    
     */
    public function get_list()
    {
        return $this->post_list();
    }

    /**
     * Get all Categories by action POST
     *  
     * @return boolean    
     */
    public function post_all()
    {
        return \Bus\Categories_All::getInstance()->execute();
    }
    /**
     * Get all Categories by action GET
     *  
     * @return boolean    
     */
    public function get_all()
    {
        return $this->post_all();
    }

    /**
     * Get detail Categories by action POST
     *  
     * @return boolean   
     */
    public function post_detail()
    {
        return \Bus\Categories_Detail::getInstance()->execute();
    }
    /**
     * Get detail Categories by action GET
     *  
     * @return boolean    
     */
    public function get_detail()
    {
        return $this->post_detail();
    }

    /**
     * Update disable field for Categories by action POST
     *  
     * @return Category Object    
     */
    public function post_disable()
    {
        return \Bus\Categories_Disable::getInstance()->execute();
    }
    /**
     * Update or add new information for Categories by action GET
     *  
     * @return Category Object    
     */
    public function post_addupdate()
    {
        return \Bus\Categories_AddUpdate::getInstance()->execute();
    }

}
