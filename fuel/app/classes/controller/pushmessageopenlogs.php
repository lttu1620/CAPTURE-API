<?php

/**
 * Controller for actions on push message open logs
 *
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_PushMessageOpenLogs extends \Controller_App
{
    /**
     *  Get list of PushMessageOpenLogs by actiob GET
     * 
     * @return boolean 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list of PushMessageOpenLogs by actiob POST
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\PushMessageOpenLogs_List::getInstance()->execute();
    }

    /**
     *  Get detail of PushMessageOpenLogs by actiob GET
     * 
     * @return boolean 
     */
    public function get_detail() {
        return $this->post_detail();
    }

    /**
     *  Get detail of PushMessageOpenLogs by actiob POST
     * 
     * @return boolean 
     */
    public function post_detail() {
        return \Bus\PushMessageOpenLogs_Detail::getInstance()->execute();
    }

    /**
     *  Add new PushMessageOpenLogs
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\PushMessageOpenLogs_Add::getInstance()->execute();
    }

}