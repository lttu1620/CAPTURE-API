<?php

/**
 * Controller for actions on share logs
 *
 * @package Controller
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_ShareLogs extends \Controller_App
{
    /**
     *  Get list Sharelogs by action GET
     * 
     * @return boolean 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     *  Get list Sharelogs by action POST
     * 
     * @return boolean 
     */
    public function post_list() {
        return \Bus\ShareLogs_List::getInstance()->execute();
    }

    /**
     *  Add new ShareLogs
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\ShareLogs_Add::getInstance()->execute();
    }

}