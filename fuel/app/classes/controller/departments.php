<?php

/**
 * Controller_Departments
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Departments extends \Controller_App
{
    /**
     * Get detail for Department  
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Departments_Detail::getInstance()->execute();
    }

    /**
     * Get list for Department  
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Departments_List::getInstance()->execute();
    }

    /**
     * Update disable field for Department  
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\Departments_Disable::getInstance()->execute();
    }

    /**
     * Update or add new informatiob for Department  
     *  
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Departments_AddUpdate::getInstance()->execute();
    }

    /**
     * Get all for Department  
     *  
     * @return boolean
     */
    public function action_all() {
        return \Bus\Departments_All::getInstance()->execute();
    }
}