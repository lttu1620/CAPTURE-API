<?php

/**
 * Controller for actions on GroupSetting
 *
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author Dien Nguyen
 * @copyright Oceanize INC
 */
class Controller_GroupSettings extends \Controller_App {

    /**
     * Get all of GroupSettings by action POST
     *  
     * @return boolean
     */
    public function post_all() {
        return \Bus\GroupSettings_All::getInstance()->execute();
    }
    
    /**
     * Get all of GroupSettings by action GET
     *  
     * @return boolean
     */
    public function get_all(){
        return $this->post_all();
    }

    /**
     * Update disable field for GroupSettings by action POST
     *  
     * @return boolean
     */
    public function post_disable() {
        return \Bus\GroupSettings_Disable::getInstance()->execute();
    }
    
    /**
     * Update disable field for GroupSettings by action GET
     *  
     * @return boolean
     */
    public function get_disable(){
        return $this->post_disable();
    }

    /**
     * Update or add new information for GroupSettings by action POST
     *  
     * @return boolean
     */
    public function post_addupdate() {
        return \Bus\GroupSettings_AddUpdate::getInstance()->execute();
    }

    /**
     * Update or add new information for GroupSettings by action GET
     *  
     * @return boolean
     */
    public function get_addupdate() {
        return $this->post_addupdate();
    }

}
