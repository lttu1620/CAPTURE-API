<?php

/**
 * Controller for actions on News Comment Likes
 *
 * @package Controller
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsCommentLikes extends \Controller_App
{
    /**
     *  Get list of NewsCommentLikes
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\NewsCommentLikes_List::getInstance()->execute();
    }

    /**
     *  Update disable field of NewsCommentLikes
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsCommentLikes_Disable::getInstance()->execute();
    }
    
    /**
     *  Add new NewsCommentLikes

     * @return boolean 
     */
    public function action_add() {
        return \Bus\NewsCommentLikes_Add::getInstance()->execute();
    }

}