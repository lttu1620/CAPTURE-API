<?php

/**
 * Controller for actions on News Feed Favorites
 *
 * @package Controller
 * @created 2014-11-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsFeedFavorites extends \Controller_App
{
    /**
     *  Get list NewsFeedFavorites
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\NewsFeedFavorites_List::getInstance()->execute();
    }

    /**
     *  Update disable field of NewsFeedFavorites
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsFeedFavorites_Disable::getInstance()->execute();
    }

    /**
     *  Add new NewsFeedFavorites
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\NewsFeedFavorites_Add::getInstance()->execute();
    }

}