<?php

/**
 * Controller_Mail
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Mail extends \Controller_Rest {

    /**
     *  Send mail
     * 
     * @return boolean 
     */
    public function action_index() {
        try {
            //\Fuel\Core\Config::load('email');
            
            //p(\Fuel\Core\Config::get('phpmailer'), 1);
            
            if (Input::get('debug') == 1) {
                $result = \Email::forge(array('driver' => 'ses'))
                        ->to('thailvn@gmail.com')
                        ->from('info@capture-news.jp')
                        ->subject('Capture 仮登録')
                        ->body('Your message goes here.')
                        ->send();
                d($result, 1);
            }

            if (Input::get('debug') == 2) {                
                $body = '<Hello world!>';
                $email = \Email::forge();
                $email->body($body);
                $email->from('info@capture-news.jp', 'Info');
                $email->to('thailvn@gmail.com', 'Thai');
                d($email->send(), 1);        
            }
            
        } catch (Exception $e) {
            p($e, 1);
        }    
        exit;
    }

}
