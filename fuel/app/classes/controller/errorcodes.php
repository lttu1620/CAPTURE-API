<?php

/**
 * Controller for actions on error codes
 *
 * @package Controller
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_ErrorCodes extends \Controller_App
{
    /**
     * Get list for ErrorCodes by action GET
     *  
     * @return boolean
     */
    public function get_list() {
        return $this->post_list();
    }
    
    /**
     * Get list for ErrorCodes by action POST
     *  
     * @return boolean
     */
    public function post_list() {
        return \Bus\ErrorCodes_List::getInstance()->execute();
    }

    /**
     * Get all for ErrorCodes by action GET
     *  
     * @return boolean
     */
    public function get_all() {
        return $this->post_all();
    }

    /**
     * Get all for ErrorCodes by action POST
     *  
     * @return boolean
     */
    public function post_all() {
        return \Bus\ErrorCodes_All::getInstance()->execute();
    }

    /**
     * Update or add new information for ErrorCodes 
     *  
     * @return boolean
     */
    public function action_addUpdate() {
        return \Bus\ErrorCodes_AddUpdate::getInstance()->execute();
    }

}