<?php

/**
 * Controller for actions on news feed tag
 *
 * @package Controller
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsFeedTags extends \Controller_App
{
    /**
     *  Add new  NewsFeedTags 
     * 
     * @return boolean 
     */
    public function action_add()
    {
        return \Bus\NewsFeedTags_Add::getInstance()->execute();
    }

    /**
     *  Get all NewsFeedTags 
     * 
     * @return boolean 
     */
    public function action_all()
    {
        return \Bus\NewsFeedTags_All::getInstance()->execute();
    }

    /**
     *  Update disable field of NewsFeedTags
     * 
     * @return boolean 
     */
    public function action_disable()
    {
        return \Bus\NewsFeedTags_Disable::getInstance()->execute();
    }
}