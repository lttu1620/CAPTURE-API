<?php

/**
 * Controller for actions on News Sites
 *
 * @package Controller
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsSites extends \Controller_App
{
    /**
     *  Get detail of NewsSites
     * 
     * @return boolean  
     */
    public function action_detail() {
        return \Bus\NewsSites_Detail::getInstance()->execute();
    }

    /**
     *  Get all of NewsSites
     * 
     * @return boolean 
     */
    public function action_all() {
        return \Bus\NewsSites_All::getInstance()->execute();
    }

    /**
     *  Get list of NewsSites
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\NewsSites_List::getInstance()->execute();
    }

    /**
     *  Update disable field for NewsSites
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsSites_Disable::getInstance()->execute();
    }

    /**
     *   Update or add new NewsSites
     * 
     * @return boolean
     */
    public function action_addUpdate() {
        return \Bus\NewsSites_AddUpdate::getInstance()->execute();
    }
    
    /**
     *  Update counter for NewsSites
     * @return boolean 
     */
    public function action_updateCounter() {
        return \Bus\NewsSites_UpdateCounter::getInstance()->execute();
    }

    /**
     * Set public for tadacopy
     *
     * @return boolean
     */
    public function action_isTadacopy()
    {
        return \Bus\NewsSites_IsTadacopy::getInstance()->execute();
    }
}