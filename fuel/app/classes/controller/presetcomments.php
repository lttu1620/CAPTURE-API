<?php

/**
 * Controller for actions on Preset Comment
 *
 * @package Controller
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_PresetComments extends \Controller_App
{
    /**
     *  Get list of PresetComments
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\PresetComments_List::getInstance()->execute();
    }

    /**
     *  Update or add new PresetComments
     * 
     * @return boolean 
     */
    public function action_addUpdate() {
        return \Bus\PresetComments_AddUpdate::getInstance()->execute();
    }

    /**
     *  Get detail of PresetComments
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\PresetComments_Detail::getInstance()->execute();
    }

}