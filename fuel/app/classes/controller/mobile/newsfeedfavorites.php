<?php

/**
 * Controller for actions on News Feed Favorites (in mobile)
 *
 * @package Controller
 * @created 2015-01-22
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Mobile_NewsFeedFavorites extends \Controller_App
{
    /**
     * Action list   
     *  
     * @author Le Tuan Tu 
     * @return void
     */
    public function action_list()
    {
        return \Bus\Mobile_NewsFeedFavorites_List::getInstance()->execute();
    }
}
