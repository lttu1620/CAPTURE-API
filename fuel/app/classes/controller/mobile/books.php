<?php

/**
 * Controller_Mobile_Books - Controller for actions on books (just for reference)
 * 
 * @package Controller
 * @created 2014-11-13
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Mobile_Books extends \Controller_App {

    /**
     * Action index   
     *  
     * @author thailh 
     * @return void
     */
    public function action_index() {        
        return \Bus\Books_List::getInstance()->execute();
    }
    
    /**
     * Action detail   
     *  
     * @author thailh 
     * @return void
     */
    public function action_detail() {        
        return \Bus\Mobile_Books_Detail::getInstance()->execute();
    }

    /**
     * Action all   
     *  
     * @author thailh 
     * @return void
     */
    public function action_all() {
        return \Bus\Books_All::getInstance()->execute();
    }
    
    /**
     * Action disable   
     *  
     * @author thailh 
     * @return void
     */
    public function action_disable() {        
        return \Bus\Books_Disable::getInstance()->execute();
    }
    
    /**
     * Action addupdate   
     *  
     * @author thailh 
     * @return void
     */
    public function action_addupdate() {        
        return \Bus\Books_AddUpdate::getInstance()->execute();
    }
}