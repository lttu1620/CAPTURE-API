<?php

/**
 * Controller_Mobile_CompanyUserViewLogs - Controller for actions on CompanyUserViewLogs
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Mobile_CompanyUserViewLogs extends \Controller_App
{

    /**
     * Action post_list   
     *  
     * @author truongnn 
     * @return void
     */
    public function post_list()
    {
        return \Bus\Mobile_CompanyUserViewLogs_List::getInstance()->execute();
    }
    
    /**
     * Action get_list   
     *  
     * @author truongnn 
     * @return void
     */
    public function get_list()
    {
        return $this->post_list();
    }
    
    /**
     * Action post_addupdate   
     *  
     * @author truongnn 
     * @return void
     */
    public function post_addupdate()
    {
        return \Bus\Mobile_CompanyUserViewLogs_AddUpdate::getInstance()->execute();
    }
    
}
