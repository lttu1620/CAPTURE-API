<?php

/**
 * Controller for actions on User (in mobile)
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Mobile_Users extends \Controller_App
{
    /**
     * Action detail   
     *  
     * @author Le Tuan Tu
     * @return void 
     */
	public function action_detail()
	{
		return \Bus\Mobile_Users_Detail::getInstance()->execute();
	}

}
