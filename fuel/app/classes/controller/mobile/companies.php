<?php

/**
 * Controller for actions on Company (in mobile)
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Mobile_Companies extends \Controller_App
{
    
    /**
     * Action detail   
     *  
     * @author Le Tuan Tu 
     * @return void
     */
    public function action_detail()
    {
        return \Bus\Mobile_Companies_Detail::getInstance()->execute();
    }

    /**
     * Action recruiters   
     *  
     * @author Le Tuan Tu
     * @return void 
     */
    public function action_recruiters()
    {
        return \Bus\Mobile_Companies_Recruiters::getInstance()->execute();
    }

    /**
     * Action newsFeedPopular   
     *  
     * @author Le Tuan Tu 
     * @return void
     */
    public function action_newsFeedPopular()
    {
        return \Bus\Mobile_Companies_NewsFeedPopular::getInstance()->execute();
    }

    /**
     * Action newsFeedNewer   
     *  
     * @author Le Tuan Tu 
     * @return void
     */
    public function action_newsFeedNewer()
    {
        return \Bus\Mobile_Companies_NewsFeedNewer::getInstance()->execute();
    }
}
