<?php

/**
 * Controller for the follow company on mobile
 *
 * @package Controller
 * @created 2015-01-22
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Mobile_FollowCompanies extends \Controller_App {

    /**
     * Action get_list   
     *  
     * @author truongnn
     * @return void 
     */
    public function get_list() {
        return $this->post_list();
    }
    
    /**
     * Action post_list   
     *  
     * @author truongnn 
     * @return void
     */
    public function post_list() {
        return \Bus\Mobile_FollowCompanies_List::getInstance()->execute();
    }

}
