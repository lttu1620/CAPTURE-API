<?php

/**
 * Controller_Mobile_Followcategories - Controller for actions on FollowCategories on mobile
 *
 * @package Controller
 * @created 2015-01-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Controller_Mobile_Followcategories extends \Controller_App 
{

    /**
     * Action list   
     *  
     * @author tuancd
     * @return void 
     */
    public function action_list() 
    {
        return \Bus\Mobile_FollowCategories_List::getInstance()->execute();
    }

    /**
     * Action update   
     *  
     * @author tuancd
     * @return void 
     */
    public function post_update() 
    {
        return \Bus\Mobile_FollowCategories_Update::getInstance()->execute();
    }

}
