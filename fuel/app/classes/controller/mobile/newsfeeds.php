<?php

/**
 * Controller_Mobile_NewsFeeds - Controller for actions on NewsFeeds on mobile
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Mobile_NewsFeeds extends \Controller_App
{

     /**
     * Action list   
     *  
     * @author diennvt 
     * @return void
     */
    public function action_list()
    {
        return \Bus\Mobile_NewsFeeds_List::getInstance()->execute();
    }   

     /**
     * Action detail   
     *  
     * @author diennvt 
     * @return void
     */
    public function action_detail()
    {
        return \Bus\Mobile_NewsFeeds_Detail::getInstance()->execute();
    }    
   
}
