<?php

/**
 * Controller for actions on follow subcategory logs
 *
 * @package Controller
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowSubcategoryLogs extends \Controller_App
{
    /**
     * Get list of FollowSubcategoryLogs 
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\FollowSubcategoryLogs_List::getInstance()->execute();
    }

    /**
     * Add news FollowSubcategoryLogs 
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowSubcategoryLogs_Add::getInstance()->execute();
    }

}