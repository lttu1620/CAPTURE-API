<?php

namespace Bus;

/**
 * <NewsFeeds_schedule - API to set startdate and end date for NewsFeeds>
 *
 * @package Bus
 * @created 2015-04-02
 * @version 1.0
 * @author Tuan cao
 * @copyright Oceanize INC
 */
class NewsFeeds_Schedule extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_feed_id',
    );

    //check number
    protected $_number_format = array(       
        'news_feed_id'
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::set_schedule_for_news_feed($data);
            return $this->result(\Model_News_Feed::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
