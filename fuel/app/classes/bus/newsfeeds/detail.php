<?php

namespace Bus;

/**
 * <NewsFeeds_Detail - API to get detail of NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeeds_Detail extends BusAbstract
{
    //check require
    protected $_required = array(
        'id',
    );
    
    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'id',
    );
    
    public function operateDB($data)
    {
        try
        {   
            $this->_response = \Model_News_Feed::get_detail($data['id']);
            if ($this->_response && !empty($data['log']) && !empty($data['user_id'])) 
            {   
                $param['user_id'] = $data['user_id'];
                $param['news_feed_id'] = $data['id'];
                $param['share_type'] = '0';
                \Model_News_Feed_View_Log::add_update($param);
            }
            return $this->result(\Model_News_Feed_Favorite::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
