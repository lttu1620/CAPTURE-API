<?php

namespace Bus;

/**
 * <NewsFeeds_List - API to get list of NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeeds_List extends BusAbstract
{
    //check length
    protected $_length = array(       
        'disable' => 1,
    );
    
    //check number
    protected $_number_format = array(       
        'disable',
        'page',
        'limit',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::get_list($data);
            return $this->result(\Model_News_Feed::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
