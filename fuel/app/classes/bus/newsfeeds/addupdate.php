<?php

namespace Bus;

/**
 * <NewsFeeds_AddUpdate - API to get list of NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeeds_AddUpdate extends BusAbstract
{

    //check require
    protected $_required = array(
        'title',
        'short_content',
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'news_site_id' => array(1, 11),
        'short_content' => array(0, 255),
        'image_url' => array(0, 255),
        'distribution_date' => array(0, 11),
        'favorite_count' => array(0, 11),
        'title' => array(0, 255),
    );
    //check number
    protected $_number_format = array(
        'id',
        'news_site_id',
        'favorite_count',
        'distribution_date',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::add_update($data);
            return $this->result(\Model_News_Feed::error());            
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
