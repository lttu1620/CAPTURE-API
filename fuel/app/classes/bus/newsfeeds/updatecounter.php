<?php

namespace Bus;

/**
 * update counter for anything about news feed
 *
 * @package Bus
 * @created 2015-01-15
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeeds_UpdateCounter extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_feed_id'
    );

    /**
     * call function update_counter() from model News Feed
     *
     * @created 2015-01-15
     * @updated 2015-01-15
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool|true
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed::update_counter($data);
            return $this->result(\Model_News_Feed::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
