<?php

namespace Bus;

/**
 * <NewsFeeds_Public - API to disable or enable a NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeeds_Public extends BusAbstract
{
    //check require
    protected $_required = array(
        'id',
        'is_public',
    );

    //check length
    protected $_length = array(        
        'is_public' => 1,
    );
    
    //check number
    protected $_number_format = array(       
        'is_public'
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::is_public($data);
            return $this->result(\Model_News_Feed::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
