<?php

namespace Bus;

/**
 * <NewsFeeds_Public - API to disable or enable a NewsFeeds>
 *
 * @package Bus
 * @created 2015-04-03
 * @version 1.0
 * @author Tuan cao
 * @copyright Oceanize INC
 */
class NewsFeeds_IsvalidPublic extends BusAbstract {

    //check require
    protected $_required = array(
        'news_feed_id',
    );

    public function operateDB($data) {
        try {
            $options = array('where' => array(
                    'news_feed_id' => $data['news_feed_id'],
                    'disable'      => 0
            ));
            $this->_response = !empty(\Model_News_Feed_Category::find('first', $options)) ? 1 : 0;
            return $this->result(\Model_News_Feed_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
