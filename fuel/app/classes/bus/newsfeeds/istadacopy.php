<?php

namespace Bus;

/**
 * Set public for tadacopy
 *
 * @package Bus
 * @created 2015-09-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeeds_IsTadacopy extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'is_tadacopy',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'        => array(1, 11),
        'is_tadacopy' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'is_tadacopy',
    );

    /**
     * Call function is_tadacopy() from model News Feed
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed::is_tadacopy($data);
            return $this->result(\Model_News_Feed::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
