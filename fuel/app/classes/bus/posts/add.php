<?php

namespace Bus;

/**
 * Add info for Post
 *
 * @package Bus
 * @created 2015-08-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Posts_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'post_title',
        'post_content',
        'post_description',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'post_title'       => array(1, 255),
        'post_description' => array(1, 255),
    );

    /**
     * Call function add() from model Post
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Post::add($data);
            return $this->result(\Model_Post::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
