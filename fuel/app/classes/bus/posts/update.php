<?php

namespace Bus;

/**
 * Update info for Post
 *
 * @package Bus
 * @created 2015-08-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Posts_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'news_feed_id',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'               => array(1, 11),
        'news_feed_id'     => array(1, 11),
        'post_title'       => array(1, 255),
        'post_description' => array(1, 255),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'news_feed_id',
    );

    /**
     * Call function update() from model Post
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Post::set_update($data);
            return $this->result(\Model_Post::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
