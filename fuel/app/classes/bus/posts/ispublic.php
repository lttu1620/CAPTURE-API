<?php

namespace Bus;

/**
 * Public/Private a post
 *
 * @package Bus
 * @created 2015-08-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Posts_IsPublic extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'is_public',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'        => array(1, 11),
        'is_public' => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'is_public',
    );

    /**
     * Call function is_public() from model Post
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Post::is_public($data);
            return $this->result(\Model_Post::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
