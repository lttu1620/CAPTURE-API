<?php

namespace Bus;

/**
 * Get list Post (using array count)
 *
 * @package Bus
 * @created 2015-08-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Posts_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'post_title'       => array(1, 255),
        'post_description' => array(1, 255),
        'is_public'        => 1,
        'disable'          => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'is_public',
        'disable',
        'limit',
        'page',
    );

    /**
     * Call function get_list() from model Post
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Post::get_list($data);
            return $this->result(\Model_Post::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
