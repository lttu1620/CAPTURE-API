<?php

namespace Bus;

/**
 * add or update info for preset comments
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PresetComments_AddUpdate extends BusAbstract
{
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'comments' => array(1, 40),
    );
    
    //check number
    protected $_number_format = array(
        'id',
    );
    /**
     * call function add_update() from model Preset Comments
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Preset_Comment::add_update($data);
            return $this->result(\Model_Preset_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
