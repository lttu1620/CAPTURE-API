<?php

namespace Bus;

/**
 * get list news feed favorite
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedFavorites_List extends BusAbstract
{
    protected $_length = array(
        'user_name' => array(0, 40),
        'site_name' => array(1, 40),
        'title' => array(1, 40),
        'news_site_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
    );
    
    protected $_number_format = array(
        'news_site_id',
        'news_feed_id',
        'user_id',
        'disable',
        'page',
        'limit',
    );
    
     //check email
    protected $_email_format = array(
        'email',
    );
    
    /**
     * call function get_list() from model News Feed Favorite
     *
     * @created 2014-11-25
     * @updated 2014-11-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Favorite::get_list($data);
            return $this->result(\Model_News_Feed_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
