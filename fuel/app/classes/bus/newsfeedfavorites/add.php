<?php

namespace Bus;

/**
 * add or update info for news feed favorites
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedFavorites_Add extends BusAbstract
{
    protected $_required = array(
        'news_feed_id',
        'user_id'
    );
    
    protected $_length = array(
        'news_feed_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'news_feed_id',
        'user_id'
    );
    
    /**
     * call function add() from model News feed favorite
     *
     * @created 2014-11-25
     * @updated 2014-11-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Favorite::add($data);
            return $this->result(\Model_News_Feed_Favorite::error());            
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}