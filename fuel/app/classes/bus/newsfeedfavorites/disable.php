<?php

namespace Bus;

/**
 * Enable/Disable News feed favorites
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedFavorites_Disable extends BusAbstract
{
    protected $_length = array(
        'id' => array(1, 11), 
        'news_feed_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
    );
    
    protected $_number_format = array(
        'id',
        'news_feed_id',
        'user_id',
        'disable',
    );

    /**
     * call function disable() from model News Feed Favorite
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Favorite::disable($data);
            return $this->result(\Model_News_Feed_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
