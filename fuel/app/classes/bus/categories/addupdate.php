<?php

namespace Bus;

/**
 * <Categories_AddUpdate - API to get list of Categories>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Categories_AddUpdate extends BusAbstract
{

    protected $_required = array(
        'name',
    );

    protected $_url_format = array(
        'image_url'
    );

    protected $_number_format = array(
        'id'
    );

    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Category::add_update($data);
            return $this->result(\Model_Category::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
