<?php

namespace Bus;

/**
 * <Categories_Detail - API to get detail of Categories>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Categories_Detail extends BusAbstract
{

    protected $_required = array(
        'id',
    );

    protected $_number_format = array(
        'id'
    );

    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Category::find($data['id']);
            if ($this->_response == null) { 
                $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                return false;
            }
            return $this->result(\Model_Category::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
