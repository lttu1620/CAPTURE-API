<?php

namespace Bus;

/**
 * <Categories_Disable - API to disable or enable a Category>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Categories_Disable extends BusAbstract
{

    protected $_required = array(
        'id',
        'disable',
    );
    
    // check number
    protected $_number_format = array(
        'disable'
    );

    // check length
    protected $_length = array(
        'disable' => 1
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Category::disable($data);
            return $this->result(\Model_Category::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
