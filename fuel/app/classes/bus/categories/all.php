<?php

namespace Bus;

/**
 * get list categories
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Categories_All extends BusAbstract
{
    /**
     * call function get_all() from model Category
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $conditions['where'][] = array(
                'disable' => 0
            );
            $this->_response = \Model_Category::find('all', $conditions);
            return $this->result(\Model_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
