<?php

namespace Bus;

/**
 * <Categories_List - API to get list of Categories>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Categories_List extends BusAbstract
{
    protected $_number_format = array(
        'page',
        'limit'
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Category::get_list($data);
            return $this->result(\Model_Category::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
