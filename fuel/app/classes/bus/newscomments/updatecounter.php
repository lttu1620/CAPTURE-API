<?php

namespace Bus;

/**
 * update counter for anything about news comment
 *
 * @package Bus
 * @created 2015-01-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_UpdateCounter extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_comment_id'
    );

    /**
     * call function update_counter() from model News Comment
     *
     * @created 2015-01-26
     * @updated 2015-01-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool|true
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::update_counter($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
