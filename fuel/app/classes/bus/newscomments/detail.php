<?php

namespace Bus;

/**
 * get detail news comments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_Detail extends BusAbstract
{
    protected $_required = array(
        'id'
    );

    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'id'
    );
    /**
     * get detail news comments by id
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::get_detail($data['id']);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
