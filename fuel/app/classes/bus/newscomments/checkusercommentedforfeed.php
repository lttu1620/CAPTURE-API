<?php

namespace Bus;

/**
 * check user commented for feed
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_CheckUserCommentedForFeed extends BusAbstract
{
    //check require
    protected $_required = array(
        'feed_id',
        'user_id',
    );
    
     //check length
    protected $_length = array(
        'feed_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'feed_id',
        'user_id'
    );

    /**
     * call function check_user_commented_for_feed() from model News Comment
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::check_user_commented_for_feed($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
