<?php

namespace Bus;

/**
 * get list news comments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_List extends BusAbstract
{
    //check length
    protected $_length = array(
        'news_feed_id' => array(0, 11),
        'user_id' => array(0, 11),
        'disable' => 1,
        'username' => array(0, 40),
        'nickname' => array(0, 40),
        'news_feed_title' => array(0, 255),
    );
    
    //check number
    protected $_number_format = array(
        'news_feed_id',
        'user_id',
        'disable',
        'limit',
        'page',
    );
    
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );
    
    /**
     * call function get_list() from model NewsComment
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::get_list($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
