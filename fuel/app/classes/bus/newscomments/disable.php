<?php

namespace Bus;

/**
 * Enable/Disable News comments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_Disable extends BusAbstract
{
    //check require
    protected $_required = array(
        'id',
        'disable',
    );

    //check length
    protected $_length = array(
        'disable' => 1,
    );
    
    //check number
    protected $_number_format = array(
        'disable',
    );

    /**
     * call function disable() from model News Comment
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::disable($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
