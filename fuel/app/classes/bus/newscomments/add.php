<?php

namespace Bus;

/**
 * add info for news comments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_Add extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_feed_id',
        'user_id',
        'comment'
    );
    
     //check length
    protected $_length = array(
        'news_feed_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'news_feed_id',
        'user_id'
    );

    /**
     * call function add() from model News Comment
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::add($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
