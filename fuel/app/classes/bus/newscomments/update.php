<?php

namespace Bus;

/**
 * update info for news comments
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsComments_Update extends BusAbstract
{
    //check require
    protected $_required = array(
        'id'
    );

    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'id'
    );
    
    /**
     * call function update() from model News Comment
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment::edit($data);
            return $this->result(\Model_News_Comment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
