<?php

namespace Bus;

/**
 * Enable/Disable a news feed tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedTags_Disable extends BusAbstract
{
    //check require
    protected $_required = array(
        'tag_id',
        'news_feed_id',
        'disable'
    );

    //check length
    protected $_length = array(
        'tag_id'       => array(1, 11),
        'news_feed_id' => array(1, 11),
        'disable'      => 1
    );

    //check number
    protected $_number_format = array(
        'tag_id',
        'news_feed_id',
        'disable'
    );

    /**
     * call function disable() from model Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Tag::disable($data);
            return $this->result(\Model_News_Feed_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
