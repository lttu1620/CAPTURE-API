<?php

namespace Bus;

/**
 * get all tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedTags_All extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_feed_id'
    );

    //check length
    protected $_length = array(
        'news_feed_id' => array(1, 11)
    );

    //check number
    protected $_number_format = array(
        'news_feed_id'
    );

    /**
     * call function get_all() from model News Feed Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Tag::get_all($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
