<?php

namespace Bus;

/**
 * add or update info for news feed tags
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedTags_Add extends BusAbstract
{
    //check require
    protected $_required = array(
        'tag_id',
        'news_feed_id'
    );

    //check length
    protected $_length = array(
        'tag_id'       => array(1, 11),
        'news_feed_id' => array(1, 11)
    );

    //check number
    protected $_number_format = array(
        'tag_id',
        'news_feed_id'
    );

    /**
     * call function add_update() from model News Feed Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Tag::add($data);
            return $this->result(\Model_News_Feed_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
