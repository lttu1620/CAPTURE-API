<?php

namespace Bus;

/**
 * <SubCategories_AddUpdate - API to get list of SubCategories>
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class SubCategories_AddUpdate extends BusAbstract
{

    protected $_required = array(
        'category_id',
        'name',
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'category_id' => array(1, 11),
        'name' => array(1, 40),
        'image_url' => array(1, 255),
    );
    //check number
    protected $_number_format = array(
        'id',
        'category_id',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Subcategory::add_update($data);
            return $this->result(\Model_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
