<?php

namespace Bus;

/**
 * <SubCategories_Detail - API to get detail of SubCategories>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class SubCategories_Detail extends BusAbstract
{

    protected $_required = array(
        'id',
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'id',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Subcategory::find($data['id']);
            if ($this->_response == null) { 
                $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                return false;
            }
            return $this->result(\Model_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
