<?php

namespace Bus;

/**
 * Enable/Disable Company
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Companies_Disable extends BusAbstract
{
    /**
     * call function disable() from model Company
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company::disable($data);
            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
