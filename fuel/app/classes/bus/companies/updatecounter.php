<?php

namespace Bus;

/**
 * update info follow_count for company
 *
 * @package Bus
 * @created 2015-01-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Companies_UpdateCounter extends BusAbstract
{
    protected $_required = array(
        'company_id' //multiple value is OK (ex: 1,2,3,4)
    );
   
    /**
     * call function update_counter() from model company
     *
     * @created 2015-01-15
     * @updated 2015-01-15
     * @access public
     * @author diennvt
     * @param $data
     * @return bool
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company::update_counter($data);
            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
