<?php

namespace Bus;

/**
 * get list company
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Companies_All extends BusAbstract
{
    /**
     * call function get_all() from model Company
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {                      
            $options['where'] = array(
                'disable' => 0
            );
            $options['order_by'] = array(
                'name' => 'ASC'
            );           
            $this->_response = \Model_Company::find('all', $options);
            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
