<?php

namespace Bus;

/**
 * add or update info for company
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Companies_AddUpdate extends BusAbstract
{
    protected $_url_format = array(
        'corporate_url'
    );

    protected $_number_format = array(
        'user_id'
    );

    /**
     * call function add_update() from model Company
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company::add_update($data);
            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}