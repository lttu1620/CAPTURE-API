<?php

namespace Bus;

/**
 * get list company category
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CompanyCategories_All extends BusAbstract
{
    /**
     * call function get_all() from model Company Categories
     *
     * @created 2014-12-12
     * @updated 2014-12-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $options['where'] = array(
                'disable' => 0
            );
            $this->_response = \Model_Company_Category::find('all', $options);
            return $this->result(\Model_Company_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
