<?php

namespace Bus;

/**
 * add info for company category
 *
 * @package Bus
 * @created 2014-12-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CompanyCategories_Add extends BusAbstract
{
    protected $_required = array(
        'category_id',
        'company_id'
    );

    protected $_number_format = array(
        'category_id',
        'company_id'
    );

    /**
     * call function add() from model Company Categories
     *
     * @created 2014-12-11
     * @updated 2014-12-11
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company_Category::add($data);
            return $this->result(\Model_Company_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
