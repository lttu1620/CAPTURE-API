<?php

namespace Bus;

/**
 * get list company category
 *
 * @package Bus
 * @created 2014-12-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CompanyCategories_List extends BusAbstract
{
    protected $_number_format = array(
        'page',
        'limit',
        'company_id'
    );

    /**
     * call function get_list() from model company category
     *
     * @created 2014-12-11
     * @updated 2014-12-11
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company_Category::get_list($data);
            return $this->result(\Model_Company_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
