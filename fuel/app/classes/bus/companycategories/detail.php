<?php

namespace Bus;

/**
 * get detail company category
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class CompanyCategories_Detail extends BusAbstract
{
    protected $_required = array(
        'id'
    );

    protected $_number_format = array(
        'id'
    );

    /**
     * get detail company categories by id
     *
     * @created 2014-12-12
     * @updated 2014-12-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company_Category::get_detail($data);
            return $this->result(\Model_Company_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
