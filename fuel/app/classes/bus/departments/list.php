<?php

namespace Bus;

/**
 * get list department
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_List extends BusAbstract
{
    protected $_number_format = array(
        'page',
        'limit',
        'university_id'
    );

    /**
     * call function get_list() from model Department
     *
     * @created 2014-11-21
     * @updated 2014-11-21
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Department::get_list($data);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
