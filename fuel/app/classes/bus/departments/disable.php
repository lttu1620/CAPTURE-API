<?php

namespace Bus;

/**
 * Enable/Disable Department
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_Disable extends BusAbstract
{
    /**
     * call function disable() from model Department
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Department::disable($data);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
