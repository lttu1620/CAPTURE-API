<?php

namespace Bus;

/**
 * get company detail tada copy relation
 *
 * @package Bus
 * @created 2015-01-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_CompanyDetail extends BusAbstract
{
    protected $_required = array(
        'company_id',
        'tadacopy_app_id',
        'comment_order_by'
    );

    protected $_number_format = array(
        'company_id'
    );

    /**
     * get company detail info by company id
     *
     * @created 2015-01-12
     * @updated 2015-01-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $data['apply_app_id'] = $data['tadacopy_app_id'];
            $this->_response      = \Model_Company::get_company_detail_for_tada_copy($data);
            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
