<?php

namespace Bus;

/**
 * add log after calling API
 *
 * @package Bus
 * @created 2015-01-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_ExternalRelationLogs extends BusAbstract
{

	protected $_required = array(
		'called_api_name',
		'apply_app_id',
		'called_from'
	);

	/**
	 * add log after calling API
	 *
	 * @created 2015-01-12
	 * @updated 2015-01-12
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_External_Relation_Log::insert($data);
			return $this->result(\Model_External_Relation_Log::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
