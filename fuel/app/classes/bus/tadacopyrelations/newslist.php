<?php

namespace Bus;

/**
 * get detail tada copy relation
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_NewsList extends BusAbstract
{
	protected $_required = array(
		'tadacopy_app_id'
	);

	protected $_number_format = array(
		'page'
	);

	//check date
	protected $_date_format = array(
		'list_date' => 'Ymd',
	);

	/**
	 * get detail info by date
	 *
	 * @created 2014-12-12
	 * @updated 2014-12-12
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$data['apply_app_id'] = $data['tadacopy_app_id'];
			$this->_response = \Model_News_Feed::get_news_list_detail_for_tada_copy($data);
			return $this->result(\Model_News_Feed::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
