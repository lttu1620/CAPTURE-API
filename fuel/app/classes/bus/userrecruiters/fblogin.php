<?php

namespace Bus;

/**
 * <UserRecruiters_FbLogin - Login facebook for user recruiter>
 *
 * @package Bus
 * @created 2014-12-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_FbLogin extends BusAbstract
{

    protected $_required = array(
        'email', // email which is registered facebook
        'id'//id of facebook
    );
    protected $_email_format = array(
        'email',
    );

    public function operateDB($data)
    {
        try
        {
            $result = \Model_User::login_facebook($data, 1);
            if (!empty($result))
            {
                $result['token'] = \Model_Authenticate::addupdate(array(
                            'user_id' => $result['id'],
                            'regist_type' => 'user'
                ));
            }
            $this->_response = $result;
            return $this->result(\Model_User::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
