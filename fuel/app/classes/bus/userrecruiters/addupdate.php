<?php

namespace Bus;

/**
 * <UserRecruiters_AddUpdate - API to get list of UserRecruiters>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_AddUpdate extends BusAbstract
{
    protected $_length = array(
        'id' => array(0, 11),
        'company_id'=> array(0, 11),
        'user_id'=> array(0, 11),
        'introduction_text' => array(0, 255),
        'is_admin'=> 1 ,
        'is_approved' => 1,
        'disable' => 1,
    );
    
     protected $_number_format = array(
        'id',
        'company_id',
        'user_id',
        'is_admin',
        'is_approved',
        'disable',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Recruiter::add_update($data);
            return $this->result(\Model_User_Recruiter::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
