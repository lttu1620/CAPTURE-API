<?php

namespace Bus;

/**
 * <UserRecruiters_List - API to get list of Recruiters>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_List extends BusAbstract
{
    
    protected $_length = array(
        'company_id' => array(0, 11),
        'user_id' => array(0, 11),
        'is_admin' => 1,
        'is_approved' => 1,
        'disable' => 1,
    );
    protected $_number_format = array(
        'company_id',
        'user_id',
        'is_admin',
        'is_approved',
        'disable',
    );
    protected $_email_format = array(
        'email',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Recruiter::get_list($data);
            return $this->result(\Model_User_Recruiter::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
