<?php

namespace Bus;

/**
 * enable/disable property isAdmin
 *
 * @package Bus
 * @created 2014-12-22
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserRecruiters_ChangeAdmin extends BusAbstract
{
	protected $_required = array(
		'id',
		'is_admin',
	);
        
    protected $_length = array(
        'id' => array(0, 11),
        'is_admin'=> 1 ,
    );

    protected $_number_format = array(
        'id',
        'is_admin',
    );
    
	/**
	 * call function disableIsAdmin() from model User Recruiter
	 *
	 * @created 2014-12-22
	 * @updated 2014-12-22
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Recruiter::disableIsAdmin($data);
			return $this->result(\Model_User_Recruiter::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
