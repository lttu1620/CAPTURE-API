<?php

namespace Bus;

/**
 * <UserRecruiters_Disable - API to disable or enable a UserRecruiters>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_Disable extends BusAbstract
{

    protected $_required = array(
        'id',
        'disable',
    );
    
    protected $_length = array(
        'disable' => 1,
    );
    
     protected $_number_format = array(
        'disable',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Recruiter::disable($data);
            return $this->result(\Model_User_Recruiter::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
