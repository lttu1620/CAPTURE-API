<?php

namespace Bus;

/**
 * <UserRecruiters_Detail - API to get detail of Recruiters>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_Detail extends BusAbstract
{    
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'id',
        'user_id',
    );

    public function operateDB($data)
    {
        try
        {
            if (isset($data['id'])) {
                $this->_response = \Model_User_Recruiter::find($data['id']);
                if ($this->_response == null) { 
                    $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                    return false;
                }
            } elseif (isset($data['user_id'])) {
                $options['where'] = array(
                    'user_id' => $data['user_id']
                );
                $this->_response = \Model_User_Recruiter::find('first', $options);  
                if ($this->_response == null) { 
                    $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'user_id', $data['user_id']);
                    return false;
                }
            }
            return $this->result(\Model_User_Recruiter::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
