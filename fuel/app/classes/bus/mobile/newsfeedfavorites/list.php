<?php

namespace Bus;

/**
 * get list news feed favorite for mobile
 *
 * @package Bus
 * @created 2015-01-22
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_NewsFeedFavorites_List extends BusAbstract
{
    protected $_required = array(
        'user_id'
    );
    
    protected $_length = array(
        'user_id' => array(1, 11)
    );

    protected $_number_format = array(
        'user_id',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model News Feed Favorite
     *
     * @created 2015-01-22
     * @updated 2015-01-22
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Favorite::get_list_for_mobile($data);
            return $this->result(\Model_News_Feed_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
