<?php

namespace Bus;

/**
 * get detail company for mobile
 *
 * @package Bus
 * @created 2014-12-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Companies_Detail extends BusAbstract
{
    protected $_required = array(
        'id',
        'user_id'
    );
    //check length
    protected $_length = array(
        'id'      => array(1, 11),
        'user_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
    );

    /**
     * get detail company by id and user_id for mobile
     *
     * @created 2014-12-05
     * @updated 2014-12-05
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company::get_detail_for_mobile($data);

            return $this->result(\Model_Company::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }

        return false;
    }

}
