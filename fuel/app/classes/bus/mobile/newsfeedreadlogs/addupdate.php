<?php

namespace Bus;

/**
 * <Mobile_NewsFeedReadLogs_AddUpdate - API to get list of NewsFeedReadLog>
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_NewsFeedReadLogs_AddUpdate extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'news_feed_id',
        'share_type',
    );

    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'share_type' => array(1, 2)
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
        'news_feed_id',
        'share_type'
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_Read_Log::add_update($data);
            return $this->result(\Model_News_Feed_Read_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
