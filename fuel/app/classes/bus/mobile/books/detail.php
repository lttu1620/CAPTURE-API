<?php

namespace Bus;

use Fuel\Core\Config;

/**
 * Agent Room Type Download service
 * 
 * @package 	Bus
 * @created 	Apr 31, 2014
 * @version     1.0
 * @author      thailh <thailh@evolableasia.vn>
 * @copyright   Yahoo! JAPAN
 */
class Mobile_Books_Detail extends BusAbstract {
    
    protected $_required = array(
        'book_id',        
    );   
    
    public function checkDataFormat($data) { 
        return true;
    }

    public function getXMLResponseSuccess() {
        
    }
   
    public function getXMLResponse()
    {    
        
    }
    
    public function operateDB($data) {
        try {
            $id = $data['book_id'];
            $result = \Model_Book::find($id);
            if ($result !== false) { 
                $this->_response = $result;
                return true;
            }              
        } catch (\Exception $e) {            
            $this->_exception = $e;
        }
        return false;
    }

}
