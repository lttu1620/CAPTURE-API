<?php

namespace Bus;

/**
 * <Mobile_NewsFeedViewLogs_List - API to get list of NewsFeedViewLogs on mobile>
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_NewsFeedViewLogs_List extends BusAbstract
{

    protected $_required = array(
        'share_type',
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'news_feed_id' => array(1, 11),
        'title' => array(1, 255),
        'share_type' => array(1, 2),
    );
    //check number
    protected $_number_format = array(
        'user_id',
        'news_feed_id',
        'share_type',
        'page',
        'limit'
    );
    //check email
    protected $_email_format = array(
        'email',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_View_Log::get_list($data);
            return $this->result(\Model_News_Feed_View_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
