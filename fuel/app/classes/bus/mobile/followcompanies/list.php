<?php

namespace Bus;

/**
 * <Get list follow company on mobile>
 *
 * @package Bus
 * @created 2015-01-22
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Mobile_FollowCompanies_List extends BusAbstract
{

    protected $_required = array(
        'user_id'
    );
    
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'disable' => 1
    );
    
    //check number
    protected $_number_format = array(
        'user_id',      
        'page',
        'limit'
    );
    
    /**
     * <Call function get_list() from model follow company on bile>
     *
     * @created 2015-01-22
     * @updated 2015-01-22
     * @access public
     * @author truongnn
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Company::mobile_get_list($data);
            return $this->result(\Model_Follow_Company::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
