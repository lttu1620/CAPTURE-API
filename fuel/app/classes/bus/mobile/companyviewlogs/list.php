<?php

namespace Bus;

/**
 * <Mobile_CompanyViewLogs_List - API to get list of CompanyViewLogs>
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_CompanyViewLogs_List extends BusAbstract
{

    protected $_required = array(
        'share_type',
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'company_id' => array(1, 11),
        'share_type' => array(1, 2),
    );
    
    //check number
    protected $_number_format = array(
        'user_id',
        'company_id',
        'share_type',
    );
    
    //check email
    protected $_email_format = array(
        'email',
    );
    
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_View_Log::get_list($data);
            return $this->result(\Model_Company_View_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
