<?php

namespace Bus;

/**
 * <Mobile_FollowCategories_Update - API to update FollowCategories on mobile>
 *
 * @package Bus
 * @created 2015-01-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_FollowCategories_Update extends BusAbstract
{
    //check required
    protected $_required = array(
        'user_id',
        'value'
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'user_id',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Category::mobile_update($data);
            return $this->result(\Model_Follow_Category::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
