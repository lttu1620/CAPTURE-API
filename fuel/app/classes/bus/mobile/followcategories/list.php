<?php

namespace Bus;

/**
 * get detail user for mobile
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_FollowCategories_List extends BusAbstract
{
    protected $_required = array(
        'user_id'
    );

    protected $_length = array(
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'user_id',
    );
    /**
     * get detail user by user_id for mobile
     *
     * @created 2014-12-19
     * @updated 2014-12-19
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Follow_Category::get_followCategories_mobile($data);
            return $this->result(\Model_Follow_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
