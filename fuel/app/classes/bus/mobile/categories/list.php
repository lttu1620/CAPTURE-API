<?php

namespace Bus;

/**
 * <Mobile_Categories_List - API to get list of Categories on mobile>
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_Categories_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(1, 40),
        'disable' => 1
    );
    //check number
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Category::mobile_get_list($data);
            return $this->result(\Model_Category::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
