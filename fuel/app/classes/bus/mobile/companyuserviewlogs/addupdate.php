<?php

namespace Bus;

/**
 * <Mobile_CompanyUserViewLogs_AddUpdate - API to get list of CompanyUserViewLog>
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author <truongnn>
 * @copyright Oceanize INC
 */
class Mobile_CompanyUserViewLogs_AddUpdate extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'company_id',
        'share_type',
    );
    //check length
    protected $_length = array(
        'id' =>  array(1, 11),
        'user_id' => array(1, 11),
        'company_id' => array(1, 11),
        'share_type' => array(1, 2)
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
        'company_id',
        'share_type'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_User_View_Log::add_update($data);
            return $this->result(\Model_Company_User_View_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
