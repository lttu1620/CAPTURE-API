<?php

namespace Bus;

/**
 * get detail user for mobile
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Users_Detail extends BusAbstract
{
    protected $_required = array(
        'id'
    );

    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'id',
        'user_id'
    );
    /**
     * get detail user by user_id for mobile
     *
     * @created 2014-12-19
     * @updated 2014-12-19
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::get_detail_for_mobile($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
