<?php

namespace Bus;

/**
 * <NewsFeedViewLogs_AddUpdate - API to get list of NewsFeedViewLog>
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeedViewLogs_AddUpdate extends BusAbstract
{
    //check require
    protected $_required = array(
        'user_id',
        'news_feed_id',
        'share_type',
    );

    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'share_type' => array(1, 2)
    );
    protected $_number_format = array(
        'id',
        'news_feed_id',
        'user_id',
        'share_type',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_View_Log::add_update($data);
            return $this->result(\Model_News_Feed_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
