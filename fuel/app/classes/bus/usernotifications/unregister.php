<?php

namespace Bus;

/**
 * Unregister user notifications
 *
 * @package Bus
 * @created 2015-01-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserNotifications_Unregister extends BusAbstract
{
	protected $_required = array(
		'user_id'
	);

	protected $_length = array(
		'user_id' => array(1, 11)
	);

	protected $_number_format = array(
		'user_id',
	);

	/**
	 * call function unregister() from model User Notification
	 *
	 * @created 2015-01-05
	 * @updated 2015-01-05
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Notification::unregister($data);
			return $this->result(\Model_User_Notification::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
