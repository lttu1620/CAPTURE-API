<?php

namespace Bus;

/**
 * get list info for register user notifications
 *
 * @package Bus
 * @created 2014-12-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserNotifications_List extends BusAbstract
{
	protected $_length = array(
		'id'      => array(1, 11),
		'user_id' => array(1, 11),
	);

	protected $_number_format = array(
		'id',
		'user_id',
	);

	/**
	 * call function get_list() from model User Notification
	 *
	 * @created 2014-12-26
	 * @updated 2014-12-26
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_User_Notification::get_list($data);
			return $this->result(\Model_User_Notification::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
