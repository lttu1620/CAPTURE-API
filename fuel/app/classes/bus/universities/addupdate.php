<?php

namespace Bus;

/**
 * add or update info for university
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_AddUpdate extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(1, 45),
        'kana' => array(1, 90),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
    );

    /**
     * call function add_update() from model University
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_University::add_update($data);
            return $this->result(\Model_University::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
