<?php

namespace Bus;

/**
 * get detail university
 *
 * @package Bus
 * @created 2014-11-24
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_Detail extends BusAbstract
{
    
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'name' => array(1, 45),
    );
    //check number
    protected $_number_format = array(
        'id',
    );
    /**
     * get detail university by id or name
     *
     * @created 2014-11-24
     * @updated 2014-11-24
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['id'])) {
                $id = $data['id'];
                $this->_response = \Model_University::find($id);
            } else {
                if (!empty($data['name'])) {
                    $conditions['where'][] = array(
                        'name' => $data['name']
                    );
                    $this->_response = \Model_University::find('first', $conditions);
                }
            }
            return $this->result(\Model_University::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
