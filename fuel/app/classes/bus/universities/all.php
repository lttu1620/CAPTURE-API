<?php

namespace Bus;

/**
 * get list universities
 *
 * @package Bus
 * @created 2014-12-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_All extends BusAbstract
{
    /**
     * call function get_all() from model University
     *
     * @created 2014-12-01
     * @updated 2014-12-01
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($param)
    {
        try {
            $this->_response = \Model_University::all($param);
            return $this->result(\Model_University::error());            
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
