<?php

namespace Bus;

/**
 * get list university
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Universities_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(1, 45),
        'kana' => array(1, 90),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model University
     *
     * @created 2014-11-21
     * @updated 2014-11-21
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_University::get_list($data);
            return $this->result(\Model_University::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
