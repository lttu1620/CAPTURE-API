<?php

namespace Bus;

/**
 * add info for follow company
 *
 * @package Bus
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCompanyLogs_Add extends BusAbstract
{
    protected $_required = array(
        'company_id',
        'user_id',
        'unfollow'
    );
     //check length
    protected $_length = array(
        'company_id' => array(1, 11),
        'user_id' => array(1, 11),
        'unfollow' => 1
    );
    //check number
    protected $_number_format = array(
        'company_id',
        'user_id',
        'unfollow',
    );
    /**
     * call function add() from model Follow Company Log
     *
     * @created 2014-12-10
     * @updated 2014-12-10
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Follow_Company_Log::add($data);
            return $this->result(\Model_Follow_Company_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
