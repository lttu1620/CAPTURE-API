<?php

namespace Bus;

/**
 * <Get list start logs>
 *
 * @package Bus
 * @created 2015-01-08
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class StartLogs_List extends BusAbstract {

    //check length
    protected $_length = array(
        'device' => array(1, 2),
    );
    //check number
    protected $_number_format = array(
        'device',
        'page',
        'limit'
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * <call function get_list() from model start logs>
     *
     * @created 2015-01-08
     * @updated 2015-01-08
     * @access public
     * @author truongnn
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Start_Log::get_list($data);
            return $this->result(\Model_Start_Log::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
