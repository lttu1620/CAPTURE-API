<?php

namespace Bus;

/**
 * <Add info for start logs>
 *
 * @package Bus
 * @created 2015-01-08
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class StartLogs_Add extends BusAbstract
{

    /**
     * <Call function add() from model Start Logs>
     *
     * @created 2015-01-08
     * @updated 2015-01-08
     * @access public
     * @author truongnn
     * @param $data
     * @return bool
     */
    public function operateDB($data)
    {
        try
        {           
            $this->_response = \Model_Start_Log::add();
            return $this->result(\Model_Start_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
