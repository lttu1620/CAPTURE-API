<?php

namespace Bus;

/**
 * get detail user facebook information
 *
 * @package Bus
 * @created 2014-11-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserFacebookInformations_Detail extends BusAbstract
{
    
     //check length
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'facebook_id' => array(1, 255),
        'facebook_email' => array(1, 100),
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
    );
    
    protected $_email_format = array(
        'facebook_email'
    );
    
    /**
     * call function get_detail() from model User Facebook Information
     *
     * @created 2014-11-20
     * @updated 2014-11-20
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Facebook_Information::get_detail($data);
            return $this->result(\Model_User_Facebook_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
