<?php

namespace Bus;

/**
 * add or update info for user facebook information
 *
 * @package Bus
 * @created 2014-11-27
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserFacebookInformations_AddUpdate extends BusAbstract
{
    
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'facebook_id' => array(1, 255),
        'facebook_name' => array(1, 100),
        'facebook_username' => array(1, 100),
        'facebook_email' => array(1, 100),
        'facebook_first_name' => array(1, 100),
        'facebook_last_name' => array(1, 100),
        'facebook_link' => array(1, 255),
        'facebook_image' => array(1, 255),
        'facebook_gender' => array(1, 10),
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
    );
    
    protected $_url_format = array(
        'facebook_link'
    );
    
    protected $_email_format = array(
        'facebook_email'
    );

    /**
     * call function add_update() from model User Facebook Information
     *
     * @created 2014-11-27
     * @updated 2014-11-27
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Facebook_Information::add_update($data);
            return $this->result(\Model_User_Facebook_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
