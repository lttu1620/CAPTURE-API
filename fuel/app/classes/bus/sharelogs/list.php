<?php

namespace Bus;

/**
 * get list share logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ShareLogs_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'news_feed_title',
        'user_id' => array(1, 11),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'news_feed_id' => array(1, 11),
        'share_type' => array(1, 2),
    );
    //check number
    protected $_number_format = array(
        'id',
        'news_feed_id',
        'share_type',
        'page',
        'limit'
    );
    //check email
    protected $_email_format = array(
        'email',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * call function get_list() from model share logs
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Share_Log::get_list($data);
            return $this->result(\Model_Share_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
