<?php

namespace Bus;

/**
 * add info for share logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ShareLogs_Add extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'news_feed_id',
        'share_type'
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'share_type' => array(1, 2),
    );
    //check number
    protected $_number_format = array(
        'id',
        'news_feed_id',
        'share_type'
    );

    /**
     * call function add() from model share Logs
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Share_Log::add($data);
            return $this->result(\Model_Share_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
