<?php

namespace Bus;

/**
 * Get list Invite (using array count)
 *
 * @package Bus
 * @created 2015-07-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Invites_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
        'email'   => array(1, 255),
        'status'  => 1,
        'disable' => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'user_id',
        'status'
    );

    /**
     * Call function get_list() from model Invite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Invite::get_list($data);
            return $this->result(\Model_Invite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
