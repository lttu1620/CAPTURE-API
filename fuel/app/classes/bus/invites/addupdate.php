<?php

namespace Bus;

/**
 * Add or update info for Invite
 *
 * @package Bus
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Invites_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'regist_type',
        'email',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'regist_type' => array(1, 20),
        'user_id'     => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function add_update() from model Invite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Invite::add_update($data);
            return $this->result(\Model_Invite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
