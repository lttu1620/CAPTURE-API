<?php

namespace Bus;

/**
 * get list news feed like
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedLikes_List extends BusAbstract
{
    
    protected $_length = array(
        'news_site_id' => array(1, 11),
        'user_name' => array(0, 40),
        'nickname' => array(0, 40),
        'news_feed_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
    );
    protected $_number_format = array(
        'news_site_id',
        'news_feed_id',
        'user_id',
        'disable',
    );
    protected $_email_format = array(
        'email',
    );

    /**
     * call function get_list() from model NewsFeedsLike
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Like::get_list($data);
            return $this->result(\Model_News_Feed_Like::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
