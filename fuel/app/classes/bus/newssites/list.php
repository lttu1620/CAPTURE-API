<?php

namespace Bus;

/**
 * get list news sites
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSites_List extends BusAbstract
{
    //check length
    protected $_length = array(
       'name'=> array(1, 255),
       'disable' => 1
    );
    //check number
    protected $_number_format = array(
       'disable',
    );

    /**
     * call function get_list() from model Campus
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Site::get_list($data);
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
