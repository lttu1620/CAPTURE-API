<?php

namespace Bus;

/**
 * Enable/Disable News Sites
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSites_Disable extends BusAbstract
{
     //check length
    protected $_length = array(
        'disable' =>1
    );
    
    //check number
    protected $_number_format = array(
        'disable'
    );
    /**
     * call function disable() from model News Sites
     *
     * @created 2014-11-28
     * @updated 2014-11-28
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Site::disable($data);
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
