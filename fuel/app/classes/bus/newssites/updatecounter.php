<?php

namespace Bus;

/**
 * update info feed_count and rss_count for news site
 *
 * @package Bus
 * @created 2015-01-15
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class NewsSites_UpdateCounter extends BusAbstract
{
    protected $_required = array(
        'news_site_id' //multiple value is OK (ex: 1,2,3,4)
    );
   
    /**
     * call function update_counter() from model News Site
     *
     * @created 2015-01-15
     * @updated 2015-01-15
     * @access public
     * @author diennvt
     * @param $data
     * @return bool
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Site::update_counter($data);
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
