<?php

namespace Bus;

/**
 * Add or update info for news site
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSites_AddUpdate extends BusAbstract {

    /** @var array $_required Define list required fields */
    protected $_required = array(
        'name',
    );
    
    /** @var array $_length Define list length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'name' => array(1, 255),
        'introduction_text' => array(0, 255),
        'thumbnail_img' => array(0, 255),
        'disable' => 1,
    );
    
    /** @var array $_number_format Define list number fields */
    protected $_number_format = array(
        'id',
        'disable'
    );

    /**
     * Insert/Update a News Site
     * 
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return boolean Success or otherwise   
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_News_Site::add_update($data);
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
