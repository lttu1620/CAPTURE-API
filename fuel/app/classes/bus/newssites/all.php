<?php

namespace Bus;

/**
 * get list news sites
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSites_All extends BusAbstract
{
    /**
     * call function get_all() from model News Site
     *
     * @created 2014-11-28
     * @updated 2014-11-28
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $options['where'] = array(
                'disable' => 0
            );
            $this->_response = \Model_News_Site::find('all', $options);
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
