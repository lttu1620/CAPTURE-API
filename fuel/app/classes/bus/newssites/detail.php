<?php

namespace Bus;

/**
 * get detail news site
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSites_Detail extends BusAbstract
{
    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'id',
    );
    /**
     * get detail news site by id
     *
     * @created 2014-11-28
     * @updated 2014-11-28
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Site::find($data['id']);
            if ($this->_response == null) { 
                $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                return false;
            }
            return $this->result(\Model_News_Site::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
