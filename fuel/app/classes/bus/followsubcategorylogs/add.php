<?php

namespace Bus;

/**
 * add info for follow subcategory
 *
 * @package Bus
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowSubcategoryLogs_Add extends BusAbstract
{

    protected $_required = array(
        'subcategory_id',
        'user_id',
        'unfollow'
    );
    //check length
    protected $_length = array(
        'subcategory_id' => array(1, 11),
        'user_id' => array(1, 11),
        'unfollow' => 1
    );
    //check number
    protected $_number_format = array(
        'subcategory_id',
        'user_id',
        'unfollow',
    );

    /**
     * call function add() from model Follow subcategory Log
     *
     * @created 2014-12-10
     * @updated 2014-12-10
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Subcategory_Log::add($data);
            return $this->result(\Model_Follow_Subcategory_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
