<?php

namespace Bus;

/**
 * Enable/Disable News comment like
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsCommentLikes_Disable extends BusAbstract
{
    //check require
    protected $_required = array(
        'news_comment_id',
        'user_id'
    );
    
    //check length
    protected $_length = array(
        'news_comment_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'news_comment_id',
        'user_id'
    );

    /**
     * call function disable() from model News Comment Like
     *
     * @created 2014-11-28
     * @updated 2014-11-28
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment_Like::disable($data);
            return $this->result(\Model_News_Comment_Like::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
