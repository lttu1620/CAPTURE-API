<?php

namespace Bus;

/**
 * get list news comment like
 *
 * @package Bus
 * @created 2014-11-28
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsCommentLikes_List extends BusAbstract
{
    //check length
    protected $_length = array(
        'news_site_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'news_comment_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
        'name' => array(0, 40),
        'nickname' => array(0, 40),
    );
    
    //check number
    protected $_number_format = array(
        'news_site_id',
        'news_feed_id',
        'news_comment_id',
        'user_id',
        'disable',
    );
    
    //check email
    protected $_email_format = array(
        'email',
    );
    
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * call function get_list() from model NewsCommentLike
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment_Like::get_list($data);
            return $this->result(\Model_News_Comment_Like::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
