<?php

namespace Bus;

/**
 * add info for login logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class LoginLogs_Add extends BusAbstract
{

    protected $_required = array(
        'user_id',
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'share_type' => array(0, 2),
        'login_device' => array(0, 2)
    );
    //check number
    protected $_number_format = array(
        'user_id',
        'news_feed_id',
        'share_type',
        'login_device'
    );

    /**
     * call function add() from model Login Logs
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Login_Log::add($data);
            return $this->result(\Model_Login_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
