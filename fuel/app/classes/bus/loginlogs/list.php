<?php

namespace Bus;

/**
 * get list login logs
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class LoginLogs_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
        'share_type',
    );
    //check number
    protected $_number_format = array(
        'user_id',
        'news_feed_id',
        'share_type',
        'page',
        'limit'
    );
    //check email
    protected $_email_format = array(
        'email',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * call function get_list() from model login logs
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Login_Log::get_list($data);
            return $this->result(\Model_Login_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
