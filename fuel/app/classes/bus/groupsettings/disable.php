<?php

namespace Bus;

/**
 * <GroupSettings_Disable - Model to operate to GroupSettings's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class GroupSettings_Disable extends BusAbstract
{

    protected $_required = array(
        'id',
        'disable',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Group_Setting::disable($data);
            return $this->result(\Model_Group_Setting::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
