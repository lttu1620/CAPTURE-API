<?php

namespace Bus;

/**
 * get list follow company
 *
 * @package Bus
 * @created 2014-12-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCompanies_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'company_id' => array(1, 11),
        'disable' => 1
    );
    //check number
    protected $_number_format = array(
        'company_id',
        'disable',
        'page',
        'limit'
    );
    //check email
    protected $_email_format = array(
        'email',
    );

    /**
     * call function get_list() from model follow company
     *
     * @created 2014-12-08
     * @updated 2014-12-08
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Company::get_list($data);
            return $this->result(\Model_Follow_Company::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
