<?php

namespace Bus;

/**
 * Enable/Disable follow company
 *
 * @package Bus
 * @created 2014-12-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCompanies_Disable extends BusAbstract
{
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'disable' => 1,
        'company_id' => array(1, 11),
        'user_id' => array(1, 11)
    );
    //check number
    protected $_number_format = array(
        'id',
        'disable',
        'company_id',
        'user_id'
    );

    /**
     * call function disable() from model Follow Company
     *
     * @created 2014-12-08
     * @updated 2014-12-08
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Company::disable($data);
            return $this->result(\Model_Follow_Company::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
