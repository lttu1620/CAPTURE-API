<?php

namespace Bus;

/**
 * add info for follow company
 *
 * @package Bus
 * @created 2014-12-08
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCompanies_Add extends BusAbstract
{

    protected $_required = array(
        'company_id',
        'user_id'
    );
    //check length
    protected $_length = array(
        'company_id' => array(1, 11),
        'user_id' => array(1, 11)
    );
    //check number
    protected $_number_format = array(
        'company_id',
        'user_id'
    );

    /**
     * call function add() from model Follow Company
     *
     * @created 2014-12-08
     * @updated 2014-12-08
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Company::add($data);
            return $this->result(\Model_Follow_Company::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
