<?php

namespace Bus;

/**
 * <CompanyViewLogs_AddUpdate - API to get list of CompanyViewLog>
 *
 * @package Bus
 * @created 2014-11-24
 * @updated 2014-12-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class CompanyViewLogs_AddUpdate extends BusAbstract
{
    protected $_required = array(
        'user_id',
        'company_id',
        'share_type'
    );

    protected $_number_format = array(
        'id',
        'user_id',
        'company_id',
        'share_type'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_View_Log::add_update($data);
            return $this->result(\Model_Company_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
