<?php

namespace Bus;

/**
 * get all tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_All extends BusAbstract
{
    /**
     * call function get_all() from model Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::get_all($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
