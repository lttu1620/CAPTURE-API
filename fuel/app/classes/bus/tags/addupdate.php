<?php

namespace Bus;

/**
 * Add or update info for tags.
 *
 * @package Bus
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'         => array(1, 11),
        'name'       => array(1, 50),
        'color'      => array(0, 10),
        'feed_count' => array(0, 11),
        'type'       => 1,
        'category_id'=> array(1,11)
    );

   /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'feed_count',
        'type',
        'category_id'
    );

    // set default value
    protected $_default_value = array(
        'feed_count' => '0'
    );

    /**
     * Call function add_update() from model Tags.
     *
     * @param array $data Input array.
     * @return bool|int Id of tags.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::add_update($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
