<?php

namespace Bus;

/**
 * Get list tag.
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name'       => array(1, 50),
        'color'      => array(0, 10),
        'type'       => 1,
        'category_id'=> array(1,11)
    );

   /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
        'page',
        'limit',
        'type',
        'category_id'
    );

    /**
     * Call function get_list() from model Tag.
     *
     * @author Le Tuan Tu
     * @param array $data Input array.
     * @return bool|array List tags.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::get_list($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
