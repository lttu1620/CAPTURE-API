<?php

namespace Bus;

/**
 * Check token invite for user recruiter
 *
 * @package Bus
 * @created 2015-07-30
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserActivations_CheckTokenInvite extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'token'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'token' => array(1, 255)
    );

    /**
     * Call function check_token_invite() from model User Activation
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Activation::check_token_invite($data);
            return $this->result(\Model_User_Activation::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
