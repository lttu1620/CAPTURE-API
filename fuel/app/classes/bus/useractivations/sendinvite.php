<?php

namespace Bus;

/**
 * Send invite for user recruiter
 *
 * @package Bus
 * @created 2015-07-30
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserActivations_SendInvite extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'regist_type',
        'email',
        'company_id',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'regist_type' => array(1, 20),
        'user_id'     => array(1, 11),
        'admin_id'     => array(1, 11),
        'company_id'  => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'admin_id',
        'company_id',
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function send_invite() from model User Activation
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Activation::send_invite($data);
            return $this->result(\Model_User_Activation::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
