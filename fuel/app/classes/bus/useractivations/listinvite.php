<?php

namespace Bus;

/**
 * Get list invite (using array count)
 *
 * @package Bus
 * @created 2015-07-31
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserActivations_ListInvite extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'admin_id' => array(1, 11),
        'user_id'  => array(1, 11),
        'email'    => array(1, 255),
        'disable'  => 1,
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'disable',
        'admin_id',
        'user_id'
    );

    /**
     * Call function get_list_invite() from model User Activation
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Activation::get_list_invite($data);
            return $this->result(\Model_User_Activation::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
