<?php

namespace Bus;

/**
 * add or update info for contacts
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Contacts_AddUpdate extends BusAbstract
{
    protected $_email_format = array(
        'email'
    );

    protected $_number_format = array(
        'id'
    );

    /**
     * call function add_update() from model Contact
     *
     * @created 2014-12-12
     * @updated 2014-12-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Contact::add_update($data);
            return $this->result(\Model_Contact::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
