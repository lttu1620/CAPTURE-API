<?php

namespace Bus;

/**
 * get detail contact
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Contacts_Detail extends BusAbstract
{
    protected $_required = array(
        'id'
    );

    protected $_number_format = array(
        'id'
    );

    /**
     * get detail contact by id
     *
     * @created 2014-12-12
     * @updated 2014-12-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Contact::get_detail($data);
            return $this->result(\Model_Contact::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
