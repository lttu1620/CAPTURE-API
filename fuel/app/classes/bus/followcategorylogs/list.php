<?php

namespace Bus;

/**
 * get list follow category log
 *
 * @package Bus
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCategoryLogs_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'category_id' => array(1, 11),
        'unfollow' => 1,
    );
    //check number
    protected $_number_format = array(
        'category_id',
        'unfollow',
        'page',
        'limit',
    );
    //check email
    protected $_email_format = array(
        'email',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * call function get_list() from model follow category log
     *
     * @created 2014-12-10
     * @updated 2014-12-10
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Category_Log::get_list($data);
            return $this->result(\Model_Follow_Category_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
