<?php

namespace Bus;

/**
 * get list follow subcategory
 *
 * @package Bus
 * @created 2014-12-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowSubcategories_List extends BusAbstract
{
    
    //check length
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'subcategory_id' => array(1, 11),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'subcategory_id',
        'disable',
        'page',
        'limit'
    );
    /**
     * call function get_list() from model follow subcategory
     *
     * @created 2014-12-09
     * @updated 2014-12-09
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Follow_Subcategory::get_list($data);
            return $this->result(\Model_Follow_Subcategory::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
