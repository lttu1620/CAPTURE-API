<?php

namespace Bus;

/**
 * Enable/Disable follow subcategory
 *
 * @package Bus
 * @created 2014-12-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowSubcategories_Disable extends BusAbstract
{

    //check length
    protected $_length = array(
        'id'  => array(1, 11),
        'disable' => 1,
        'subcategory_id' => array(1, 11),
        'user_id' => array(1, 11)
    );
    //check number
    protected $_number_format = array(
        'id',
        'disable',
        'subcategory_id',
        'user_id',
    );

    /**
     * call function disable() from model Follow Subcategory
     *
     * @created 2014-12-09
     * @updated 2014-12-09
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Subcategory::disable($data);
            return $this->result(\Model_Follow_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
