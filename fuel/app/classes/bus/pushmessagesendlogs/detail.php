<?php

namespace Bus;

/**
 * get detail push message send log
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PushMessageSendLogs_Detail extends BusAbstract
{

    protected $_required = array(
        'id'
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'id',
    );

    /**
     * get detail Push Message Send Logs by id
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Push_Message_Send_Log::get_detail($data['id']);
            if ($this->_response == null) { 
                $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                return false;
            }
            return $this->result(\Model_Push_Message_Send_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
