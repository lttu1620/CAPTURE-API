<?php

namespace Bus;

/**
 * <CompanySettings_AddUpdate - Model to operate to CompanySettings's functions>
 *
 * @package Bus
 * @created 2014-12-15
 * @updated 2014-12-15
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class CompanySettings_AddUpdate extends BusAbstract
{
    protected $_required = array(
        'company_id',
        'value'
    );

    protected $_number_format = array(
        'company_id'
    );

    /**
     * call function add_update()
     *
     * @created 2014-12-15
     * @updated 2014-12-15
     * @access public
     * @author <diennvt>
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Company_Setting::add_update($data);
            return $this->result(\Model_Company_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
