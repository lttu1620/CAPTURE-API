<?php

namespace Bus;

/**
 * <CompanySettings_Disable - Model to operate to CompanySettings's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class CompanySettings_Disable extends BusAbstract
{
    protected $_required = array(
        'id',
        'disable'
    );

    protected $_number_format = array(
        'id'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_Setting::disable($data);
            return $this->result(\Model_Company_Setting::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
