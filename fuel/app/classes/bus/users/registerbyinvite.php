<?php

namespace Bus;

/**
 * Register by invite
 *
 * @package Bus
 * @created 2015-07-30
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Users_RegisterByInvite extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'email',
        'password',
        'token',
        'regist_type',
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'token'       => array(1, 255),
        'email'       => array(1, 255),
        'password'    => array(1, 255),
        'regist_type' => array(1, 20),
    );

    /** @var array $_email_format field email */
    protected $_email_format = array(
        'email',
    );

    /**
     * Call function register_by_invite() from model User Activation
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::register_by_invite($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
