<?php

namespace Bus;

/**
 * <Users_AddUpdate - API to get list of Users>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_AddUpdate extends BusAbstract
{
    //check length
    protected $_length = array(
        'app_id' => array(0, 11),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'university_id' => array(0, 11),
        'department_id' => array(0, 11),
        'campus_id' => array(0, 11),
        'enrollment_year' => array(0, 4),
        'sex_id' => 1,
        'is_company' => 1,
        'is_ios' => 1,
        'is_android' => 1,
        'disable' => 1,
        'image_url' => array(0, 255),
        'additional_year' => array(0, 2),
        'introduction_text'=> array(0, 255),
    );
    
    //check number
    protected $_number_format = array(
        'university_id',
        'department_id',
        'campus_id',
        'enrollment_year',
        'sex_id',
        'is_company',
        'is_ios',
        'is_android',
        'disable',
        'additional_year',
    );

    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_User::add_update($data);
            return $this->result(\Model_User::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
