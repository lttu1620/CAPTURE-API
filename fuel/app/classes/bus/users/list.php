<?php

namespace Bus;

/**
 * <Users_List - API to get list of Users>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_List extends BusAbstract
{

    protected $_required = array(
        'page',
        'limit',
    );
    
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'university_id' => array(0, 11),
        'department_id' => array(0, 11),
        'campus_id' => array(0, 11),
        'enrollment_year' => array(0, 4),
        'sex_id' => 1,
        'is_company' => 1,
        'is_ios' => 1,
        'is_android' => 1,
        'disable' => 1,
    );
    
    //check number
    protected $_number_format = array(
        'university_id',
        'department_id',
        'campus_id',
        'enrollment_year',
        'sex_id',
        'is_company',
        'is_ios',
        'is_android',
        'disable',
        'page',
        'limit',
    );
    
    protected $_email_format = array(
        'email',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::get_list($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
