<?php

namespace Bus;

/**
 * add register company
 *
 * @package Bus
 * @created 2015-01-22
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Users_RegisterCompany extends BusAbstract
{
	protected $_required = array(
		'name',
		'kana',
        'user_id',//user recruiter id
	);
    
    //check length
    protected $_length = array(
        'name' => array(1, 120),
        'kana' => array(0, 255),
        'user_id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'user_id'
    );

	/**
	 * call function register_company() from Model_Company
	 *
	 * @created 2015-01-22
	 * @updated 2015-01-22
	 * @access public
	 * @author diennvt
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try 
        {
			$this->_response = \Model_Company::register_company($data);             
			return $this->result(\Model_Company::error());
		} 
        catch (\Exception $e) 
        {
			$this->_exception = $e;
		}
		return false;
	}

}
