<?php

namespace Bus;

/**
 * <Users_Detail - API to get detail of Users>
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Users_Detail extends BusAbstract
{
    //check require
    protected $_required = array(
        'id', //user_id
    );
    
     //check length
    protected $_length = array(
        'id' => array(0, 11),
    );
    
    //check number
    protected $_number_format = array(
        'id',
    );
    
    public function operateDB($data)
    {
        try
        {
            if (isset($data['full'])) {
                $this->_response = \Model_User::get_detail($data);
            } elseif (!empty($data['id'])) {
                $this->_response = \Model_User::find($data['id']);
                if ($this->_response == null) { 
                    $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                    return false;
                }
            }
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
