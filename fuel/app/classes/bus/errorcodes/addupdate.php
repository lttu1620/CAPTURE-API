<?php

namespace Bus;

/**
 * add or update info for error codes
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ErrorCodes_AddUpdate extends BusAbstract
{
    protected $_required = array(
        'id',
        'message'
    );

    protected $_number_format = array(
        'id'
    );

    /**
     * call function add_update() from model Error Codes
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Error_Code::add_update($data);
            return $this->result(\Model_Error_Code::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
