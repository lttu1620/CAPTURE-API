<?php

namespace Bus;

/**
 * get list error codes
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ErrorCodes_List extends BusAbstract
{
    protected $_number_format = array(
        'page',
        'limit'
    );
    
    /**
     * call function get_list() from model error codes
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Error_Code::get_list($data);
            return $this->result(\Model_Error_Code::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
