<?php

namespace Bus;

/**
 * get list error code
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class ErrorCodes_All extends BusAbstract
{
    /**
     * call function get_all() from model error codes
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Error_Code::find('all');
            return $this->result(\Model_Error_Code::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
