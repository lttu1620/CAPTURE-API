<?php

namespace Bus;

/**
 * <NewsFeedReadLogs_List - API to get list of NewsFeedReadLogs>
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeedReadLogs_List extends BusAbstract
{

    protected $_required = array(
        'share_type',
        
    );
    protected $_length = array(
        'share_type' => array(1, 2),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'title' => array(1, 255),
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
    );
    protected $_number_format = array(
        'share_type',
        'user_id',
        'news_feed_id',
    );
    protected $_email_format = array(
        'email',
    );
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_Read_Log::get_list($data);
            return $this->result(\Model_News_Feed_Read_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
