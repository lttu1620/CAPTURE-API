<?php

namespace Bus;

/**
 * get list help content
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'title' => array(1, 40),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model Help Content
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Help_Content::get_list($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
