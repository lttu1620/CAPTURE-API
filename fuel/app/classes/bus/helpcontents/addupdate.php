<?php

namespace Bus;

/**
 * add or update info for help content
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_AddUpdate extends BusAbstract
{

    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'title' => array(1, 40),
        'description' => array(0, 255),
    );
    //check number
    protected $_number_format = array(
        'id'
    );

    /**
     * call function add_update() from model Help Content
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Help_Content::add_update($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
