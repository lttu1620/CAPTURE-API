<?php

namespace Bus;

/**
 * get list campuses
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_List extends BusAbstract
{
    protected $_number_format = array(
        'page',
        'limit',
        'university_id'
    );

    /**
     * call function get_list() from model Campus
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campus::get_list($data);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
