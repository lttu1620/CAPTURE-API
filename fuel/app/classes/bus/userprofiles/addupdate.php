<?php

namespace Bus;

/**
 * add or update info for user profile
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_AddUpdate extends BusAbstract
{
    
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
        'password' => array(6, 40),
    );
    
    protected $_number_format = array(
        'id',
        'user_id',
    );
    
    protected $_email_format = array(
        'email',
    );

    /**
     * call function add_update() from model User Profile
     *
     * @created 2014-11-25
     * @updated 2014-11-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::add_update($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
