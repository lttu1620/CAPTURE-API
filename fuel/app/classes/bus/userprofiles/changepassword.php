<?php

namespace Bus;

/**
 * change password by user id
 *
 * @package Bus
 * @created 2014-12-05
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_ChangePassword extends BusAbstract
{
    protected $_required = array(
        'user_id',
        'password'
    );

    protected $_length = array(
        'user_id' => array(1, 11),
        'password' => array(6, 40),
    );
    
    protected $_number_format = array(
        'user_id',
    );
    /**
     * call function change_password() from model User Profile
     *
     * @created 2014-12-05
     * @updated 2014-12-05
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::change_password($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
