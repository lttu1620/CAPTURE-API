<?php

namespace Bus;

/**
 * get detail user profiles
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_Detail extends BusAbstract
{
   
    protected $_length = array(
        'id' => array(1, 11),
    );

    protected $_number_format = array(
        'id',
    );

    /**
     * get detail user profile by id
     *
     * @created 2014-11-25
     * @updated 2014-11-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['id'])) {
                $id = $data['id'];
                $this->_response = \Model_User_Profile::find($id);
                if ($this->_response == null) { 
                     $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                     return false;
                }
            }
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
