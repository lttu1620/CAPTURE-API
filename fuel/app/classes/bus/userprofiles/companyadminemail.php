<?php

namespace Bus;

/**
 * get company admin email
 *
 * @package Bus
 * @created 2014-12-22
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class UserProfiles_CompanyAdminEmail extends BusAbstract
{
    protected $_required = array(
        'user_id',
    );

    protected $_length = array(
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'user_id',
    );
    
    /**
     * call function change_password() from model User Profile
     *
     * @created 2014-12-05
     * @updated 2014-12-05
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::get_company_admin_email($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
