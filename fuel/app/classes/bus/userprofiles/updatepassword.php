<?php

namespace Bus;

/**
 * update password by token
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_UpdatePassword extends BusAbstract
{
    protected $_required = array(
        'token',
        'password',
    );

    protected $_length = array(
        'password' => array(6, 40),
    );

    protected $_default_value = array(
        'regist_type' => 'forget_password'
    );

    /**
     * call function update_password() from model User Profile
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Profile::update_password($data);
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
