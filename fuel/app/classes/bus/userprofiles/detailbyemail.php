<?php

namespace Bus;

/**
 * get detail user profiles
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserProfiles_DetailByEmail extends BusAbstract
{
    
    protected $_required = array(
        'email',
    );

    protected $_email_format = array(
        'email',
    );
    
    /**
     * get detail user profile by email
     *
     * @created 2014-11-25
     * @updated 2014-11-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            if (!empty($data['email'])) {
                $conditions['where'][] = array(
                    'email' => $data['email']
                );
                $this->_response = \Model_User_Profile::find('first', $conditions);
            }
            return $this->result(\Model_User_Profile::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
