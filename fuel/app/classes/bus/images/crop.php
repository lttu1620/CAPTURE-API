<?php

namespace Bus;

use Fuel\Core\Image;
use Fuel\Core\Config;

/**
 * get list login logs
 *
 * @package Bus
 * @created 2015-01-20
 * @version 1.0
 * @author <Tuancd>
 * @copyright Oceanize INC
 */
class Images_Crop extends BusAbstract {

    protected $_required = array(
        'field',
        'image_url',
        'x1',
        'y1',
        'x2',
        'y2'
    );

    /**
     * call function register_active() from model User
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author <Tuancd>
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($param) {       
        try {
            $this->_response = \Model_Image::cropImage($param);
            return $this->result(\Model_Image::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
