<?php

namespace Bus;

/**
 * get detail news site rss
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSitesRss_Detail extends BusAbstract
{
    protected $_required = array(
        'id'
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'id',
    );
    /**
     * get detail news comments by id
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Sites_Rss::find($data['id']);
            if ($this->_response == null) { 
                $this->_addError(self::ERROR_CODE_FIELD_NOT_EXIST, 'id', $data['id']);
                return false;
            }
            return $this->result(\Model_News_Sites_Rss::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
