<?php

namespace Bus;

/**
 * Enable/Disable News site rss
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSitesRss_Disable extends BusAbstract
{
    protected $_required = array(
        'id'
    );
    
    //check length
    protected $_length = array(        
        'disable' => 1
    );
    
    //check number
    protected $_number_format = array(       
        'disable'
    );
    
    /**
     * call function disable() from model News Comment
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Sites_Rss::disable($data);
            return $this->result(\Model_News_Sites_Rss::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
