<?php

namespace Bus;

/**
 * get list news site rss
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSitesRss_List extends BusAbstract
{
    
    //check length
    protected $_length = array(
       'news_site_id'=> array(1, 11),
       'name'=> array(1, 255),
       'description'=> array(1, 255),
       'url'=> array(1, 255),
       'disable'=> 1,
    );
    //check number
    protected $_number_format = array(
       'news_site_id',
       'disable',
       'time',
    );
    //check url
    protected $_url_format = array(
        'url',
    );
    
    /**
     * call function get_list() from model News Site Rss
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Sites_Rss::get_list($data);
            return $this->result(\Model_News_Sites_Rss::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
