<?php

namespace Bus;

/**
 * add or update info for news site rss
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsSitesRss_AddUpdate extends BusAbstract
{
    
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'news_site_id' => array(1, 11),
        'url' => array(1, 255),
        'name' => array(1, 255),
        'description' => array(1, 255),
        'tag_item_title' => array(1, 50),
        'tag_item_link' => array(1, 50),
        'tag_item_description' => array(1, 50),
        'tag_item_date' => array(1, 50)
    );
    //check number
    protected $_number_format = array(
        'id',
        'news_site_id',
    );
    //check url
    protected $_url_format = array(
        'url',
    );
    
    /**
     * call function add_update() from model Campus
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Sites_Rss::add_update($data);
            return $this->result(\Model_News_Sites_Rss::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
