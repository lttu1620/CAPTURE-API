<?php

namespace Bus;

/**
 * add info for follow category
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCategories_Add extends BusAbstract
{

    protected $_required = array(
        'category_id',
        'user_id'
    );
    //check length
    protected $_length = array(
        'category_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'category_id',
        'user_id'
    );

    /**
     * call function add() from model Follow Categories
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Category::add($data);
            return $this->result(\Model_Follow_Category::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
