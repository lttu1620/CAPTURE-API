<?php

namespace Bus;

/**
 * get list follow category
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCategories_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'category_id' => array(1, 11),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'category_id',
        'disable',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model follow category
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Category::get_list($data);
            return $this->result(\Model_Follow_Category::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
