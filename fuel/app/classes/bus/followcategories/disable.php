<?php

namespace Bus;

/**
 * Enable/Disable follow category
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowCategories_Disable extends BusAbstract
{
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'category_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'id',
        'category_id',
        'user_id',
        'disable'
    );

    /**
     * call function disable() from model Follow Category
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Follow_Category::disable($data);
            return $this->result(\Model_Follow_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
