<?php
namespace Bus;
/**
 * add or update info for admin
 *
 * @package Bus
 * @created 2015-01-14
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Systems_DeleteCache extends BusAbstract {

    /**
     * call function delete_cache() from model System
     *
     * @created 2015-01-14
     * @updated 2015-01-14
     * @access public
     * @author truongnn
     * @param $data
     * @return bool
     */
    public function operateDB($data) {
        try {
            $path = \Config::get('cache.path');
            $files = array();
            $files = array_merge($files, glob($path . '*.cache')); // remove cached data
            foreach ($files as $f) {
                if (is_file($f)) {
                    unlink($f);
                }
            }        
            return true;
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
