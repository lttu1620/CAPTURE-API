<?php

use Fuel\Core\DB;

/**
 * Any query in Model Company.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Company extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'name',
        'kana',
        'address',
        'description_short',
        'description',
        'thumbnail_img',
        'background_img',
        'introduction_movie',
        'corporate_url',
        'corporate_facebook',
        'corporate_twitter',
        'corporate_entry',
        'ower_user_id',
        'map_url',
        'follow_count',
        'employees',
        'name_english',
        'establishment_date',
        'introduction_what_txt',
        'introduction_what_img1',
        'introduction_what_img2',
        'introduction_why_txt',
        'introduction_why_img1',
        'introduction_why_img2',
        'introduction_how_txt',
        'introduction_how_img1',
        'introduction_how_img2',
        'introduction_likethis_txt',
        'introduction_likethis_img1',
        'introduction_likethis_img2',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'companies';

    /**
     * Get list company by address, name, kana.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $query = DB::select(
                        self::$_table_name.'.id',
                        self::$_table_name.'.name',
                        self::$_table_name.'.kana',
                        self::$_table_name.'.address',
                        self::$_table_name.'.employees',
                        self::$_table_name.'.follow_count',
                        self::$_table_name.'.disable',
                        self::$_table_name.'.created',
                        self::$_table_name.'.updated',
                        DB::expr("IFNULL(IF(thumbnail_img='',NULL,thumbnail_img),'" . \Config::get('no_image_company') . "') AS thumbnail_img")
                    )
                    ->from(self::$_table_name)
            ;
        
        if (!empty($param['address'])) {
            $query->where('address', 'LIKE', "%{$param['address']}%");
        }
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['kana'])) {
            $query->where('kana', 'LIKE', "%{$param['kana']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', '=', $param['disable']);
        }
        if (isset($param['category_id']) && $param['category_id'] != '') {
            $query->where(DB::expr(" EXISTS  (
                                                SELECT company_id 
                                                FROM company_categories 
                                                WHERE company_categories.category_id = {$param['category_id']} 
                                                        AND company_categories.company_id  = companies.id 
                                            )"));
        }

        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        
        if (isset($data) && !empty($data)) {
            $_param = array(
                'company_id' => \Lib\Arr::field($data, 'id', true)
            );
            $listCategory = \Model_Company_Category::get_listcategory_by_companyid($_param);
            foreach ($data as $key => $info) {
                $companyCatergory = \Lib\Arr::filter($listCategory, 'company_id', $info['id']);
                $data[$key]['category_name'] = str_replace(","," 、", \Lib\Arr::field($companyCatergory, 'category_name', true));
            }
        } 
        return array($total, $data);
    }

    /**
     * Enable/disable a or any company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $company = self::find($id);
            $company->set('disable', $param['disable']);
            if (!$company->save()) {
                return false;
            }
        }

        return true;
    }
    
    
    /**
     * Add or update info for company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param) {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $company = new self;
        if (!empty($id)) {
            $company = self::find($id);
            if (empty($company)) {
                return false;
            }
        }
        if (isset($param['name'])) {
            $company->set('name', $param['name']);
        }
        if (isset($param['name_english'])) {
            $company->set('name_english', $param['name_english']);
        }
        if (isset($param['map_url'])) {
            $company->set('map_url', $param['map_url']);
        }
        if (isset($param['employees'])) {
            $company->set('employees', !empty($param['employees']) ?$param['employees'] :0);
        }
        if (isset($param['establishment_date'])) {
            $company->set('establishment_date', !empty($param['establishment_date'])?$param['establishment_date'] :0);
        }
        if (isset($param['kana'])) {
            $company->set('kana', $param['kana']);
        }
        if (isset($param['address'])) {
            $company->set('address', $param['address']);
        }
        if (isset($param['description_short'])) {
            $company->set('description_short', $param['description_short']);
        }
        if (isset($param['description'])) {
            $company->set('description', $param['description']);
        }
        if (isset($param['thumbnail_img'])) {
            $company->set('thumbnail_img', $param['thumbnail_img']);
        }
        if (isset($param['ower_user_id'])) {
            $company->set('ower_user_id', $param['ower_user_id']);
        }
        // WHAT, WHY, HOW, LIKETHIS
        if (isset($param['introduction_what_txt'])) {
            $company->set('introduction_what_txt', $param['introduction_what_txt']);
        }
        if (isset($param['introduction_what_img1'])) {
            $company->set('introduction_what_img1', $param['introduction_what_img1']);
        }
        if (isset($param['introduction_what_img2'])) {
            $company->set('introduction_what_img2', $param['introduction_what_img2']);
        }

        if (isset($param['introduction_why_txt'])) {
            $company->set('introduction_why_txt', $param['introduction_why_txt']);
        }
        if (isset($param['introduction_why_img1'])) {
            $company->set('introduction_why_img1', $param['introduction_why_img1']);
        }
        if (isset($param['introduction_why_img2'])) {
            $company->set('introduction_why_img2', $param['introduction_why_img2']);
        }

        if (isset($param['introduction_how_txt'])) {
            $company->set('introduction_how_txt', $param['introduction_how_txt']);
        }
        if (isset($param['introduction_how_img1'])) {
            $company->set('introduction_how_img1', $param['introduction_how_img1']);
        }
        if (isset($param['introduction_how_img2'])) {
            $company->set('introduction_how_img2', $param['introduction_how_img2']);
        }

        if (isset($param['introduction_likethis_txt'])) {
            $company->set('introduction_likethis_txt', $param['introduction_likethis_txt']);
        }
        if (isset($param['introduction_likethis_img1'])) {
            $company->set('introduction_likethis_img1', $param['introduction_likethis_img1']);
        }
        if (isset($param['introduction_likethis_img2'])) {
            $company->set('introduction_likethis_img2', $param['introduction_likethis_img2']);
        }

        if (isset($param['background_img'])) {
            $company->set('background_img', $param['background_img']);
        }
        if (isset($param['introduction_movie'])) {
            $company->set('introduction_movie', $param['introduction_movie']);
        }
        if (isset($param['corporate_url'])) {
            $company->set('corporate_url', $param['corporate_url']);
        }
        if (isset($param['corporate_facebook'])) {
            $company->set('corporate_facebook', $param['corporate_facebook']);
        }
        if (isset($param['corporate_twitter'])) {
            $company->set('corporate_twitter', $param['corporate_twitter']);
        }
        if (isset($param['corporate_entry'])) {
            $company->set('corporate_entry', $param['corporate_entry']);
        }
        if (!empty($param['disable']) && $param['disable'] != '') {
            $company->set('disable', $param['disable']);
        }
        $company->set('follow_count', !empty($param['follow_count']) && $param['follow_count'] != '' 
            ? $param['follow_count'] : 0);

        if ($company->save()) {
            if (empty($company->id)) {
                $company->id = self::cached_object($company)->_original['id'];                
            }
            if (!empty($param['category_id'])) {
                if (!is_array($param['category_id'])) {
                    $param['category_id'] = array($param['category_id']);
                }
                foreach ($param['category_id'] as $categoryId) {
                    Model_Company_Category::add_update(array(
                        'company_id' => $company->id,
                        'category_id' => $categoryId,
                    ));
                }
            }
            if (!empty($param['user_id'])) {
                $conditions['where'][] = array(
                    'company_id' => $company->id,
                );
                $finds = Model_User_Recruiter::find('all', $conditions);
                $recruiter = new Model_User_Recruiter();
                $recruiter->set('user_id', $param['user_id']);
                $recruiter->set('company_id', $company->id);
                if (empty($finds)) {
                    $recruiter->set('is_admin', 1);
                    $recruiter->set('is_approved', 1);
                    $recruiter->create();
                } else {
                    $hasUser = false;
                    foreach ($finds as $find) {
                        if ($find->_original['user_id'] == $param['user_id'] || empty($find->_original['user_id'])) {
                            $hasUser = true;
                            $user_recruiter_id = $find->_original['id'];
                            break;
                        }
                    }
                    if (!$hasUser) {
                        $recruiter->set('is_admin', 0);
                        $recruiter->set('is_approved', 0);
                        $recruiter->create();
                    } else {
                        $query = DB::update("user_recruiters")
                                ->value('is_admin', 1)
                                ->value('is_approved', 1)
                                ->value('user_id', $param['user_id'])
                                ->where('id', $user_recruiter_id);
                        $query->execute();
                    }
                }
            }

            return !empty($company->id) ? $company->id : 0;
        }

        return false;
    }

    /**
     * Get detail of company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail($param)
    {
        if(empty($param['user_id']))
            $param['user_id']=0;
        
        $query = DB::select(
            self::$_table_name . '.*',
            array('categories.name', 'category_name'),
            DB::expr("IF(ISNULL(follow_companies.id), 0, 1) as is_follow")
        )
            ->from(self::$_table_name)
            ->join('company_categories', 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'company_categories.company_id')
            ->join('categories', 'LEFT')
            ->on('company_categories.category_id', '=', 'categories.id')
            ->join(
                DB::expr(
                    "(SELECT * FROM follow_companies WHERE company_id='{$param['id']}' AND user_id='{$param['user_id']}' AND disable = 0) AS follow_companies"
                ),
                'LEFT'
            )
            ->on('follow_companies.company_id', '=', 'companies.id')
            ->where(self::$_table_name . '.id', '=', $param['id'])
            ->where(self::$_table_name . '.disable', '=', '0');
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        if (!empty($data)) {
            // Get list company category
            $company_category = \Model_Company_Category::get_list(array(
                    'company_id' => $param['id']
            ));
            $result = $data[0];            
            $result['company_category'] = !empty($company_category) ? $company_category : array();
           
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Get detail of company for mobile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail_for_mobile($param) {
        $data = self::get_detail($param);
        $data['followers'] = \Model_Follow_Company::get_user_follow_company($param);
        if (isset($param['tab']) && $param['tab'] == 'newer') {
            list ($data['news_feed_newer']['total'], $data['news_feed_newer']['data']) = \Model_News_Comment::get_news_feed_newer_by_company($param);
        } elseif ((isset($param['tab']) && $param['tab'] == 'popular')) {
            list ($data['news_feed_popular']['total'], $data['news_feed_popular']['data']) = \Model_News_Comment::get_news_feed_popular_by_company($param);
        }

        return $data ? $data : array();
    }

    /**
     * Function to get all company id.
     *
     * @author diennvt
     * @return array Returns the array.
     */
    public static function get_company_ids() {
        $company = self::query()
                ->select('id')
                ->where('disable', 0)
                ->get();

        return !empty($company) ? $company : array();
    }

    /**
     * Get company detail for tada copy (API 3).
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_company_detail_for_tada_copy($param)
    {
        $query = DB::select(
            'id',
            'name',
            array('name_english', 'name_en'),
            array('thumbnail_img', 'icon'),
            array('employees', 'members_number'),
            array('introduction_what_txt', 'do_what'),
            array('introduction_how_txt', 'do_how'),
            DB::expr("
                DATE(FROM_UNIXTIME(establishment_date)) AS establish
            "),
            array('address', 'address')
        )
            ->from(self::$_table_name)
            ->where('id', '=', $param['company_id'])
            ->where(self::$_table_name . '.disable', '=', '0');

        $data = $query->execute()->as_array();
        if (!empty($data[0])) {
            // get comment
            $query = DB::select(
                array('news_comments.news_feed_id', 'id'),
                array('news_feeds.image_url', 'image'),
                array('news_feeds.title', 'headline'),
                array('news_comments.comment_short', 'text'),
                array('news_feeds.detail_url', 'url'),
                array('news_comments.like_count', 'nice'),
                array('news_comments.created', 'created'),
                array('news_sites.id', 'news_site_id'),
                array('news_sites.name', 'news_site_name'),
                array('news_feeds.distribution_date', 'distribution_date')
            )
                ->from('news_comments')
                ->join(self::$_table_name)
                ->on('news_comments.company_id', '=', 'companies.id')
                ->join('news_feeds', 'LEFT')
                ->on('news_comments.news_feed_id', '=', 'news_feeds.id')
                ->join('news_sites', 'LEFT')
                ->on('news_feeds.news_site_id', '=', 'news_sites.id')
                ->where(self::$_table_name . '.id', '=', $param['company_id'])
                ->where('news_comments.disable', '=', '0')
                ->where('news_feeds.is_tadacopy', '=', '1')
                ->where(DB::expr(
                    "(IFNULL(news_feeds.news_site_id,0) = 0 OR (news_feeds.news_site_id > 0 AND news_sites.disable = 0 AND news_sites.is_tadacopy = 1))"
                ));
                if ($param['comment_order_by'] == 'LIKE_COUNT') {
                    $query->order_by('news_comments.like_count', 'DESC');
                } elseif ($param['comment_order_by'] == 'SUBMIT_DATE') {
                    $query->order_by('news_comments.created', 'DESC');
                }
                $query->limit(5);
            $data[0]['comments'] = $query->execute()->as_array();
            $data[0]['comments'] = \Model_Setting::set_image_url($data[0]['comments'], array('image'));

            // get member
            $query = DB::select(
                array('user_recruiters.id', 'id'),
                array('users.name', 'name'),
                array('user_recruiters.position', 'post'),
                array('user_recruiters.thumbnail_img', 'icon')
            )
                ->from('user_recruiters')
                ->join('users')
                ->on('user_recruiters.user_id', '=', 'users.id')
                ->where('user_recruiters.company_id', '=', $param['company_id'])
                ->where('user_recruiters.disable', '=', '0');
            $data[0]['members'] = $query->execute()->as_array();

            // get follow
            $query = DB::select(
                array('users.image_url', 'icon')
            )
                ->from('follow_companies')
                ->join('users')
                ->on('follow_companies.user_id', '=', 'users.id')
                ->join('companies')
                ->on('follow_companies.company_id', '=', 'companies.id')
                ->where('follow_companies.company_id', '=', $param['company_id'])
                ->where('follow_companies.disable', '=', '0');
            $data[0]['follows'] = $query->execute()->as_array();

            // get category
            $query = DB::select(
                array('categories.name', 'name')
            )
                ->from('company_categories')
                ->join('categories')
                ->on('company_categories.category_id', '=', 'categories.id')
                ->join('companies')
                ->on('company_categories.company_id', '=', 'companies.id')
                ->where('company_categories.company_id', '=', $param['company_id'])
                ->where('company_categories.disable', '=', '0');
            $data[0]['category'] = $query->execute()->as_array();
        }

        $param['called_api_name'] = \Config::get('tadacopy_company_detail');
        if (!\Model_External_Relation_Log::insert($param)) {
            return false;
        }

        return !empty($data[0]) ? $data[0] : array();
    }
    
     /**
     * Update_counter - follow_count for Company.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_counter($param)
    {
        if (empty($param['company_id']))
        {
            return false;
        }
        $company_ids = explode(',', $param['company_id']);
        $followCountArr = Model_Follow_Company::get_follow_count($param['company_id']);
        foreach ($company_ids as $id)
        {
            $followCount = 0;
            foreach ($followCountArr as $followCnt)
            {
                if ($id == $followCnt['company_id'])
                {
                    $followCount = $followCnt['follow_count'];
                    break;
                }
            }
            $company = self::find($id);
            if (!empty($company))
            {
                $company->set('follow_count', $followCount);
                if (!$company->update())
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Regist company.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function register_company($param)
    {
        // find company and recuiter in database
        $query = DB::select(
                    array(self::$_table_name . '.id', 'company_id'),
                    self::$_table_name . '.name',
                    self::$_table_name . '.kana',
                    array('user_recruiters.id', 'user_recruiter_id'),
                    'user_recruiters.user_id',
                    'user_recruiters.disable',
                    'user_recruiters.is_admin',
                    'user_profiles.email'                    
                )
                ->from(self::$_table_name)
                ->join("user_recruiters", "LEFT")
                ->on(self::$_table_name . '.id', '=', 'user_recruiters.company_id')
                ->join("user_profiles", "LEFT")
                ->on('user_recruiters.user_id', '=', 'user_profiles.user_id')
                ->where(self::$_table_name . '.disable', '=', '0')
                ->where(DB::expr("MD5(". self::$_table_name . ".name) = '" . md5($param['name']) . "'"))
                ->where(DB::expr("MD5(". self::$_table_name . ".kana) = '" . md5($param['kana']) . "'"));        
        $data = $query->execute()->as_array();
        $recruiterData = array();         
        $adminList = array();
        if (!empty($data))
        {
            foreach ($data as $item) 
            {
                // find exist user 
                if ($item['user_id'] == $param['user_id']) 
                {
                    if (!empty($item['disable'])) 
                    {                        
                        $recruiterData['id'] = $item['user_recruiter_id'];
                    } 
                    else 
                    {
                        self::errorDuplicate('user_id', $param['user_id']);
                        return false;
                    }
                }
                
                // find recruiter admin list
                if (!empty($item['user_id']) && !empty($item['is_admin']) && $item['user_id'] != $param['user_id']) 
                {
                    $adminList[] = $item;                    
                }
            }        
        }
        
        // recruiter data for insert/update and return
        $recruiterData = array_merge($recruiterData, array(                
            'company_id' => !empty($data[0]['company_id']) ? $data[0]['company_id'] : 0,
            'company_name' => !empty($data[0]['name']) ? $data[0]['name'] : '',
            'company_kana' => !empty($data[0]['kana']) ? $data[0]['kana'] : '',
            'user_id' => $param['user_id'],
            'is_approved' => !empty($adminList) ? 0 : 1,
            'is_admin' => !empty($adminList) ? 0 : 1,          
            'disable' => '0',            
        )); 
        
        // if mew company then add new
        if (empty($recruiterData['company_id'])) {            
            $recruiterData['company_id'] = Model_Company::add_update(array(
                    'name' => $param['name'],
                    'kana' => $param['kana'],
                )
            );
            $recruiterData['new_company'] = 1;
        }
        $isNew = !empty($recruiterData['id']) ? false: true;
        if ($isNew) {
            $user = Model_User::find($param['user_id']);
            if (!empty($user->get('image_url'))) {
                $recruiterData['thumbnail_img'] = $user->get('image_url');
            }
        }
        $recruiter = new Model_User_Recruiter($recruiterData, $isNew);
        if ($recruiter->save() === false) 
        {
            \LogLib::info('Can not insert into table user_recruiters', __METHOD__, $param);
            return false;
        }        
        $recruiterData['id'] = $recruiter->get('id');
        
        // send email to recruiter admin for approving
        if (empty($recruiterData['is_admin']) && !empty($adminList)) 
        {
            $options['where'] = array(
                'user_id' => $param['user_id']
            );
            $profile = \Model_User_Profile::find('first', $options);
            if (empty($profile)) 
            {
                self::errorNotExist('user_id');               
                return false;
            }               
            $token = \Lib\Str::generate_token();
            $userActivation = new \Model_User_Activation();
            $userActivation->set('user_id', $profile->get('user_id'));
            $userActivation->set('email', $profile->get('email'));
            $userActivation->set('token', $token);          
            $userActivation->set('expire_date', \Config::get('register_token_expire'));
            $userActivation->set('regist_type', \Config::get('user_activations_type')['register_company']);
            if (!$userActivation->create()) 
            {
                \LogLib::warning('Can not create token', __METHOD__, $param);                    
                return false;
            }
            foreach ($adminList as $item)
            {       
                $param = array(   
                    'email' => $item['email'],
                    'token' => $token,        
                );                
                if (!\Lib\Email::sendApproveEmail($param)) 
                {
                    \LogLib::warning('Can not send email for recruiter admin', __METHOD__, $param);                    
                }
            }
        }
        return $recruiterData;
    }

    /**
     * Resend register company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function resend_register_company($param)
    {
        $option['where'] = array(
            'email'       => $param['email'],
            'disable'     => '0',
            'regist_type' => \Config::get('user_activations_type')['register_company'],
        );
        $userActivation = \Model_User_Activation::find('first', $option);
        if (!empty($userActivation)) {
            $token = $userActivation->get('token');
            // update new expire_date
            $userActivation->set('expire_date', \Config::get('register_token_expire'));
            if (!$userActivation->update()) {
                \LogLib::info('Can not update user_activations', __METHOD__, $param);
                return false;
            }
            $sql = "
                SELECT user_profiles.email
                FROM user_recruiters
                JOIN user_profiles ON user_recruiters.user_id = user_profiles.user_id
                WHERE company_id =
                        (SELECT company_id
                         FROM user_recruiters
                         WHERE user_id = {$userActivation->get('user_id')})
                AND   is_admin = 1
            ";
            $adminList = DB::query($sql)->execute()->as_array();
            foreach ($adminList as $item) {
                $param = array(
                    'email' => $item['email'],
                    'token' => $token,
                );
                if (!\Lib\Email::sendApproveEmail($param)) {
                    \LogLib::warning('Can not send email for recruiter admin', __METHOD__, $param);
                    return false;
                }
            }
        } else {
            \LogLib::info('Not exist email in user_activations', __METHOD__, $param);
            self::errorNotExist('email', $param['email']);
            return false;
        }
        return true;
    }
}
