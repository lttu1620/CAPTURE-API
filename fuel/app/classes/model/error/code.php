<?php

/**
 * Any query for model error codes.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Error_Code extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'message',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'error_codes';

    /**
     * Get list error codes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);

        if (!empty($param['message'])) {
            $query->where('message', 'LIKE', "%{$param['message']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Add info for error codes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int|mixed Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        try {
            $code = self::find($param['id']);
            if (!$code) {
                $code = new self;
            }
            if (!empty($param['id'])) {
                $code->set('id', $param['id']);
            }
            if (!empty($param['message'])) {
                $code->set('message', $param['message']);
            }
            
            if ($code->save()) {
                if (empty($code->id)) {
                    $code->id = self::cached_object($code)->_original['id'];
                }
                return !empty($code->id) ? $code->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
