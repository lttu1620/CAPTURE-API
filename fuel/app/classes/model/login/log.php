<?php

/**
 * Any query for model Login Logs.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Login_Log extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'user_id',
		'news_feed_id',
		'share_type',
		'login_device',
		'created',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events'          => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events'          => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'login_logs';

	/**
	 * Get list login logs.
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data.
	 * @return array Returns array(total, data).
	 */
	public static function get_list($param)
	{
		$query = DB::select(
			self::$_table_name . '.*',
			array('users.name', 'user_name'),
			array('users.nickname', 'user_nickname'),
			array('user_profiles.email', 'user_email'),
			array('news_feeds.title', 'news_feed_title')
		)
			->from(self::$_table_name)
			->join('users', 'LEFT')
			->on(self::$_table_name . '.user_id', '=', 'users.id')
			->join('news_feeds', 'LEFT')
			->on(self::$_table_name . '.news_feed_id', '=', 'news_feeds.id')
			->join('user_profiles', 'LEFT')
			->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

		if (!empty($param['user_id'])) {
			$query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
		}
		if (!empty($param['name'])) {
			$query->where('users.name', 'LIKE', "%{$param['name']}%");
		}
		if (!empty($param['nickname'])) {
			$query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
		}
		if (!empty($param['email'])) {
			$query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
		}
		if (!empty($param['news_feed_id'])) {
			$query->where(self::$_table_name . '.news_feed_id', '=', $param['news_feed_id']);
		}
		if (!empty($param['share_type'])) {
			$query->where(self::$_table_name . '.share_type', '=', $param['share_type']);
		}
		if (!empty($param['date_from'])) {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
		}
		if (!empty($param['date_to'])) {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
		}
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			if ($sortExplode[0] == 'created') {
				$sortExplode[0] = self::$_table_name . '.created';
			}
			$query->order_by($sortExplode[0], $sortExplode[1]);
		} else {
			$query->order_by(self::$_table_name . '.created', 'DESC');
		}
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;

		return array($total, $data);
	}

	/**
	 * Add info for login logs.
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data.
	 * @return bool|int Returns the booleans or integer.
	 */
	public static function add($param)
	{
		$log = new self;
		$log->set('user_id', $param['user_id']);
		if (!empty($param['news_feed_id'])) {
			$log->set('news_feed_id', $param['news_feed_id']);
		}
		if (isset($param['share_type'])) {
			$log->set('share_type', $param['share_type']);
		}
		if (isset($param['login_device'])) {
			$log->set('login_device', $param['login_device']);
		}
		if ($log->create()) {
			$log->id = self::cached_object($log)->_original['id'];

			return !empty($log->id) ? $log->id : 0;
		}

		return false;
	}

	/**
	 * Get login report.
	 *
	 * @author Le Tuan Tu
	 * @param array $param Input data.
	 * @return array Returns the array.
	 */
	public static function get_login_report($param)
	{
		$query = DB::select(
			DB::expr("
                DATE(FROM_UNIXTIME(created)) AS date_report,
                IFNULL(SUM((CASE WHEN login_device = " . Config::get('ios_login_device') . "
                    THEN login_device ELSE 0 END)), 0) AS ios_count,
                IFNULL(SUM((CASE WHEN login_device = " . Config::get('android_login_device') . "
                    THEN login_device ELSE 0 END)), 0) AS android_count,
                IFNULL(SUM((CASE WHEN login_device = " . Config::get('web_login_device') . "
                    THEN login_device ELSE 0 END)), 0) AS webos_count,
                IFNULL(SUM((CASE WHEN login_device = " . Config::get('other_login_device') . "
                    THEN login_device ELSE 0 END)), 0) + IFNULL(SUM(CASE WHEN login_device IS NULL
                    THEN 1 ELSE 0 END), 0) AS other_count
            ")
		)
			->from(self::$_table_name);
		if (!empty($param['date_from'])) {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
		}
		if (!empty($param['date_to'])) {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
		}
		$query->group_by('date_report');
		$data = $query->execute()->as_array();

		return $data;
	}
}
