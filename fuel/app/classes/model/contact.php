<?php

/**
 * Any query for model Contact.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Contact extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'name',
        'email',
        'tel',
        'address',
        'website',
        'subject',
        'content',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'contacts';

    /**
     * Get list contact.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Return array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);

        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['email'])) {
            $query->where('email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['tel'])) {
            $query->where('tel', 'LIKE', "%{$param['tel']}%");
        }
        if (!empty($param['address'])) {
            $query->where('address', 'LIKE', "%{$param['address']}%");
        }
        if (!empty($param['website'])) {
        $query->where('website', 'LIKE', "%{$param['website']}%");
        }
        if (!empty($param['subject'])) {
            $query->where('subject', 'LIKE', "%{$param['subject']}%");
        }
        if (!empty($param['content'])) {
            $query->where('content', 'LIKE', "%{$param['content']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail contact.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function get_detail($param)
    {
        $options['where'][] = array(
            'id' => $param['id']
        );
        $data = self::find('first', $options);
        if (empty($data)) {
            self::errorNotExist('contact_id', $param['id']);

            return false;
        }

        return $data;
    }

    /**
     * Add or update info for contact.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        try {
            //check id if existing
            $id = !empty($param['id']) ? $param['id'] : 0;
            $contact = new self;
            if (!empty($id)) {
                $contact = self::find($id);
                if (empty($contact)) {
                    static::errorNotExist('contact_id', $id);
                    return false;
                }
            }

            if (!empty($param['user_id'])) {
                $contact->set('user_id', $param['user_id']);
            }
            if (!empty($param['name'])) {
                $contact->set('name', $param['name']);
            }
            if (!empty($param['email'])) {
                $contact->set('email', $param['email']);
            }
            if (!empty($param['tel'])) {
                $contact->set('tel', $param['tel']);
            }
            if (!empty($param['address'])) {
                $contact->set('address', $param['address']);
            }
            if (!empty($param['website'])) {
                $contact->set('website', $param['website']);
            }
            if (!empty($param['subject'])) {
                $contact->set('subject', $param['subject']);
            }
            if (!empty($param['content'])) {
                $contact->set('content', $param['content']);
            }
            if ($contact->save()) {
                if (empty($param['id']) && !empty($param['is_send_email']) && $param['is_send_email'] == '1') {
                    $send = \Lib\Email::sendContactEmail($param);
                    if (!$send) {
                        return false;
                    }
                }
                if (empty($contact->id)) {
                    $contact->id = self::cached_object($contact)->_original['id'];
                }
                return !empty($contact->id) ? $contact->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
