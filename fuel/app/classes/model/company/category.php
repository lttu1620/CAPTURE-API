<?php

/**
 * Any query for model company category.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Company_Category extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'company_id',
        'category_id',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'company_categories';

    /**
     * Get list company category.
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_list($param) {
        $query = DB::select(
                array('categories.name', 'category_name'), array('companies.name', 'company__name'), self::$_table_name . '.*'
            )
            ->from(self::$_table_name)
            ->join('categories', 'LEFT')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join('companies', 'LEFT')
            ->on(self::$_table_name . '.company_id', '=', 'companies.id');

        if (!empty($param['company_id'])) {
            $query->where(self::$_table_name . '.company_id', '=', $param['company_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail of company categories.
     * 
     * @author Le Tuan Tu
     * @param int $id Input data.
     * @return array Return the array.
     */
    public static function get_detail($param) {
        $query = DB::select(
                array('categories.name', 'category_name'), array('companies.name', 'company_name'), self::$_table_name . '.*'
            )
            ->from(self::$_table_name)
            ->join('categories', 'LEFT')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join('companies', 'LEFT')
            ->on(self::$_table_name . '.company_id', '=', 'companies.id')
            ->where(self::$_table_name . '.id', '=', $param['id']);
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $id);
        }
        return $data ? $data[0] : array();
    }

    /**
     * Add info for company category.
     * 
     * @author Le Tuan Tu
     * @param array $param input data.
     * @throws Exception If the provided is not of type array.
     * @return int|mixed Returns the boolean or the integer.
     */
    public static function add($param) {
        try {
            $options['where'][] = array(
                'category_id' => $param['category_id'],
                'company_id' => $param['company_id']
            );
            $follow = self::find('first', $options);
            if (!$follow) {
                $follow = new self;
                $follow->set('category_id', $param['category_id']);
                $follow->set('company_id', $param['company_id']);

                if ($follow->save()) {
                    return true;
                }
            } else {
                if ($follow->get('disable') == '1') {
                    $follow->set('disable', '0');
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('company_category_id');
                    return false;
                }
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Disable a company category.
     * 
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        try {
            if (!isset($param['disable'])) {
                $param['disable'] = '1';
            }
            if (!empty($param['id'])) {
                $options['where'][] = array(
                    'id' => $param['id'],
                );
            } else {
                $options['where'][] = array(
                    'category_id' => $param['category_id'],
                    'company_id' => $param['company_id'],
                );
            }
            $follow = self::find('first', $options);
            if ($follow) {
                if ($follow->_data['disable'] != $param['disable']) {
                    $follow->set('disable', $param['disable']);
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('company_category_id');
                }

                return false;
            } else {
                self::errorNotExist('company_category_id');
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Add info for company category.
     * 
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function add_update($param) {
        try {
            $options['where'][] = array(
                //'category_id' => $param['category_id'],
                'company_id' => $param['company_id']
            );
            $follow = self::find('first', $options);
            if (!$follow) {
                $follow = new self;
            }
            if (!empty($param['category_id'])) {
                $follow->set('category_id', $param['category_id']);
            }
            if (!empty($param['company_id'])) {
                $follow->set('company_id', $param['company_id']);
            }
            $follow->set('disable', '0');
            if ($follow->save()) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function get_listcategory_by_companyid($param) {
        $sql = "
            SELECT company_categories.*
                  ,categories.name as category_name 
            FROM company_categories
            LEFT JOIN categories on categories.id = company_categories.category_id
            WHERE company_categories.company_id in ({$param['company_id']})";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }

}
