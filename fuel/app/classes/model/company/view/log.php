<?php

/**
 * Model to operate to Model_Company_View_Logs's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Company_View_Log extends Model_Abstract
{

    protected static $_properties = array(
        'id',
        'user_id',
        'company_id',
        'share_type',
        'created',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'company_view_logs';

    /**
     * Function to get a list of Company_View_Logs
     * 
     * @author diennvt 
     * @param array $param Input data
     * @return array Return the array include total and data
     */
    public static function get_list($param)
    {
        $query = DB::select(static::$_table_name . '.*',
                array('users.name', 'user_name'),
                array('users.nickname', 'user_nickname'),
                array('user_profiles.email','user_email'),
                array('companies.name', 'company_name')
                )
                ->from(static::$_table_name)
                ->join('users','LEFT')
                ->on(self::$_table_name.'.user_id','=','users.id')
                ->join('companies','LEFT')
                ->on(self::$_table_name.'.company_id','=','companies.id')
                ->join('user_profiles','LEFT')
                ->on(self::$_table_name.'.user_id','=','user_profiles.user_id');
        
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['company_id'])) {
            $query->where(self::$_table_name . '.company_id', '=', $param['company_id']);
        }
        if (!empty($param['share_type'])) {
            $query->where(self::$_table_name . '.share_type', '=', $param['share_type']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if(!empty($sortExplode)){
                switch ($sortExplode[0]){
                    case 'name':
                        $sortExplode[0] = 'users.name';
                        break;
                    case 'company_name':
                        $sortExplode[0] = 'companies.name';
                        break;
                    case 'created':
                        $sortExplode[0] = self::$_table_name . '.created';
                }
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to add or update a Company_View_Logs
     *   
     * @author diennvt 
     * @param array $param Input data
     * @return mixed Returns the boolean or integer
     */
    public static function add_update($param)
    {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $companyviewlog = new self;
        if (!empty($id))
        {
            $companyviewlog = self::find($id);
            if (empty($companyviewlog))
            {
                static::errorNotExist('company_view_log_id', $param['id']);
                return false;
            }
        }

        // set information for subcategory if having
        if (!empty($param['user_id']))
        {
            $companyviewlog->set('user_id', $param['user_id']);
        }

        if (!empty($param['company_id']))
        {
            $companyviewlog->set('company_id', $param['company_id']);
        }

        if (!empty($param['share_type']))
        {
            $companyviewlog->set('share_type', $param['share_type']);
        }

        //check id for adding new or updating
        if ($companyviewlog->save())
        {
            if (empty($companyviewlog->id))
            {
                $companyviewlog->id = self::cached_object($companyviewlog)->_original['id'];
            }
            return !empty($companyviewlog->id) ? $companyviewlog->id : 0;
        }
        return false;
    }

}
