<?php

use Fuel\Core\DB;

/**
 * Model to operate Company_Setting's functions
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Company_Setting extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'company_id',
        'setting_id',
        'value',
        'disable',
        'created',
        'updated',
    );
    protected static $not_checks = array('id', 'created', 'updated');
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'company_settings';
    protected static $_setting_type = 'company';

    /**
     * Function to add or update a company_settings
     * 
     * @author diennvt 
     * @param array $param Input data
     * @return bool Returns the boolean
     */
    public static function add_update($param) {
        //check if company_id exist or not
        $options['where'] = array(
            'id' => $param['company_id'],
        );
        $data = Model_Company::find('all', $options);
        if (empty($data)) {//if company_id not exist
            static::errorNotExist('company_id', $param['company_id']);
            return false;
        } else {//if company_id exist
            $values = json_decode($param['value']);
            foreach ($values as $val) {
                $options['where'] = array(
                    'company_id' => $param['company_id'],
                    'setting_id' => $val->setting_id
                );
                //check if update or insert
                $company_setting = self::find('first', $options);
                if (!empty($company_setting)) {//if exist then update
                    $company_setting->set('company_id', $param['company_id']);
                    $company_setting->set('setting_id', $val->setting_id);
                    $company_setting->set('value', $val->value);
                    $company_setting->update();
                } else {//if not exist then insert
                    $cs = new self;
                    $cs->set('company_id', $param['company_id']);
                    $cs->set('setting_id', $val->setting_id);
                    $cs->set('value', $val->value);
                    $cs->save();
                }
            }
        }
        return true;
    }

    /**
     * Function to disable or enable a company_setting
     * 
     * @author diennvt 
     * @param array $param Input data
     * @return bool Returns the boolean
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $companysetting = self::find($id);
            if (empty($companysetting)) {
                return false;
            }
            $companysetting->set('disable', $param['disable']);
            if (!$companysetting->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to get all user_setting
     *
     * @author diennvt
     * @param array $param Input data
     * @return mixed[] Return a array include total and data
     */
    public static function get_all($param) {
        $options['where'] = array(
            'id' => $param['company_id'],
        );
        $res = Model_Company::find('all', $options);
        if (empty($res)) {
            static::errorNotExist('company_id', $param['company_id']);
            return false;
        } else {
            $query = DB::select(
                    'settings.name', 
                    'settings.description', 
                    'settings.data_type', 
                    DB::expr("IF(ISNULL(" . self::$_table_name . '.value' . "), settings.value, " . self::$_table_name . '.value' . ") as value"), 
                    DB::expr("IF(ISNULL(" . self::$_table_name . '.id' . "), settings.id, " . self::$_table_name . '.id' . ") as id"),  
                    self::$_table_name . '.disable'
                    )
                    ->from('settings')
                    ->join(DB::expr("(SELECT * FROM ".self::$_table_name." WHERE company_id = {$param['company_id']}) AS ".self::$_table_name), 'LEFT')
                    ->on('settings.id', '=', self::$_table_name . '.setting_id')
                    ->where('settings.type', self::$_setting_type);

            if (!empty($param['name'])) {
                $query->where('settings.name', 'LIKE', "{$param['name']}%");
            }
            if (!empty($param['data_type'])) {
                $query->where('settings.data_type', $param['data_type']);
            }
            if (isset($param['disable']) && $param['disable'] != '') {
                $query->where('settings.disable', $param['disable']);
            }
            if (!empty($param['sort'])) {
                $sortExplode = explode('-', $param['sort']);
                if ($sortExplode[0] == 'created') {
                    $sortExplode[0] = self::$_table_name . '.created';
                }
                $query->order_by($sortExplode[0], $sortExplode[1]);
            } else {
                $query->order_by(self::$_table_name . '.created', 'DESC');
            }
            if (!empty($param['page']) && !empty($param['limit'])) {
                $offset = ($param['page'] - 1) * $param['limit'];
                $query->limit($param['limit'])->offset($offset);
            }

            $data = $query->execute()->as_array();
            $total = !empty($data) ? DB::count_last_query() : 0;
            return array($total, $data);
        }
    }

    /**
     * Function to add or update a companysettings
     *
     * @author diennvt 
     * @param array $param Input data
     * @return bool Returns the boolean
     */
    public static function multi_update($param) {
        $upd_data = json_decode($param['value']);

        if (empty($upd_data)) {
            return false;
        }
        //set infomation
        foreach ($upd_data as $row) {
            $companysetting = self::find($row->id);
            if (empty($companysetting)) {
                $cs = new self;
                $cs->set('company_id', $row->company_id);
                $cs->set('setting_id', $row->setting_id);
                $cs->set('value', $row->value);
                if (!$cs->save()) {
                    AppLog::info("Company setting insert failed", __METHOD__, $row);
                    return false;
                }
            } else {
                foreach ($row as $key => $val) {
                    if (in_array($key, self::$_properties) && !in_array($key, self::$not_checks)) {
                        if ($val != '') {
                            $companysetting->set($key, $val);
                        }
                    }
                }
                if (!$companysetting->update()) {
                    AppLog::info("Company setting update failed", __METHOD__, $row);
                    return false;
                }
            }
        }
        return true;
    }

}
