<?php

/**
 * Any query in Model Image.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Model_Image extends Model_Abstract {

    protected $_functionName = array(
        'user_id', //image_url <user> | thumbnail_img <user_recruiter>
        'company_id',
        'newsfeed_id'
    );

    /**
     * Crop Image for user | newsfeed | company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return string|bool Returns the string or the boolean.
     */
    public static function cropImage($param) {
        $x1 = !empty($param['x1']) ? intval($param['x1']) : 0;
        $y1 = !empty($param['y1']) ? intval($param['y1']) : 0;
        $x2 = !empty($param['x2']) ? intval($param['x2']) : 0;
        $y2 = !empty($param['y2']) ? intval($param['y2']) : 0;
        $background_color = !empty($param['background_color']) ? $param['background_color'] : "";
        
        if ($x1 == 0 && $y1 == 0 & $x2 == 0 && $y2 == 0 & $background_color == "") {
            //$this->_addError(self::ERROR_CODE_INVALED_PARAMETER, 'crop_size');
            static::errorParamInvalid('x1|x2|y1|y2', 0);
            return false;
        }
        $uploadConfig = Config::load('upload', true);
        $imageUrl = $param['image_url'];
        $imagePath = str_replace(Config::get('img_url'), $uploadConfig['img_dir'], $imageUrl);

        preg_match('/' . str_replace('/', '\/', Config::get('img_url')) . "/i", $imageUrl, $matches);
        if (!empty($matches)) {
            if (!file_exists($imagePath)) {
                static::errorNotExist('image_url', $imagePath);
                return false;
            }
            $imgDir = dirname($imagePath);
            $imgFileName = basename($imagePath);
            $originDir = $imgDir . '/origin';
            @mkdir($originDir, 0777, true);
            if (!file_exists($originDir . '/' . $imgFileName)) {
                Image::load($imagePath)->save($originDir . '/' . $imgFileName);
            }
            if(empty($background_color)){
                Image::load($imagePath)->crop($x1, $y1, $x2, $y2)->save($imagePath);
            }else{
                // Load Image by $imagePath and set backgroung for iamge 16:9;
                self::setBackgroudColorForImage($imagePath, $background_color);
            }
            
            return $imageUrl;
        } else {
            // action get image from url with format [http://www.nikkansports.com/mod/img/facebook_nikkansports_logo.jpg]
            $imageInfo = \Lib\Util::uploadFromImageUrl($imagePath);
            if (empty($imageInfo)) {
                static::errorOther(static::ERROR_CODE_OTHER_1, 'image url', "Can't save image form \"$imagePath\"");
                return false;
            }
            $imageFromURL = $imageInfo[1];
            $imagefromDir = $imageInfo[0];
            // Crop Image
            $imgDir = dirname($imagefromDir);
            $imgFileName = basename($imagefromDir);

            $originDir = $imgDir . '/origin';
            @mkdir($originDir, 0777, true);
            if (!file_exists($originDir . '/' . $imgFileName)) {
                Image::load($imagefromDir)->save($originDir . '/' . $imgFileName);
            }
            if(empty($background_color)){
                Image::load($imagefromDir)->crop($x1, $y1, $x2, $y2)->save($imagefromDir);
            }else{
                // Load Image by $imagePath and set backgroung for iamge 16:9;
                self::setBackgroudColorForImage($imagefromDir, $background_color);
            }
            
            // Update for User and user_recruiter
            if (isset($param['user_id'])) {
                // update image for user
                if ($param['field'] == 'image_url') {
                    $param = array(
                        'id' => $param['user_id'],
                        'image_url' => $imageFromURL
                    );
                    \Model_User::add_update($param);
                } elseif ($param['field'] == 'thumbnail_img') { // Update thumbnail_img for user_recruiter
                    $param = array(
                        'user_id' => $param['user_id'],
                        'image_url' => $imageFromURL
                    );
                    \Model_User_Recruiter::add_update($param);
                }
            }
            // Update for company
            if (isset($param['company_id'])) {
                // update image for user
                if ($param['field'] == 'thumbnail_img') {
                    $param = array(
                        'id' => $param['company_id'],
                        'thumbnail_img' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'background_img') { // Update background_img for company
                    $param = array(
                        'id' => $param['company_id'],
                        'background_img' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_what_img1') { // Update introduction_what_img1 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_what_img1' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_what_img2') { // Update introduction_what_img2 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_what_img2' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_why_img1') { // Update introduction_why_img1 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_why_img1' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_why_img2') { // Update introduction_why_img2 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_why_img2' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_how_img1') { // Update introduction_how_img1 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_how_img1' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_how_img2') { // Update introduction_how_img2 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_how_img2' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_likethis_img1') { // Update introduction_likethis_img1 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_likethis_img1' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                } elseif ($param['field'] == 'introduction_likethis_img2') { // Update introduction_likethis_img2 for company
                    $param = array(
                        'id' => $param['company_id'],
                        'introduction_likethis_img2' => $imageFromURL
                    );
                    \Model_Company::add_update($param);
                }
            }

            // Update for Newsfeed
            if (isset($param['newsfeed_id'])) {
                if ($param['field'] == 'image_url') {
                    $param = array(
                        'id' => $param['newsfeed_id'],
                        'image_url' => $imageFromURL
                    );
                    \Model_News_Feed::add_update($param);
                }
            }

            // Update for Subcategory
            if (isset($param['subcategory_id'])) {
                if ($param['field'] == 'image_url') {
                    $param = array(
                        'id' => $param['subcategory_id'],
                        'image_url' => $imageFromURL
                    );
                    \Model_Subcategory::add_update($param);
                }
            }

            // Update for Category
            if (isset($param['category_id'])) {
                if ($param['field'] == 'image_url') {
                    $param = array(
                        'id' => $param['category_id'],
                        'image_url' => $imageFromURL
                    );
                    \Model_Category::add_update($param);
                }
            }
            // Update for News_Site
            if (isset($param['newssite_id'])) {
                if ($param['field'] == 'thumbnail_img') {
                    $param = array(
                        'id' => $param['newssite_id'],
                        'thumbnail_img' => $imageFromURL
                    );
                    \Model_News_Site::add_update($param);
                }
            }

            return $imageFromURL;
        }
    }

    private static function setBackgroudColorForImage($imagePath,$background_color){
         if(!file_exists($imagePath)){
           static::errorOther(static::ERROR_CODE_OTHER_1, 'image URL', __('Image do not exist.'));
                    return false;
         }
        list($width, $height) = getimagesize($imagePath);
        $new_width  = $height * 16 / 9;
        $new_height = $height;

        // Create new image with backbround color $background_color
        $im     = imagecreatetruecolor($new_width, $new_height)
                    or die('Cannot Initialize new GD image stream');
        $color  = \Lib\Util::rgb2hex2rgb($background_color);
        $pos    =strrpos($imagePath, ".");
        $imagePathTemp  =  substr_replace($imagePath, '__'.rand(1,100000)."__TEMP", $pos, 0);

        $color_background = imagecolorallocatealpha($im, $color['red'], $color['green'], $color['blue'],127);
        imagefill($im, 0, 0, $color_background); 
         $fileType = exif_imagetype($imagePath);
         switch ($fileType) {
            case IMAGETYPE_JPEG:
                 # code for jpg type
                  // Save the image
                    imagejpeg($im,$imagePathTemp);
                    // Load image above save to merge image first
                    $dest   = imagecreatefromjpeg($imagePathTemp);
                    $src    = imagecreatefromjpeg($imagePath);
                    imagecopymerge($dest, $src, 
                                ($new_width - $width)/2, 
                                ($new_height- $height)/2, 
                                0, 
                                0, 
                                $width, 
                                $height, 100);
                     // Output and free from memory
                    imagejpeg($dest,$imagePath);
                    imagedestroy($im);
                    imagedestroy($src);
                    imagedestroy($dest);
                    
                 break;

            case IMAGETYPE_GIF:
                 # code for jpg type
                    // Save the image
                    imagegif($im,$imagePathTemp);
                    // Load image above save to merge image first
                    $dest   = imagecreatefromgif($imagePathTemp);
                    $src    = imagecreatefromgif($imagePath);
                    imagecopymerge($dest, $src, 
                                ($new_width - $width)/2, 
                                ($new_height- $height)/2, 
                                0, 
                                0, 
                                $width, 
                                $height, 100);
                    imagegif($dest,$imagePath);
                     // Output and free from memory
                    imagedestroy($im);
                    imagedestroy($src);
                    imagedestroy($dest);
                    
                 break;

             case IMAGETYPE_PNG:
                # code for png type
                // Save the image
                imagepng($im,$imagePathTemp);
                // Load image above save to merge image first
                $dest   = imagecreatefrompng($imagePathTemp);
                $src    = imagecreatefrompng($imagePath);
                imagecopy($dest, $src, 
                            ($new_width - $width)/2, 
                            ($new_height- $height)/2, 
                            0, 
                            0, 
                            $width, 
                            $height);
                 // Output and free from memory

                imagepng($dest,$imagePath);
                imagedestroy($im);
                imagedestroy($src);
                imagedestroy($dest);
                break;
             default:
                    static::errorOther(static::ERROR_CODE_OTHER_1, 'image TYPE', __('Not support image type: {$fileType}'));
                    return false;
                 break;
         }
        // Check to delete image TEMP
         if(file_exists($imagePathTemp)){
            unlink($imagePathTemp);
         }
         return $imagePath;
    }
}
