<?php

/**
 * Model_Category - Model to operate to categories's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Category extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'name',
        'image_url',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'categories';

    /**
     * Get_list - function to get a list of category.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $query = DB::select()->from('categories');

        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to disable or enable a category.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Return the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $category = self::find($id);
            if (empty($category)) {
                return false;
            }
            $category->set('disable', $param['disable']);
            if (!$category->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to add or update a category.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or boolean.
     */
    public static function add_update($param) {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $category = new self;
        if (!empty($id)) {
            $category = self::find($id);
            if (empty($category)) {
                static::errorNotExist('category_id', $param['id']);
                return false;
            }
        }        
        if (isset($param['name'])) {
            $category->set('name', $param['name']);
        }
        if (isset($param['image_url'])) {
            $category->set('image_url', $param['image_url']);
        }
        //check id for adding new or updating
        if ($category->save()) {
            if (empty($category->id)) {
                $category->id = self::cached_object($category)->_original['id'];
            }
            return !empty($category->id) ? $category->id : 0;
        }
        return false;
    }

    /**
     * Function to get a list of category.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function mobile_get_list($param) {
        $query = DB::select()->from('categories');

        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();

        return !empty($data) ? $data : array();
    }

}
