<?php

/**
 * Model_Subcategory - Model to operate to subcategories's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Subcategory extends Model_Abstract
{

    protected static $_properties = array(
        'id',
        'category_id',
        'name',
        'image_url',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'subcategories';

    /**
     * Function to get a list of subcategory
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select('subcategories.id',
                'subcategories.category_id',
                array('subcategories.name','subcategory_name'),
                 'subcategories.image_url',
                'subcategories.disable',
                'subcategories.created',
                'subcategories.updated',
                array('categories.name','category_name'))
                ->from('subcategories')
                ->join('categories')
                ->on('subcategories.category_id', '=', 'categories.id');
        if (!empty($param['name']))
        {
            $query->where('subcategories.name', 'LIKE', "{$param['name']}%");
        }
        if (!empty($param['category_id']))
        {
            $query->where('subcategories.category_id', $param['category_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '')
        {
            $query->where('subcategories.disable', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to disable or enable a subcategory.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id']))
        {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id)
        {
            $subcategory = self::find($id);
            if (empty($subcategory))
            {
                return false;
            }
            $subcategory->set('disable', $param['disable']);
            if (!$subcategory->update())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to add or update a subcategory
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param)
    {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $subcategory = new self;
        if (!empty($id))
        {
            $subcategory = self::find($id);
            if (empty($subcategory))
            {
                 static::errorNotExist('subcategory_id', $param['id']);
                return false;
            }
        }

        // set information for subcategory if having
        if (!empty($param['category_id']))
        {
            $subcategory->set('category_id', $param['category_id']);
        }

        if (!empty($param['name']))
        {
            $subcategory->set('name', $param['name']);
        }

        if (!empty($param['image_url']))
        {
            $subcategory->set('image_url', $param['image_url']);
        }

        //check id for adding new or updating
        if ($subcategory->save())
        {
            if (empty($subcategory->id))
            {
                $subcategory->id = self::cached_object($subcategory)->_original['id'];
            }
            return !empty($subcategory->id) ? $subcategory->id : 0;
        }
        return false;
    }

}
