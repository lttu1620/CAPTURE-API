<?php

/**
 * Any query in Model Invite
 *
 * @package Model
 * @created 2015-04-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Invite extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'email',
        'status',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'invites';

    /**
     * Add or update info for Invite
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Invite id or false if error
     */
    public static function add_update($param)
    {
        $adminRecruiter = Model_User_Recruiter::find(
            'first',
            array(
                'where' => array(
                    'user_id'     => $param['user_id'],
                    'is_admin'    => '1',
                    'is_approved' => '1',
                    'disable'     => '0',
                ),
            )
        );
        if (!$adminRecruiter) {
            self::errorOther(self::ERROR_CODE_OTHER_1, 'user_id', 'This user is not admin!');
            return false;
        }
        $profile = Model_User_Profile::find(
            'first',
            array(
                'where' => array(
                    'email' => $param['email'],
                ),
            )
        );
        if ($profile) {
            self::errorDuplicate('email', $param['email']);
            return false;
        }
        if (isset($param['status']) && $param['status'] == '0') {
            if (!\Lib\Email::sendInviteEmail($param)) {
                \LogLib::warning('Can not send invite email', __METHOD__, $param);
                return false;
            }
        } elseif (isset($param['status']) && $param['status'] == '1') {
            // approve user
        } else {
            $invite = self::find(
                'first',
                array(
                    'where' => array(
                        'user_id' => $param['user_id'],
                        'email'   => $param['email'],
                    ),
                )
            );
            if ($invite) {
                self::errorDuplicate('email', $param['email']);
                return false;
            } else {
                $invite = new self;
                $invite->set('user_id', $param['user_id']);
                $invite->set('email', $param['email']);
                $invite->set('status', '0');
                if (!$invite->save()) {
                    return false;
                } else {
                    if (!\Lib\Email::sendInviteEmail($param)) {
                        \LogLib::warning('Can not send sent email', __METHOD__, $param);
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Get list Invite (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Invite
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.*'
        )
            ->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name.'.user_id', '=', $param['user_id']);
        }
        if (!empty($param['email'])) {
            $query->where(self::$_table_name.'.email', '=', $param['email']);
        }
        if (isset($param['status']) && $param['status'] != '') {
            $query->where(self::$_table_name.'.status', '=', $param['status']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Disable/Enable list Invite
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $invite = self::find($id);
            if ($invite) {
                $invite->set('disable', $param['disable']);
                if (!$invite->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('invite_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Update status Invite
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function status($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $invite = self::find($id);
            if ($invite) {
                $invite->set('status', $param['status']);
                if (!$invite->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('invite_id');
                return false;
            }
        }
        return true;
    }
}
