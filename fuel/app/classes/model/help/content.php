<?php

/**
 * Any query for model help content.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Help_Content extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'title',
        'description',
        'body',
        'disable',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'help_contents';

    /**
     * Get list help content.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);

        if (!empty($param['title'])) {
            $query->where('title', 'LIKE', "%{$param['title']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any help contents.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $content = self::find($id);
            if ($content) {
                $content->set('disable', $param['disable']);
                if (!$content->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('help_content_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for help contents.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        try {
            //check id if existing
            $id = !empty($param['id']) ? $param['id'] : 0;
            $content = new self;
            if (!empty($id)) {
                $content = self::find($id);
                if (empty($content)) {
                    static::errorNotExist('help_content_id', $id);
                    return false;
                }
            }

            if (!empty($param['title'])) {
                $content->set('title', $param['title']);
            }
            if (!empty($param['description'])) {
                $content->set('description', $param['description']);
            }
            if (!empty($param['body'])) {
                $content->set('body', $param['body']);
            }

            if ($content->save()) {
                if (empty($content->id)) {
                    $content->id = self::cached_object($content)->_original['id'];
                }
                return !empty($content->id) ? $content->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}