<?php

/**
 * Query for model Start Logs.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Start_Log extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'device',
        'created',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        )
    );
    protected static $_table_name = 'start_logs';

    /**
     * Get list start logs.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $query = DB::select(
                        self::$_table_name . '.*',
                        DB::expr("CASE start_logs.device
                                  WHEN " .Config::get('web_login_device')." THEN '". Config::get('os')['webos'].
                                  "' WHEN " .Config::get('ios_login_device'). " THEN '" .Config::get('os')['ios'].
                                  "' WHEN " .Config::get('android_login_device'). " THEN '" .Config::get('os')['android'].
                                  "' END as device_name")
                )
                ->from(self::$_table_name);

        if (!empty($param['device'])) {
            $query->where(self::$_table_name . '.device', '=', $param['device']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Add info for start logs.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add() {
        $data['os'] = \Lib\Util::os();
        $device = 0;
        switch ($data['os']) {
            case Config::get('os')['webos']:
                $device = Config::get('web_login_device');
                break;
            case Config::get('os')['ios']:
                $device = Config::get('ios_login_device');
                break;
            case Config::get('os')['android']:
                $device = Config::get('android_login_device');
                break;
            default :
                $device = Config::get('other_login_device');               
        }
        $log = new self;       
        $log->set('device', $device);       
        if ($log->create()) {
            $log->id = self::cached_object($log)->_original['id'];
            return !empty($log->id) ? $log->id : 0;
        }
        return false;
    }

}
