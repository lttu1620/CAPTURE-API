<?php

/**
 * Any query for model follow category.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Follow_Category extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'category_id',
        'user_id',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'follow_categories';

    /**
     * Get list follow category.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array include total and data.
     */
    public static function get_list($param) {
        $query = DB::select(
                array('categories.name', 'category_name'), array('users.name', 'user_name'), array('users.nickname', 'user_nickname'), array('user_profiles.email', 'email'), DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url"), self::$_table_name . '.*'
            )
            ->from(self::$_table_name)
            ->join('categories', 'LEFT')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('user_profiles', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['category_id'])) {
            $query->where(self::$_table_name . '.category_id', '=', $param['category_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail of follow categories.
     *
     * @author Le Tuan Tu
     * @param int $id Input data.
     * @return array Returns the array.
     */
    public static function get_detail($id) {
        $query = DB::select(
                array('categories.name', 'category_name'), array('users.name', 'user_name'), self::$_table_name . '.*'
            )
            ->from(self::$_table_name)
            ->join('categories', 'LEFT')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where(self::$_table_name . '.id', '=', $id);
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $id);
        }
        return $data ? $data[0] : array();
    }

    /**
     * Add info for follow category
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array
     * @return bool Returns the boolean.
     */
    public static function add($param) {
        try {
            $options['where'][] = array(
                'category_id' => $param['category_id'],
                'user_id' => $param['user_id']
            );
            $follow = self::find('first', $options);
            if (!$follow) {
                $follow = new self;
                $follow->set('category_id', $param['category_id']);
                $follow->set('user_id', $param['user_id']);
                if ($follow->create()) {
                    if (\Model_Follow_Category_Log::add(array(
                            'category_id' => $param['category_id'],
                            'user_id' => $param['user_id'],
                            'unfollow' => '0'
                        ))) {
                        return true;
                    }
                }
            } else {
                if ($follow->_data['disable'] == '1') {
                    $follow->set('disable', '0');
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('follow_category_id');
                    return false;
                }
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Disable a follow category.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        try {
            if (!isset($param['disable'])) {
                $param['disable'] = '1';
            }
            if (!empty($param['id'])) {
                $options['where'][] = array(
                    'id' => $param['id'],
                );
            } else {
                $options['where'][] = array(
                    'category_id' => $param['category_id'],
                    'user_id' => $param['user_id'],
                );
            }
            $follow = self::find('first', $options);
            if ($follow) {
                if ($follow->_data['disable'] != $param['disable']) {
                    $follow->set('disable', $param['disable']);
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('follow_category_id');
                }

                return false;
            } else {
                self::errorNotExist('follow_category_id');
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Update follow category.
     *
     * @author diennvt
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function mobile_update($param)
    {

        $follow_categories = self::get_followCategories_mobile(array('user_id' => $param['user_id']));
        if (!empty($follow_categories))
        {
            \LogLib::info('Have data ' . self::$_table_name, __METHOD__, $follow_categories);
            $values = json_decode($param['value']);
            foreach ($values as $val)
            {
                \LogLib::info('val', __METHOD__, $val);
                foreach ($follow_categories as $fc)
                {
                    \LogLib::info('fc', __METHOD__, $fc);
                    if ($val->category_id == $fc['id'])//check if category_id is valid
                    {
                        \LogLib::info('Category_id is valid', __METHOD__, $val);
                        $options['where'] = array(
                            'category_id' => $val->category_id,
                            'user_id' => $param['user_id']);

                        $fc_update = self::find('first', $options);
                        if (!empty($fc_update))// && $fc['follow_category_id'] != null check for updating
                        {
                            \LogLib::info('update ' . self::$_table_name, __METHOD__, $val);
                            $fc_update->set('disable', $val->disable);
                            if (!$fc_update->update())
                            {
                                \LogLib::info('update failed ' . self::$_table_name, __METHOD__, $val);
                                return false;
                            }
                        }
                        else//inserting
                        {
                            \LogLib::info('Insert new ' . self::$_table_name, __METHOD__, $val);
                            $fc_insert = new self;
                            $fc_insert->set('category_id', $val->category_id);
                            $fc_insert->set('user_id', $param['user_id']);
                            $fc_insert->disable = $val->disable;
                            if (!$fc_insert->save())
                            {
                                \LogLib::info('Save failed', __METHOD__, $val);
                                return false;
                            }
                        }
                        break;
                    }
                }
            }
            return true;
        }
        else
        {
            \LogLib::info('Have no data' . self::$_table_name, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Function to get a list of category.
     *
     * @author tuancd 
     * @param array $param Input user_id
     * @return array Returns array(id, name, disable).  
     */
    public static function get_followCategories_mobile($param) {
        // Check user is_exists first   
        $options['where']= array(
            'id'=> isset($param['user_id'])?$param['user_id']:0,
            'disable'=>0
        );
        $profile =   \Model_User::find('first', $options);
        if (empty($profile)) {
            self::errorNotExist('user_id', $param['user_id']);
            return false;
        }
        $query = DB::select(
                'categories.id', 
                'categories.name', 
                array('follow_categories.id', 'follow_category_id'), 
                DB::expr("IF (follow_categories.category_id IS NULL OR follow_categories.disable = 1, 1, 0)  AS disable"),
                DB::expr("IF (follow_categories.category_id IS NULL OR follow_categories.disable = 1, 0, 1)  AS selected")
            )
            ->from('categories')
            ->join(
                DB::expr("(
                    select *
                    from follow_categories 
                    where follow_categories.user_id = {$param['user_id']}
                    and disable = 0
                ) AS follow_categories"), 'LEFT'
            )
            ->on('categories.id', '=', 'follow_categories.category_id');
        $data = $query->execute()->as_array();

        return $data;
    }

}
