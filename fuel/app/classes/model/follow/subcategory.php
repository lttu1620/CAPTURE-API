<?php

/**
 * Any query for model follow subcategory.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Follow_Subcategory extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'subcategory_id',
		'user_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'follow_subcategories';

    /**
     * Get list follow subcategory.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            array('subcategories.name', 'subcategory_name'),
            array('users.name', 'user_name'),
            array('users.nickname', 'user_nickname'),
            array('user_profiles.email', 'email'),
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name)
            ->join('subcategories', 'LEFT')
            ->on(self::$_table_name . '.subcategory_id', '=', 'subcategories.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('user_profiles', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['subcategory_id'])) {
            $query->where(self::$_table_name . '.subcategory_id', '=', $param['subcategory_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Add info for follow subcategory.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function add($param)
    {
        try {
            $options['where'][] = array(
                'subcategory_id' => $param['subcategory_id'],
                'user_id'        => $param['user_id']
            );
            $follow = self::find('first', $options);
            if (!$follow) {
                $follow = new self;
                $follow->set('subcategory_id', $param['subcategory_id']);
                $follow->set('user_id', $param['user_id']);
                if ($follow->create()) {
                    if (\Model_Follow_Subcategory_Log::add(array(
                        'subcategory_id' => $param['subcategory_id'],
                        'user_id'        => $param['user_id'],
                        'unfollow'       => '0'
                    ))) {
                        return true;
                    }
                }
            } else {
                if ($follow->_data['disable'] == '1') {
                    $follow->set('disable', '0');
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('follow_subcategory_id');
                    return false;
                }
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Disable a follow subcategory.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        try {
            if (!isset($param['disable'])) {
                $param['disable'] = '1';
            }
            if (!empty($param['id'])) {
                $options['where'][] = array(
                    'id'      => $param['id'],
                );
            } else {
                $options['where'][] = array(
                    'subcategory_id' => $param['subcategory_id'],
                    'user_id'     => $param['user_id'],
                );
            }
            $follow = self::find('first', $options);
            if ($follow) {
                if ($follow->_data['disable'] != $param['disable']) {
                    $follow->set('disable', $param['disable']);
                    if ($follow->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('follow_subcategory_id');
                }

                return false;
            } else {
                self::errorNotExist('follow_subcategory_id');
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
