<?php

/**
 * Any query for model follow company.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Follow_Company extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'company_id',
        'user_id',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'follow_companies';

    /**
     * Get user follow company.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_user_follow_company($param)
    {
        $query = DB::select(
            array('users.nickname', 'user_name'),
			array('users.nickname', 'nickname'),
            DB::expr(
                "IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get(
                    'no_image_user'
                ) . "') AS user_image_url"
            ),
            self::$_table_name . '.user_id'
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('companies', 'LEFT')
            ->on(self::$_table_name . '.company_id', '=', 'companies.id')
            ->where(self::$_table_name . '.company_id', '=', $param['id'])
            ->where('users.disable', '=', '0')
            ->where('companies.disable', '=', '0')
            ->where(self::$_table_name . '.disable', '=', '0');
        $data = $query->execute()->as_array();

        return $data ? $data : array();
    }

    /**
     * add info for follow company
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|mixed
     */
    public static function add($param)
    {
        $options['where'][] = array(
            'company_id' => $param['company_id'],
            'user_id'    => $param['user_id']
        );
        $follow = self::find('first', $options);
        if (!$follow) {
            $follow = new self;
            $follow->set('company_id', $param['company_id']);
            $follow->set('user_id', $param['user_id']);
            if ($follow->create()) {
                if (\Model_Follow_Company_Log::add(array(
                        'company_id' => $param['company_id'],
                        'user_id'    => $param['user_id'],
                        'unfollow'   => '0'
                    )
                )) {
                    return true;
                }
            }
        } else {
            if ($follow->get('disable') == '1') {
                $follow->set('disable', '0');
                if ($follow->update()) {
                    if (\Model_Follow_Company_Log::add(
                        array(
                            'company_id' => $param['company_id'],
                            'user_id'    => $param['user_id'],
                            'unfollow'   => '0'
                        )
                    )) {
                        return true;
                    }
                }
            } else {
                self::errorDuplicate('follow_company_id');

                return false;
            }
        }

        return false;
    }

    /**
     * disable follow company
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception
     * @return int|mixed
     */
    public static function disable($param)
    {
        try {
            if (!isset($param['disable'])) {
                $param['disable'] = '1';
            }
            if (!empty($param['id'])) {
                $options['where'][] = array(
                    'id' => $param['id'],
                );
            } else {
                $options['where'][] = array(
                    'company_id' => $param['company_id'],
                    'user_id'    => $param['user_id'],
                );
            }
            $follow = self::find('first', $options);
            if ($follow) {
                if ($follow->get('disable') != $param['disable']) {
                    $follow->set('disable', $param['disable']);
                    if ($follow->update()) {
                        if (\Model_Follow_Company_Log::add(
                            array(
                                'company_id' => $follow->get('company_id'),
                                'user_id'    => $follow->get('user_id'),
                                'unfollow'   => $param['disable']
                            )
                        )
                        ) {
                            return true;
                        }
                    }
                } else {
                    self::errorDuplicate('follow_company_id');
                }

                return false;
            } else {
                self::errorNotExist('follow_company_id');

                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * get list follow company
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array
     */
    public static function get_list($param)
    {
        $query = DB::select(
            array('companies.name', 'company_name'),
            array('users.name', 'user_name'),
            DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username"),
            DB::expr(
                "IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get(
                    'no_image_user'
                ) . "') AS image_url"
            ),
            array('users.nickname', 'user_nickname'),
            array('user_profiles.email', 'email'),
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name)
            ->join('companies', 'LEFT')
            ->on(self::$_table_name . '.company_id', '=', 'companies.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('user_profiles', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

        if (!empty($param['company_id'])) {
            $query->where('companies.id', '=', $param['company_id']);
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * <get follow count from follow company>
     *
     * @author diennvt
     * @param int $company_ids company_id
     * @return mixed
     */
    public static function get_follow_count($company_ids)
    {
        $query = DB::select(
            self::$_table_name . ".company_id",
            DB::expr("COUNT(" . self::$_table_name . ".id) AS follow_count")
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . ".disable", 0)
            ->group_by(self::$_table_name . ".company_id");

        if(!empty($company_ids)){
             $query->where(DB::expr("EXISTS ( SELECT id 
                                            FROM companies
                                            WHERE follow_companies.company_id = companies.id  AND disable = 0 AND follow_companies.company_id IN ({$company_ids}))"));
        }

        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }

    /**
     * <Get list follow company on mobile>
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array
     */
    public static function mobile_get_list($param)
    {
        $query = DB::select(
            'companies.id','companies.name','companies.kana','companies.address','companies.description_short','companies.description',
            DB::expr("IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'" . \Config::get('mobile_no_image_company') . "') AS thumbnail_img"),
            'companies.introduction_movie','companies.corporate_url','companies.corporate_facebook','companies.corporate_twitter','companies.corporate_entry',
            'companies.ower_user_id','companies.name_english','companies.establishment_date','companies.employees','companies.map_url','companies.follow_count',
            'companies.disable','companies.created','companies.updated'
        )
            ->from(self::$_table_name)
            ->join('companies', 'LEFT')
            ->on(self::$_table_name . '.company_id', '=', 'companies.id')
            ->where(self::$_table_name . '.disable',0);
        if (isset($param['user_id']) && $param['user_id'] != '') {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        if (isset($data) && !empty($data)) {
            $_param = array(
                'company_id' => \Lib\Arr::field($data, 'id', true)
            );
            $listCategory = \Model_Company_Category::get_listcategory_by_companyid($_param);
            foreach ($data as $key => $info) {
                $companyCatergory = \Lib\Arr::filter($listCategory, 'company_id', $info['id']);
                $data[$key]['category_name'] = str_replace(","," 、", \Lib\Arr::field($companyCatergory, 'category_name', true));
            }
        }
        return array('total' => $total, 'data' => $data);
    }
}
