<?php

/**
 * Any query for model Push Messages.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Push_Message extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'company_id',
        'message',
        'android_icon',
        'ios_sound',
        'send_reservation_date',
        'rich_url',
        'is_sent',
        'sent_date',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'push_messages';

    /**
     * Get list push messages.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $query = DB::select(
                        array('companies.name', 'company_name'), self::$_table_name . '.*'
                )
                ->from(self::$_table_name)
                ->join('companies', 'LEFT')
                ->on('companies.id', '=', self::$_table_name . '.company_id');
        if (!empty($param['company_id'])) {
            $query->where('company_id', '=', $param['company_id']);
        }
        if (!empty($param['message'])) {
            $query->where('message', 'LIKE', "%{$param['message']}%");
        }
        if (!empty($param['android_icon'])) {
            $query->where('android_icon', 'LIKE', "%{$param['android_icon']}%");
        }
        if (isset($param['ios_sound']) && $param['ios_sound'] != '') {
            $query->where('ios_sound', '=', $param['ios_sound']);
        }
        if (!empty($param['send_reservation_date'])) {
            $query->where('send_reservation_date', '=', $param['send_reservation_date']);
        }
        if (!empty($param['rich_url'])) {
            $query->where('rich_url', 'LIKE', "%{$param['rich_url']}%");
        }
        if (isset($param['is_sent']) && $param['is_sent'] != '') {
            $query->where('is_sent', '=', $param['is_sent']);
        }
        if (!empty($param['sent_date'])) {
            $query->where('sent_date', '=', $param['sent_date']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.send_reservation_date', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.send_reservation_date', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any push messages.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $message = self::find($id);
            if ($message) {
                $message->set('disable', $param['disable']);
                if (!$message->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('push_messages_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for push messages.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param) {
        try {
            //check id if existing
            $id = !empty($param['id']) ? $param['id'] : 0;
            $message = new self;
            if (!empty($id)) {
                $message = self::find($id);
                if (empty($message)) {
                    static::errorNotExist('push_messages_id', $id);
                    return false;
                }
            }

            if (!empty($param['company_id'])) {
                $message->set('company_id', $param['company_id']);
            }
            if (!empty($param['message'])) {
                $message->set('message', $param['message']);
            }
            if (!empty($param['android_icon'])) {
                $message->set('android_icon', $param['android_icon']);
            }
            if (isset($param['ios_sound']) && $param['ios_sound'] != '') {
                $message->set('ios_sound', $param['ios_sound']);
            }
            if (!empty($param['send_reservation_date'])) {
                $message->set('send_reservation_date', $param['send_reservation_date']);
            }
            if (!empty($param['rich_url'])) {
                $message->set('rich_url', $param['rich_url']);
            }

            // is_sent has default value
            $message->set('is_sent', $param['is_sent']);

            if (!empty($param['sent_date'])) {
                $message->set('sent_date', $param['sent_date']);
            }

            if ($message->save()) {
                if (empty($message->id)) {
                    $message->id = self::cached_object($message)->_original['id'];
                }
                return !empty($message->id) ? $message->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get list push messages for Task PushMessage to device.
     *
     * @author Tuancd
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_list_push_message_for_task() {
        $option['where'] = array(
            'is_sent' => 0,
            'disable' => 0,
            array(
                'send_reservation_date', '<=', time()
            )
        );
        $option['order_by'] = array(
            'send_reservation_date' => 'asc'
        );
        $data = self::find('first', $option);
        return $data;
    }

}
