<?php

use Lib\Str;
use Lib\Util;
/**
 * Model_User - Model to operate to User's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_User extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'app_id',
        'name',
        'nickname',
        'university_id',
        'department_id',
        'campus_id',
        'enrollment_year',
        'sex_id',
        'is_company',
        'is_ios',
        'is_android',
        'disable',
        'image_url',
        'additional_year',
        'introduction_text',
        'created',
        'updated',
    );
    protected static $_mobile_properties = array(
        'id',
        'app_id',
        'name',
        'nickname',
        'university_id',
        'department_id',
        'campus_id',
        'enrollment_year',
        'sex_id',
        'disable',
        'image_url',
        'additional_year',
        'introduction_text',
        'is_ios',
        'is_android',
        'created',
        'updated',
    );
    
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'users';

    /**
     * Function to get a list of user.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $page = !empty($param['page']) ? $param['page'] : 1;
        $limit = !empty($param['limit']) ? $param['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $query = DB::select(
                    'users.id',
                    'users.app_id',
                    'users.name',
                    'users.nickname',
                    'users.university_id',
                    'users.department_id',
                    'users.campus_id',
                    'users.enrollment_year',
                    'users.sex_id',
                    'users.created',
                    'users.updated',            
                    self::$_table_name. '.is_company',
                    DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url"),
                    'user_profiles.user_id', 'user_profiles.email', 'user_profiles.password', 
                    'user_recruiters.company_id', array('companies.name', 'company_name'), 
                    array('companies.kana', 'company_kana'), array('companies.address', 'company_address'), 
                    array('companies.description_short', 'company_description_short'), 
                    array('companies.description', 'company_description'), 
                    DB::expr("IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'" . \Config::get('no_image_company') . "') AS company_thumbnail_img"),
                    array('companies.introduction_movie', 'company_introduction_movie'), 
                    array('companies.background_img', 'company_background_img'), 
                    array('companies.corporate_url', 'company_corporate_url'), 
                    array('companies.corporate_facebook', 'company_corporate_facebook'), 
                    array('companies.corporate_twitter', 'company_corporate_twitter'), 
                    array('companies.corporate_entry', 'company_corporate_entry'), 'user_recruiters.company_id', 
                    array('user_recruiters.introduction_text', 'recruiter_introduction_text'), 
                    array('user_recruiters.is_admin', 'recruiter_admin'), 
                    DB::expr("IFNULL(IF(user_recruiters.thumbnail_img='',NULL,user_recruiters.thumbnail_img),'" . \Config::get('no_image_user') . "') AS thumbnail_img"),
                    'user_recruiters.is_admin', 'user_recruiters.is_approved', 'user_facebook_informations.facebook_id', 
                    'user_facebook_informations.facebook_email', 'user_facebook_informations.facebook_name', 
                    'user_facebook_informations.facebook_first_name', 'user_facebook_informations.facebook_last_name', 
                    'user_facebook_informations.facebook_gender', 'user_facebook_informations.facebook_link', 
                    'user_facebook_informations.facebook_image', array('universities.name', 'university_name'), 
                    array('universities.kana', 'university_kana'), array('campuses.name', 'campuses_name'), 
                    array('campuses.address', 'campuses_address'), array('campuses.access_method', 'campuses_access_method'), 
                    array('campuses.prefecture_id', 'campuses_prefecture_id'), 
                    array('campuses.prefecture_id', 'campuses_prefecture_id'), 
                    DB::expr("IF (users.is_ios = 1, 1, IF (users.is_android = 1, 2, 0)) AS app")
                )
                ->from('users')
                ->join('user_profiles')
                ->on('users.id', '=', 'user_profiles.user_id')
                ->join('user_recruiters', 'LEFT')
                ->on('users.id', '=', 'user_recruiters.user_id')
                ->join('companies', 'LEFT')
                ->on('user_recruiters.company_id', '=', 'companies.id')
                ->join('user_facebook_informations', 'LEFT')
                ->on('users.id', '=', 'user_facebook_informations.user_id')
                ->join('universities', 'LEFT')
                ->on('users.university_id', '=', 'universities.id')
                ->join('campuses', 'LEFT')
                ->on('users.campus_id', '=', 'campuses.id')
                ->limit($limit)
                ->offset($offset);

        if (!empty($param['university_id'])) {
            $query->where('users.university_id', '=', $param['university_id']);
        }
        if (!empty($param['campus_id'])) {
            $query->where('users.campus_id', '=', $param['campus_id']);
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "{$param['email']}%");
        }
        if (!empty($param['sex_id'])) {
            $query->where('sex_id', $param['sex_id']);
        }
        if (isset($param['is_company']) && ($param['is_company'] == 0 || $param['is_company'] == 1)) {
            $query->where('is_company', $param['is_company']);
        }
        if (!empty($param['is_ios'])) {
            $query->where('is_ios', $param['is_ios']);
        }
        if (!empty($param['is_android'])) {
            $query->where('is_android', $param['is_android']);
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "{$param['nickname']}%");
        }
        if (!empty($param['disable']) && $param['disable'] != '') {
            $query->where('users.disable', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to disable or enable a user.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $user = self::find($id);
            if (empty($user)) {
                return false;
            }
            $user->set('disable', $param['disable']);
            if (!$user->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to add or update a user.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param, $encodePwd = true) {       
        $id = !empty($param['id']) ? $param['id'] : 0; 
        $param['university_id'] = empty($param['university_id'])?  0 : $param['university_id'] ;
        $param['department_id'] = empty($param['department_id'])?  0 : $param['department_id'] ;
        $param['campus_id'] = empty($param['campus_id'])?  0 : $param['campus_id'] ;
        $param['enrollment_year'] = empty($param['enrollment_year'])?  0 : $param['enrollment_year'] ;
        $param['additional_year'] = empty($param['additional_year'])?  0 : $param['additional_year'] ;
        $param['os'] = \Lib\Util::os();
        if (!empty($param['device'])) {
            $param['os'] = $param['device'];
        } 
        if ($param['os'] == \Config::get('os')['ios']) {           
            $param['is_ios'] = 1;
        } elseif ($param['os'] == \Config::get('os')['android']) {
            $param['is_android'] = 1;
        } else {
            $param['is_ios'] = 0;
            $param['is_android'] = 0;
        }       
        $user = new self;
        if (!empty($id)) {
            $user = self::find($id);
            if (empty($user)) {
                static::errorNotExist('user_id', $param['id']);
                return false;
            }
        } elseif (!empty($param['email'])) {
            //Check Email in system
            $options['where'] = array(
                'email' => $param['email']
            );
            $data = \Model_User_Profile::find('first', $options);
            if (!empty($data)) {
                static::errorDuplicate('email', $param['email']);
                return false;
            }
        }
        if (empty($user->get('id'))) {
            // case new user
            $user_guest= new \Model_User_Guest_Id;
            if (isset($param['device_id'])) {
                $user_guest->set('device_id', $param['device_id']);
            }
            $user_guest->set('type', "user");
            $user_guest->set('device', \Lib\Util::deviceId($param['os']));
            if (!$user_guest->create()) {
                return false;
            }
            $user_id = \Model_User_Guest_Id::cached_object($user_guest)->_original['id'];
            $user->set('id', $user_id);
        }
        // set information for user if having
        if (isset($param['app_id'])) {
            $user->set('app_id', $param['app_id']);
        }
        if (isset($param['name'])) {
            $user->set('name', $param['name']);
        }
        if (isset($param['nickname'])) {
            $user->set('nickname', $param['nickname']);
        }
        if (isset($param['university_id'])) {
            $user->set('university_id', $param['university_id']);
        }
        if (isset($param['department_id'])) {
            $user->set('department_id', $param['department_id']);
        }
        if (isset($param['campus_id'])) {
            $user->set('campus_id', $param['campus_id']);
        }
        if (isset($param['enrollment_year'])) {
            $user->set('enrollment_year', $param['enrollment_year']);
        }
        if (isset($param['sex_id']) && $param['sex_id'] != '') {
            $user->set('sex_id', $param['sex_id']);
        }
        if (isset($param['is_company'])) {
            $user->set('is_company', $param['is_company']);
        }
        if (isset($param['is_ios'])) {
            $user->set('is_ios', $param['is_ios']);
        }
        if (isset($param['is_android'])) {
            $user->set('is_android', $param['is_android']);
        }        
        if (isset($param['disable']) && $param['disable'] != '') {
            $user->set('disable', $param['disable']);
        }
        if (isset($param['image_url'])) {
            $user->set('image_url', $param['image_url']);
        }
        if (isset($param['additional_year'])) {
            $user->set('additional_year', $param['additional_year']);
        }
        if (isset($param['introduction_text'])) {
            $user->set('introduction_text', $param['introduction_text']);
        }        
        //check id for adding new or updating
        $new = false;
        if ($user->save()) {
            if (empty($id)) {
                $new = true;
                $user->id = self::cached_object($user)->_original['id'];
                $appId = \Lib\Str::generate_app_id($user->id);
                $user->set('app_id', $appId);
                $user->update();
            }
            if (!empty($user->id) && !empty($param['email'])) {
                $options['where'][] = array(
                    'user_id' => $user->id,
                    'email' => $param['email']
                );
                $find = Model_User_Profile::find('first', $options);
                $profile['id'] = !empty($find->id) ? $find->id : 0;
                $profile['user_id'] = $user->id;
                $profile['email'] = $param['email'];    
                $profile['password'] = $param['password'];                    
                if (!Model_User_Profile::add_update($profile, $encodePwd)) {
                    \LogLib::warning('Can not add/update profile', __METHOD__, $profile);
                    return false;
                }
            }
            /*
            if (!empty($user->id) && !empty($param['facebook_id'])) {               
                $options['where'] = array(
                    'facebook_email' => $param['email']
                );
                $find = Model_User_Facebook_Information::find('first', $options);                
                $facebook['id'] = !empty($find->id) ? $find->id : 0;
                $facebook['user_id'] = $user->id;
                $facebook['facebook_id'] = $param['facebook_id'];
                $facebook['facebook_email'] = !empty($param['facebook_email']) ? $param['facebook_email'] : '';
                $facebook['facebook_name'] = !empty($param['facebook_name']) ? $param['facebook_name'] : '';
                $facebook['facebook_username'] = !empty($param['facebook_username']) ? $param['facebook_username'] : '';
                $facebook['facebook_first_name'] = !empty($param['facebook_first_name']) ? $param['facebook_first_name'] : '';
                $facebook['facebook_last_name'] = !empty($param['facebook_last_name']) ? $param['facebook_last_name'] : '';
                $facebook['facebook_link'] = !empty($param['facebook_link']) ? $param['facebook_link'] : '';
                $facebook['facebook_gender'] = !empty($param['facebook_gender']) ? $param['facebook_gender'] : '';
                $facebook['facebook_image'] = "http://graph.facebook.com/{$param['facebook_id']}/picture?type=large";
                if (!Model_User_Facebook_Information::add_update($facebook)) {
                    \LogLib::warning('Can not add/update facebook profile', __METHOD__, $facebook);
                    return false;
                }
            }
            * 
            */
            if (!empty($user->id) && !empty($param['company_id'])) {
                unset($param['id']);
                $param['user_id'] = $user->id;                
                if (!Model_User_Recruiter::add_update($param)) {
                    \LogLib::warning('Can not add/update recruiters', __METHOD__, $param);
                    return false;
                }
            }
            return !empty($user->id) ? $user->id : 0;
        }
        return false;
    }
    
     /**
     * Function to get detail user.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail($param) {
        $query = DB::select(
                        'users.id',
                        'users.app_id',
                        'users.name',
                        'users.nickname',
                        DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username"),
                        'users.university_id',
                        'users.department_id',
                        'users.campus_id',
                        'users.enrollment_year',
                        'users.additional_year',
                        'users.sex_id',
                        'users.created',
                        'users.updated',            
                        'users.is_ios',            
                        'users.is_android',            
                        'users.is_company',
                        DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url"),
                        'user_profiles.user_id', 
                        'user_profiles.email', 
                        'user_profiles.password',
                        'user_recruiters.company_id',
                        array('companies.name', 'company_name'),
                        array('companies.kana', 'company_kana'),
                        array('companies.address', 'company_address'),
                        array('companies.description_short', 'company_description_short'), 
                        array('companies.description', 'company_description'), 
                        DB::expr("IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'" . \Config::get('no_image_company') . "') AS company_thumbnail_img"),
                        DB::expr("IFNULL(IF(companies.thumbnail_img='',NULL,0),1) AS company_has_thumbnail_img"),
                        array('companies.introduction_movie', 'company_introduction_movie'), 
                        array('companies.background_img', 'company_background_img'),
                        array('companies.corporate_url', 'company_corporate_url'), 
                        array('companies.corporate_facebook', 'company_corporate_facebook'),
                        array('companies.corporate_twitter', 'company_corporate_twitter'), 
                        array('companies.corporate_entry', 'company_corporate_entry'),
                        array('companies.ower_user_id', 'company_owner_id'),
                        array('user_recruiters.introduction_text','recruiter_introduction_text'), 
                        array('user_recruiters.is_admin', 'recruiter_admin'),
                        'users.introduction_text', 
                        array('user_recruiters.introduction_text','recruiter_introduction_text'),
                        'user_recruiters.thumbnail_img',                        
                        'user_recruiters.is_approved',
                        'user_facebook_informations.facebook_id',
                        'user_facebook_informations.facebook_email',
                        'user_facebook_informations.facebook_name',
                        'user_facebook_informations.facebook_username',
                        'user_facebook_informations.facebook_first_name',
                        'user_facebook_informations.facebook_last_name',
                        'user_facebook_informations.facebook_gender',
                        'user_facebook_informations.facebook_link', 
                        'user_facebook_informations.facebook_image',
                        array('universities.name', 'university_name'),
                        array('universities.kana', 'university_kana'),
                        array('campuses.name', 'campuses_name'),
                        array('departments.name', 'department_name'),
                        array('campuses.address', 'campuses_address'), 
                        array('campuses.access_method', 'campuses_access_method'),
                        array('campuses.prefecture_id', 'campuses_prefecture_id'),
                        array('campuses.prefecture_id', 'campuses_prefecture_id')
                )
                ->from('users')
                ->join('user_profiles')
                ->on('users.id', '=', 'user_profiles.user_id')
                ->join('user_recruiters', 'LEFT')
                ->on('users.id', '=', 'user_recruiters.user_id')
                ->join('companies', 'LEFT')
                ->on('user_recruiters.company_id', '=', 'companies.id')
                ->join('user_facebook_informations', 'LEFT')
                ->on('users.id', '=', 'user_facebook_informations.user_id')
                ->join('universities', 'LEFT')
                ->on('users.university_id', '=', 'universities.id')
                ->join('campuses', 'LEFT')
                ->on('users.campus_id', '=', 'campuses.id')
                ->join('departments', 'LEFT')
                ->on('users.department_id', '=', 'departments.id')
                ->limit(1)
                ->offset(0);
        if (isset($param['id'])) {
            $query->where('users.id', '=', "{$param['id']}");
        }
        if (isset($param['is_company'])) {
            $query->where('users.is_company', '=', "{$param['is_company']}");
        }
        if (isset($param['disable'])) {
            $query->where('users.disable', '=', "{$param['disable']}");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', '=', "{$param['email']}");
        }
        if (!empty($param['password']) && !empty($param['email'])) {
            $query->where('user_profiles.password', '=', Util::encodePassword($param['password'], $param['email']));
        }
        $data = $query->execute()->as_array();
        if (empty($data)) {
            if (isset($param['id'])) {
                static::errorNotExist('id', $param['id']);
            }
            if (isset($param['email'])) {
                static::errorNotExist('email_or_password', $param['email']);
            }
        }
        $data = !empty($data[0]) ? $data[0] : array();
        return $data;
    }

    /**
     * Function to get all user id.
     *
     * @author <diennvt> 
     * @return array Returns the array.
     */
    public static function get_user_ids() {
        $user = self::query()
                ->select('id')
                ->where('disable', 0)
                ->get();

        return !empty($user) ? $user : array();
    }

    /**
     * Function to processing forget password user.
     *
     * @author tuancd
     * @return array|bool Returns the array or the boolean.
     */
    public static function forget_password($param) {
        $conditions['where'] = array(
            'email' => $param['email']
        );
        $data = \Model_User_Profile::find('first', $conditions);            
        if (!empty($data)) {
            // get token for sending email and add/update
            $param['token'] = \Lib\Str::generate_token();
            if ($param['os'] != 'webos') { // get for mobile
                $param['token'] = \Lib\Str::generate_token_forget_password_for_mobile();
            }
            $option['where'][] = array(
                'email' => $param['email'],
                'disable' => 0
            );
            //Check user is request forget password ??
            $user_activation = \Model_User_Activation::find('first', $option);
            if (!empty($user_activation)) {
                $param['id'] = $user_activation->get('id'); // update old record
                $param['expire_date'] = \Config::get('register_token_expire'); // update new token expire
            } else {
                $param['user_id'] = $data['user_id'];
                $param['disable'] = 0;
                $param['regist_type'] = \Config::get('user_activations_type')['forget_password'];
                $param['expire_date'] = \Config::get('register_token_expire');
            }
            if ($result = Model_User_Activation::add_update($param)) {
                if ($param['os'] != 'webos') {
                    \Lib\Email::sendForgetPasswordEmailForMobile($param);
                } else {
                    \Lib\Email::sendForgetPasswordEmail($param);
                }
            }
            return $result;
        } else {
            // Return email not exists in system
            static::errorNotExist('email', $param['email']);
            return false;
        }
    }

    /**
     * Get user detail for mobile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array|string Returns the array or the string.
     */
    public static function get_detail_for_mobile($param) {
        $profile = self::get_detail(array(
            'id' => $param['id']
        ));
        if (empty($profile)) {
            self::errorNotExist('id', $param['id']);
            return 'false';
        }
        $data['id'] = $profile['user_id'];
        $data['app_id'] = $profile['app_id'];
        $data['user_id'] = $profile['user_id'];
        $data['image_url'] = $profile['image_url'];
        $data['enrollment_year'] = $profile['enrollment_year'];
        $data['name'] = $profile['name'];
        $data['university_id'] = $profile['university_id'];
        $data['university_name'] = $profile['university_name'];
        $data['department_id'] = $profile['department_id'];
        $data['department_name'] = $profile['department_name'];
        $data['sex_id'] = $profile['sex_id'];
        // get any value year
        $thisYear = intval(date('Y'));
        $enrollmentYear = intval($profile['enrollment_year']);
        $additionalYear = intval($profile['additional_year']);
        if ($additionalYear == 99) {
            $data['study_year'] = 99;
        } else {
            // calc study year
            $data['study_year'] = $thisYear - $enrollmentYear - $additionalYear;
            // in Japan, it's April
            if (intval(date('m')) >= \Config::get('first_month_of_school_year')) {
                $data['study_year'] += 1;
            }
        }
        $data['introduction_text'] = $profile['introduction_text'];
        list ($data['news_feed_favorites']['total'], $data['news_feed_favorites']['data']) = \Model_News_Feed::get_news_feed_for_user_mobile($param);

        return $data;
    }

    /**
     * Get user detail for mobile.
     *
     * @author tuancd
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function login($param) {
        // Check user by Email is register but not active
        $options['where'] = array(
            array('disable', "=", 0),
            array('regist_type', "=", \Config::get('user_activations_type')['register_user']),
            array('email', "=", $param['email'])
        );
        $useractivation = \Model_User_Activation::find('first', $options);
        if (!empty($useractivation)) {
            // check expried date 
            if ($useractivation['expire_date'] < time()) {
                self::errorOther(static::ERROR_CODE_OTHER_1, 'email', 'User not active and has been expired register.'); // User register has expried date
                return false;
            } else {
                self::errorOther(static::ERROR_CODE_OTHER_2, 'email', 'User is  not active.'); // User is not active
                return false;
            }
        } else {
            // Check user with email
            $param['is_company'] = 0;
            $param['disable'] = 0;
            
            // check by email
            $data = self::get_detail($param); 
            if (!empty($data)) {
                // write log for login
                if (isset($param['os'])) {
                    $update_user = array();
                    if ($param['os'] == \Config::get('os')['ios']) {
                        $update_user['is_ios'] = 1;
                        $l_device = 1;
                    } elseif ($param['os'] == \Config::get('os')['android']) {
                        $update_user['is_android'] = 1;
                        $l_device = 2;
                    } else {
                        $l_device = 3;
                    }
                    if (empty($data['app_id'])) {
                        $update_user['app_id'] = \Lib\Str::generate_app_id($userId);
                    }
                    if (!empty($update_user)) {
                        $user = self::find($data['id']);
                        if (isset($update_user['app_id'])) {
                            $user->set('app_id', $update_user['app_id']);
                        }
                        if (isset($update_user['is_ios'])) {
                            $user->set('is_ios', $update_user['is_ios']);
                        }
                        if (isset($update_user['is_android'])) {
                            $user->set('is_android', $update_user['is_android']);
                        }
                        $user->save();
                    }
                }
                $param = array(
                    'user_id' => $data['id'],
                    'login_device' => $l_device
                );
                \Model_Login_Log::add($param);
                return $data;
            } else {
                self::errorNotExist('user_information', 'information_of_user'); // User with email and password not match
                return false;
            }
        }
    }

    /**
     * Login facebook 
     *
     * @author diennvt
     * @param array $facebookInfo Input data.
     * @param int $isCompany Input data or not input data.
     * @return bool Returns the boolean.
     */
    public static function login_facebook($facebookInfo, $isCompany = 0)
    {
        if (empty($facebookInfo['email'])) {
            $facebookInfo['email'] = '';
        }
        if (empty($facebookInfo['email']) && empty($facebookInfo['id'])) {
            self::errorNotExist('facebook_id_and_email');
            return false;
        }
        $param['facebook_email'] = isset($facebookInfo['email']) ? $facebookInfo['email'] : '';
        $param['facebook_id'] = isset($facebookInfo['id']) ? $facebookInfo['id'] : '';
        $param['facebook_name'] = isset($facebookInfo['name']) ? $facebookInfo['name'] : '';
        $param['facebook_first_name'] = isset($facebookInfo['first_name']) ? $facebookInfo['first_name'] : '';
        $param['facebook_last_name'] = isset($facebookInfo['last_name']) ? $facebookInfo['last_name'] : '';
        $param['facebook_username'] = isset($facebookInfo['username']) ? $facebookInfo['username'] : '';
        $param['facebook_gender'] = isset($facebookInfo['gender']) ? $facebookInfo['gender'] : '';
        $param['facebook_link'] = isset($facebookInfo['link']) ? $facebookInfo['link'] : '';
        $param['os'] = isset($facebookInfo['os']) ? $facebookInfo['os'] : '';
        if (!empty($param['facebook_email'])) {
            $facebook = \Model_User_Facebook_Information::get_detail(array(
                    'facebook_email' => $param['facebook_email'],                   
                    'disable' => 0
                )
            );
        } elseif (!empty($param['facebook_id'])) {
            $facebook = \Model_User_Facebook_Information::get_detail(array(                    
                    'facebook_id' => $param['facebook_id'],
                    'disable' => 0
                )
            );
        }        
        if (!empty($facebook['facebook_id']) && $facebook['facebook_id'] != $param['facebook_id']) {
            if (\Model_User_Facebook_Information::add_update(array(
                'id' => $facebook['id'],
                'facebook_id' => $param['facebook_id'],
            )))
            {
                $facebook['facebook_id'] = $param['facebook_id'];                
                \LogLib::info('Update facebook_id', __METHOD__, $param);                
            }
        }
        
        $isNewUser = false;//use to differ first new login facebook or not
        if (!empty($facebook['user_id']) && !empty($facebook['facebook_id']))
        {
            \LogLib::info('User used to login with facebook', __METHOD__, $facebook);
            $userId = $facebook['user_id'];
        }
        elseif (!empty($facebook['user_id']) && empty($facebook['facebook_id']))
        {
            \LogLib::info('User used to login without facebook', __METHOD__, $facebook);
            $param['user_id'] = $facebook['user_id'];
            if (\Model_User_Facebook_Information::add_update($param))
            {
                \LogLib::info('Update facebook info', __METHOD__, $param);
                $userId = $facebook['user_id'];
            }
        }
        else
        {
            $isNewUser = true;
            $options['where'] = array(
                'email' => $param['facebook_email']
            );
            $profile = \Model_User_Profile::find('first', $options);
            if (!empty($profile)) {
                $userId = $profile->get('user_id');
                $isNewUser = false;
            } else {
                \LogLib::info('First login using facebook', __METHOD__, $param);
                $param['email'] = $param['facebook_email'];
                $param['password'] = '';
                $param['name'] = $param['facebook_name'];
                $param['nickname'] = $param['name'];
                $param['sex_id'] = $param['facebook_gender'] == 'male' ? 1 : 2;
                $param['is_company'] = $isCompany;
                $param['image_url'] = "http://graph.facebook.com/{$param['facebook_id']}/picture?type=large";
                $userId = Model_User::add_update($param);   
            }
            if ($userId > 0) {  
                // update facebook profile
                $param['id'] = !empty($facebook['id']) ? $facebook['id'] : 0;                
                $param['user_id'] = $userId;    
                if (\Model_User_Facebook_Information::add_update($param))
                {
                    \LogLib::info('Update facebook info', __METHOD__, $param);                
                }
            }       
        }
        if (!empty($userId))
        {
            \LogLib::info('Return user info', __METHOD__, $param);
            $data = self::get_detail(array(
                    'is_company' => $isCompany,
                    'disable' => 0,
                    'id' => $userId
                )
            );
            if (!empty($data))
            {         
                // update user               
                $update_user = array();
                if (empty($data['app_id'])) {
                    $update_user['app_id'] = $data['app_id'] = \Lib\Str::generate_app_id($userId);
                }       
                $param['os'] = \Lib\Util::os();   
                if ($data['is_ios'] != '1' && $param['os'] == \Config::get('os')['ios']) {
                    $update_user['is_ios'] = $data['is_ios'] = 1;
                } elseif ($data['is_android'] != '1' && $param['os'] == \Config::get('os')['android']) {
                    $update_user['is_android'] = $data['is_android'] = 1;                    
                }
                if (!empty($update_user)) {
                    $user = self::find($userId);
                    if (isset($update_user['app_id'])) {
                        $user->set('app_id', $update_user['app_id']); 
                    }
                    if (isset($update_user['is_ios'])) {
                        $user->set('is_ios', $update_user['is_ios']); 
                    }
                    if (isset($update_user['is_android'])) {
                        $user->set('is_android', $update_user['is_android']); 
                    }
                    $user->save();
                }
                
                //Write log login                
                $param = array(
                    'user_id' => $data['id'],
                    'login_device' => \Lib\Util::deviceId($param['os']),
                );
                \Model_Login_Log::add($param);
                $data['is_new_user'] = $isNewUser;
                return $data;
            }
            else
            {
                \LogLib::info('User unavailable', __METHOD__, $param);
                self::errorNotExist('user_information', 'user_information');
                return false;
            }
        }
    }

    /**
     * Login facebook by token.
     *
     * @author diennvt
     * @param array $param Input data.
     * @param int $isCompany Input data or not input data.
     * @return bool Returns the boolean.
     */
    public static function login_facebook_by_token($param, $isCompany = 0)
    {
        @session_start();
        try
        {   
            FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));   
            \LogLib::info('login_facebook_by_token - Get token from cookie', __METHOD__, $param);
            $session = new FacebookSession($param['token']);
            if (isset($session))
            {
                \LogLib::info('login_facebook_by_token - Session is OK', __METHOD__, $param);
                //$request = new FacebookRequest($session, 'GET', '/me');
                $request = new FacebookRequest($session, 'GET', '/me?fields=id,name,email,gender,first_name,last_name,link,locale,timezone');
                $response = $request->execute();
                $facebookInfo = (array) $response->getResponse();
                if (!empty($facebookInfo))
                { 
                    \LogLib::info('login_facebook_by_token - call login_facebook', __METHOD__, $facebookInfo);
                    return self::login_facebook($facebookInfo, $isCompany);
                }
            }
            else
            {
                \LogLib::info('login_facebook_by_token - Session is not OK', __METHOD__, $param);
                return false;
            }            
        } 
        catch (FacebookRequestException $ex)
        {   
            // When Facebook returns an error
            \LogLib::warning($ex->getRawResponse(), __METHOD__, $param);
            static::errorOther(self::ERROR_CODE_OTHER_1, '', $ex->getRawResponse());
            return false;
        } 
        catch (\Exception $ex)
        {
            // When validation fails or other local issues
            \LogLib::warning($ex->getMessage(), __METHOD__, $param);
            static::errorOther(self::ERROR_CODE_OTHER_2, '', $ex->getMessage());
            return false;
        }
        \LogLib::info('login_facebook_by_token - There is no token from cookie', __METHOD__, $param);
        return false;
    }

    /**
     * Get general report from user, favorites, likes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_general_report($param)
    {
        $data = array();
        $data = array_merge($data, self::get_user_report($param));
        $data = array_merge($data, Model_News_Feed_Favorite::get_news_feed_favorite_report($param));
        $data = array_merge($data, Model_News_Feed_Like::get_news_feed_like_report($param));
        $data = array_merge($data, Model_News_Comment_Like::get_news_comment_like_report($param));
        return $data;
    }

    /**
     * Get user report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return aray Returns the array.
     */
    public static function get_user_report($param)
    {
        $query = DB::select(
            DB::expr("
                IFNULL(SUM((CASE WHEN is_company = 1 THEN 1 ELSE 0 END)), 0) AS user_recruiter_count,               
                IFNULL(SUM(IF(is_company = 0, 1, 0)), 0) AS user_count,
                IFNULL(SUM((CASE WHEN is_company = 0 AND users.is_android = 1 THEN 1 ELSE 0 END)), 0) AS user_android_count,
                IFNULL(SUM((CASE WHEN is_company = 0 AND users.is_ios = 1 THEN 1 ELSE 0 END)), 0) AS user_ios_count,
                IFNULL(SUM((CASE WHEN is_company = 0 AND users.sex_id = ". Config::get('is_male') . " THEN 1 ELSE 0 END)), 0) AS user_male_count,
                IFNULL(SUM((CASE WHEN is_company = 0 AND users.sex_id = ". Config::get('is_female') . " THEN 1 ELSE 0 END)), 0) AS user_female_count,
                IFNULL(SUM((CASE WHEN bunri_id = 1 THEN 1 ELSE 0 END)), 0) AS user_department_humanities_count,
                IFNULL(SUM((CASE WHEN bunri_id = 2 THEN 1 ELSE 0 END)), 0) AS user_department_sciences_count
            ")
        )
            ->from(self::$_table_name)
            ->join('departments', 'LEFT')
            ->on(self::$_table_name . '.department_id', '=', 'departments.id')
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $result1 = $query->execute()->offsetGet(0);
        $sql = "
            SELECT  CONCAT('user_study_year_', (YEAR(NOW()) - enrollment_year - IFNULL(additional_year,0) + IF(MONTH(NOW()) >= 4, 1, 0))) AS study_year, count(id) AS user_count				
            FROM    users
            WHERE   enrollment_year IS NOT NULL 
                AND CHAR_LENGTH(enrollment_year) = 4 
                AND additional_year <> 99                
                AND (YEAR(NOW()) - enrollment_year - IFNULL(additional_year,0)  + IF(MONTH(NOW()) > 4, 1, 0)) >= 1 
                AND (YEAR(NOW()) - enrollment_year - IFNULL(additional_year,0)  + IF(MONTH(NOW()) > 4, 1, 0)) <= 4
                AND disable = 0
                AND is_company = 0
            GROUP BY study_year
        ";
        $result2 = DB::query($sql)->execute()->as_array('study_year', 'user_count');
        return array_merge($result1, $result2);             
    }
    
     /**
     * Function to get a list of user and user_notification.
     *
     * @author truongnn 
     * @return array Returns the array.  
     */
    public static function get_user_notification() {
         $query = DB::select(array('users.id','user_id'),'name', 'is_ios', 'is_android','google_regid','apple_regid'
                )
                ->from('users')
                ->join('user_notifications','LEFT')
                ->on("users.id", '=', 'user_notifications.user_id')
                ->where('users.disable',0);
         $data = $query->execute()->as_array();
        return $data ? $data : array();
    }
    
    /**
     * Resend register email.
     *
     * @author tuancd
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function resend_forget_password($param) {
        $option['where'] = array(
            'email' => $param['email'],
            'disable' => '0',
            'regist_type'=>  \Config::get('user_activations_type')['forget_password']            
        );        
        $userActivation = \Model_User_Activation::find('first', $option);
        if (!empty($userActivation)) {
            $token = $userActivation->get('token');
            // update new expire_date
            $userActivation->set('expire_date', \Config::get('register_token_expire'));
            if (!$userActivation->update()) {
                \LogLib::info('Can not update user_activations', __METHOD__, $param);
                return false;
            }
            $param = array(
                'email' => $param['email'],
                'token' => $token,
            );
            if (!\Lib\Email::sendForgetPasswordEmail($param)) {
                \LogLib::warning('Can not resend forgetpassword email', __METHOD__, $param);
                return false;
            }
        } else {
            \LogLib::info('Not exist email in user_activations', __METHOD__, $param);
            self::errorNotExist('email', $param['email']);
            return false;
        }
        return true;
    }

    /**
     * Get number user
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int number user
     */
    public static function get_number($param)
    {
        $query = DB::select(
            DB::expr("
                IFNULL(SUM(IF(is_company = 0, 1, 0)), 0) as number
            ")
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.disable', '=', '0');
        if (!empty($param['from_date'])) {
            $query->where(self::$_table_name . '.created', '>=', $param['from_date']);
        }
        if (!empty($param['to_date'])) {
            $query->where(self::$_table_name . '.created', '<=', $param['to_date']);
        }
        $data = $query->execute()->offsetGet(0);

        return $data['number'];
    }

    /**
     * Register user recruiter by invite
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function register_by_invite($param)
    {
        $options['where'] = array(
            'token' => $param['token'],
            'email' => $param['email'],
            'regist_type' => $param['regist_type']
        );
        // check token again
        $activation = Model_User_Activation::find('first', $options);
        if (!empty($activation)) {
            if ($activation->get('disable') == '1') {
                self::errorOther(self::ERROR_CODE_OTHER_1, 'token', 'Token has been disabled!');
                return false;
            }
            if ($activation->get('expire_date') < time()) {
                self::errorOther(self::ERROR_CODE_OTHER_1, 'token', 'Token has expired date!');
                return false;
            }
            // add new user
            $user = new self;
            $user->set('university_id', '0');
            $user->set('department_id', '0');
            $user->set('campus_id', '0');
            $user->set('enrollment_year', '0');
            $user->set('is_company', '1');
            $user->set('is_ios', 0);
            $user->set('is_android', 0);
            $user->set('additional_year', '0');
            if (!empty($param['os'])) {
                if ($param['os'] == \Config::get('os')['ios']) {
                    $user->set('is_ios', 1);
                } elseif ($param['os'] == \Config::get('os')['android']) {
                    $user->set('is_android', 1);
                }
            }
            if ($user->create()) {
                $user->id = self::cached_object($user)->_original['id'];
                $appId = \Lib\Str::generate_app_id($user->id);
                $user->set('app_id', $appId);
                if ($user->update()) {
                    // add new user profile
                    $profile = new Model_User_Profile;
                    $profile->set('user_id', $user->id);
                    $profile->set('email', $param['email']);
                    $profile->set('password', Util::encodePassword($param['password'], $param['email']));
                    if ($profile->create()) {
                        // add new user recruiter
                        $recruiter = new Model_User_Recruiter;
                        $recruiter->set('company_id', $param['company_id']);
                        $recruiter->set('user_id', $user->id);
                        if ($param['regist_type'] == \Config::get('user_activations_type')['recruiter_admin']) {
                            $recruiter->set('is_admin', '1');
                        } else {
                            $recruiter->set('is_admin', '0');
                        }
                        $recruiter->set('is_approved', '1');
                        $recruiter->set('priority', '3');
                        if ($recruiter->create()) {
                            // disable user activation
                            $activation->set('disable', '1');
                            if ($activation->save()) {
                                $param = array(
                                    'id' => $user->id,
                                    'full' => 1,
                                );
                                return Model_User::get_detail($param);
                            }
                        }
                    }
                }
            }
            return false;
        } else {
            self::errorOther(self::ERROR_CODE_OTHER_1, 'token', 'Token is invalid!');
            return false;
        }
    }
}
