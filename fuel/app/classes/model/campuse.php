<?php
/**
 * Model_Campuse - Model to operate to Campuse's functions.
 * 
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Campuse extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'university_id',
        'name',
        'address',
        'access_method',
        'prefecture_id',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'campuses';
    
    /**
     * Get information of campuse.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail($param) {
        $query = DB::select(
                self::$_table_name . '.*', array('universities.name', 'university_name')
            )
            ->from(self::$_table_name)
            ->join('universities', 'LEFT')
            ->on('universities.id', '=', self::$_table_name . '.university_id')
            ->limit(1)
            ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', '=', "{$param['id']}");
        }

        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $id);
        }
        $data = !empty($data[0]) ? $data[0] : array();
        return $data;
    }

}
