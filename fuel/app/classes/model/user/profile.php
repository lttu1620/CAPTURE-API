<?php

use Fuel\Core\DB;
use Lib\Util;

/**
 * Any query in Model User Profile.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Profile extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'user_id',
		'email',
		'password',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'user_profiles';

    /**
     * Get list user profile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        if (!empty($param['id'])) {
            $query->where('id', '=', $param['id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['email'])) {
            $query->where('email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Add or update info for user profile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Return the integer or the boolean.
     */
    public static function add_update($param, $encodePwd = true)
    {
        if (!empty($param['id'])) { 
            $userProfile = self::find($param['id']);
            if (empty($userProfile)) {
                static::errorNotExist('id', $param['id']);
                return false;
            }
        }else{
            if (!empty($param['email'])) { 
                $options['where'][] = array(
                    'email' => $param['email'],
                );
                $userProfile = self::find('first', $options);
                if(!empty($userProfile)){
                    static::errorDuplicate('email', $param['email']);
                    return false;
                }
            } 
        }
        if (!empty($param['user_id'])) { 
            $options['where'][] = array(
                'user_id' => $param['user_id'],
            );
            $userProfile = self::find('first', $options);
        } 
        if (empty($userProfile)) {
            $userProfile = new self;
        }
        if (!empty($param['user_id'])) {
            $userProfile->set('user_id', $param['user_id']);
        }
        if (!empty($param['email'])) {
            $userProfile->set('email', $param['email']);
        }              
        if ($encodePwd) { 
            $userProfile->set('password', Util::encodePassword($param['password'], $param['email']));
        } else {
            $userProfile->set('password', $param['password']);
        }
        if ($userProfile->save()) {
            if (empty($userProfile->id)) {
                $userProfile->id = self::cached_object($userProfile)->_original['id'];
            }
            return !empty($userProfile->id) ? $userProfile->id : 0;
        }
        return false;
    }

    /**
     * Update password by token.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_password($param)
    {
        $query = DB::select(
            array('user_activations.email', 'activation_email'),
            array('user_activations.token', 'token'),
            array('user_activations.regist_type', 'regist_type'),
            array('user_activations.disable', 'activation_disable'),
            array('user_activations.id', 'activation_id'),
            self::$_table_name . '.*'
        )
            ->from('user_activations')
            ->join(self::$_table_name, 'LEFT')
            ->on('user_activations.email', '=', self::$_table_name . '.email')
            ->where('user_activations.token', '=', $param['token'])
            ->where('user_activations.regist_type', '=', $param['regist_type'])
            ->where('user_activations.disable', '=', '0');
        $data = $query->execute()->as_array(); 
        if ($data) {
            if (isset($data[0]['id']) && $data[0]['id']) {
                $user = self::find($data[0]['id']);
                if ($user) {
                    $user->set('password', Util::encodePassword($param['password'], $user->get('email')));
                    if ($user->update()) {
                        if (!\Model_User_Activation::disable(array(
                            'id' => $data[0]['activation_id'],
                            'disable' => '1'
                        ))) {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Change password by user_id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function change_password($param)
    {
        
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('user_id', '=', $param['user_id']);
        $data = $query->execute()->as_array();
        if ($data) {
            if (isset($data[0]['id']) && $data[0]['id']) {
                $user = self::find($data[0]['id']);
                if ($user) {
                    $user->set('password', Util::encodePassword($param['password'], $user['email'])); 
                    if ($user->update()) {
                        return true;
                    }
                }
            }
        }
        self::errorNotExist('user_id');
        return false;
       
    }
    
    /**
     * Get_company_admin_email.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Return the boolean.
     */
    public static function get_company_admin_email($param)
    {
        try {
            $sql = "select email from user_profiles where user_id in
		    (
		    select user_id from user_recruiters 
		    where user_recruiters.company_id = 
		    (select user_recruiters.company_id 
		     from user_recruiters 
		     where user_recruiters.user_id = {$param['user_id']} limit 1)
		    and user_recruiters.is_admin = 1 
		    and user_recruiters.disable = 0 
		    and user_recruiters.is_approved = 1
		    )";
	    
	    $query = DB::query($sql);
	    $data = $query->execute()->as_array();
	    return !empty($data) ? $data : array();
	    
        } catch (\Exception $e) {
            return false;
        }
    }
}
