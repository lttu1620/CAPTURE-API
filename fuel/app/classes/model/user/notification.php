<?php

use Fuel\Core\DB;
use Fuel\Core\Config;

/**
 * Any query in Model User Notification.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Notification extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'user_id',
        'google_regid',
        'apple_regid',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'user_notifications';

    /**
     * Register user notification.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function register($param)
    {        
        if (!$regIdField = self::_get_update_field_by_os($param)) 
        {
            self::errorParamInvalid('regid'); 
            return false;
        }
        if (!empty($param['id'])) {
            $userNotification = self::find($param['id']);
            if (empty($userNotification)) {
                static::errorNotExist('id', $param['id']);
                return false;
            }
        }
        if (!empty($param['user_id'])) {
            $options['where'][] = array(
                'user_id' => $param['user_id'],                
            );
            $userNotification = self::find('first', $options);
            if (!empty($userNotification) && $userNotification->get($regIdField) == $param['regid']) {
                // regId have already registered
                return true;
            }
        }
        if (empty($userNotification)) {
            $userNotification = new self;
        }
        if (!empty($param['user_id'])) {
            $userNotification->set('user_id', $param['user_id']);
            $userNotification->set($regIdField, $param['regid']);
        }
        if ($userNotification->save()) {
            if (empty($userNotification->id)) {
                $userNotification->id = self::cached_object($userNotification)->_original['id'];
            }
            return !empty($userNotification->id) ? $userNotification->id : 0;
        }
        return false;      
    }

    /**
     * Get list user notification.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        if (!empty($param['id'])) {
            $query->where('id', '=', $param['id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('user_id', '=', $param['user_id']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get list user notification.
     *
     * @author tuancd
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_list_user_push_message($param)
    {
        $query = DB::select(self::$_table_name. ".*")
                ->from(self::$_table_name)
                ->where(DB::expr('NOT EXISTS (select push_message_send_logs.user_id from push_message_send_logs where push_message_send_logs.user_id = ' . self::$_table_name . '.user_id AND push_message_send_logs.message_id=' . $param['message_id'] . ' ) '));
        if (!empty($param['limit'])) {
            //$offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit']);
        }       
        $data = $query->execute()->as_array();
        return  $data;
    }

    /**
     * Unregister user notification by user id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the array.
     */
    public static function unregister($param)
    {       
        $options['where'][] = array(
            'user_id' => $param['user_id']
        );
        $userNotification = self::find('first', $options);
        if ($userNotification) 
        {
            if ($regIdField = self::_get_update_field_by_os($param)) 
            {
                $userNotification->set($regIdField, '');
                if ($userNotification->update()) 
                {
                    return true;
                }
            } 
            else 
            {
                self::errorParamInvalid();
            }            
        } 
        else 
        {
            self::errorNotExist('user_id');           
        }
        return false;
    }
    
    /**
     * Get field for update by os & device.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.    
     * @return string|bool Returns the string or the boolean.   
     */
    private static function _get_update_field_by_os($param = array())
    {
        if ($param['os'] == Config::get('os')['ios']) 
        {
            return 'apple_regid';
        } 
        elseif ($param['os'] == Config::get('os')['android']) 
        {
            return 'google_regid';            
        }
        elseif ($param['os'] == Config::get('os')['webos'] && !empty($param['device']) && $param['device'] == 'apple') 
        {
            return 'apple_regid';            
        }
        elseif ($param['os'] == Config::get('os')['webos'] && !empty($param['device']) && $param['device'] == 'google') 
        {
            return 'google_regid';            
        }
        return false;
    }
    
}
