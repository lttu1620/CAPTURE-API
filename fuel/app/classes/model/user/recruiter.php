<?php

/**
 * Model_User_Recruiter - Model to operate to User_Recruiter's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_User_Recruiter extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'company_id',
        'user_id',
        'introduction_text',
        'thumbnail_img',
        'is_admin',
        'is_approved',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'user_recruiters';

    /**
     * Function to get a list of recruiter.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data). 
     */
    public static function get_list($param) {
        $page = !empty($param['page']) ? $param['page'] : 1;
        $limit = !empty($param['limit']) ? $param['limit'] : 1;
        $offset = ($page - 1) * $limit;

        $query = DB::select(
                    self::$_table_name . ".*", 
                    array('companies.name', 'company_name'), 
                    DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username"),
                    DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS user_image_url"),                    
                    array('users.name', 'user_name'), 
                    array('users.nickname', 'user_nickname'), 
                    array('users.sex_id', 'user_sex_id'), 
                    array('user_profiles.email', 'user_email')
                )
                ->from(self::$_table_name)
                ->join('users')
                ->on(self::$_table_name . ".user_id", '=', 'users.id')
                ->join('user_profiles')
                ->on(self::$_table_name . ".user_id", '=', 'user_profiles.user_id')
                ->join('companies')
                ->on(self::$_table_name . ".company_id", '=', 'companies.id')
                ->limit($limit)
                ->offset($offset);
        
        if (!empty($param['user_id'])) {            
            $query->where(DB::expr(self::$_table_name . ".company_id IN (SELECT company_id FROM user_recruiters WHERE user_id={$param['user_id']})"));           
        }
        if (isset($param['is_admin']) && $param['is_admin'] != '') {
            $query->where(self::$_table_name . ".is_admin", $param['is_admin']);
        }
        if (isset($param['is_approved']) && $param['is_approved'] != '') {
            $query->where(self::$_table_name . ".is_approved", $param['is_approved']);
        }
        if (!empty($param['company_id'])) {        
            $query->where(self::$_table_name . ".company_id", $param['company_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where(DB::expr("(users.nickname LIKE  '%{$param['user_name']}%' OR users.name LIKE '%{$param['user_name']}%')"));
        }
        if (!empty($param['sex'])) {
            $query->where("users.sex_id", '=', $param['sex']);
        }
        if (!empty($param['email'])) {
            $query->where("user_profiles.email", 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }

        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . ".disable", $param['disable']);
        }

        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to disable or enable a recruiter.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $recruiter = self::find($id);
            if (empty($recruiter)) {
                return false;
            }
            $recruiter->set('disable', $param['disable']);
            if (!$recruiter->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to add or update a recruiter.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param) {//check id if existing
        // If the fisrt user of company_id= > set is_admin =1
       
        if (!empty($param['company_id']) && !empty($param['user_id'])) {
            $options['where'][] = array(
                'user_id' => $param['user_id'],
                'company_id' => $param['company_id'],
                'is_admin' => 1
            );
            $checkadmin = self::find('first', $options);
            if (empty($checkadmin)) {
                $param['is_admin'] = 1;
            } else {
                $param['is_admin'] = 0;
            }
        } elseif (!isset($param['is_admin'])) {
            $param['is_admin'] = 0;
        }
        if (!isset($param['is_approved'])) {
            $param['is_approved'] = 0;
        }
        if (!empty($param['id'])) {
            $options = array();
            $options['where'][] = array(
                'id' => $param['id'],
            );
            $recruiter = self::find('first', $options);
            if (empty($recruiter)) {
                static::errorNotExist('id', $param['id']);
                return false;
            }
        }
        if (!empty($param['user_id'])) {
            $options = array();
            $options['where'][] = array(
                'user_id' => $param['user_id'],
            );
            $recruiter = self::find('first', $options);
        }
        if (empty($recruiter)) {
            $recruiter = new self;
        }
        // set information for recruiter if having
        if (!empty($param['company_id'])) {
            $recruiter->set('company_id', $param['company_id']);
        }
        if (!empty($param['user_id'])) {
            $recruiter->set('user_id', $param['user_id']);
        }
        if (isset($param['is_admin'])) {
            $recruiter->set('is_admin', $param['is_admin']);
        }
        if (isset($param['is_approved'])) {
            $recruiter->set('is_approved', $param['is_approved']);
        }
        if (!empty($param['introduction_text'])) {
            $recruiter->set('introduction_text', $param['introduction_text']);
        }
        if (isset($param['thumbnail_img'])) {
            $recruiter->set('thumbnail_img', $param['thumbnail_img']);
        }
        //check id for adding new or updating
        if ($recruiter->save()) {
            if (empty($recruiter->id)) {
                $recruiter->id = self::cached_object($recruiter)->_original['id'];
            }
            return !empty($recruiter->id) ? $recruiter->id : 0;
        }
        return false;
    }

    /**
     * Get company's recruiters list for mobile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_recruiter_for_mobile($param) {
        $query = DB::select(
            self::$_table_name . '.user_id', 
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url"),
            array('users.name', 'name'), 
            array('users.nickname', 'nickname'), 
            array('user_profiles.email', 'email'), 
            self::$_table_name . '.position', 
            self::$_table_name . '.priority', 
            self::$_table_name . '.created'
                )
                ->from(self::$_table_name)
                ->join('users', 'LEFT')
                ->on(self::$_table_name . '.user_id', '=', 'users.id')
                ->join('companies', 'LEFT')
                ->on(self::$_table_name . '.company_id', '=', 'companies.id')
                ->join('user_profiles', 'LEFT')
                ->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id')
                ->where(self::$_table_name . '.company_id', '=', $param['company_id'])
                ->where('users.disable', '=', '0')
                ->where('companies.disable', '=', '0')
                ->where(self::$_table_name . '.disable', '=', '0');
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Dnable/disable property isAdmin.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disableIsAdmin($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $recruiter = self::find($id);
            if (empty($recruiter)) {
                return false;
            }
            $recruiter->set('is_admin', $param['is_admin']);
            if (!$recruiter->update()) {
                return false;
            }
        }
        return true;
    }
    /**
     * Approve user.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function appproveUser($param) {
        if (empty($param['user_id']))
        {
            \LogLib::warning('Parameter is invalid', __METHOD__, $param);
            return false;
        }
        $options['where'] = array(
            'user_id' => $param['user_id'],                
        );
        $recruiter = self::find('first', $options);
        if (empty($recruiter)) {
            static::errorNotExist('user_id', $param['user_id']);
            return false;
        }
        $recruiter->set('is_approved', '1');
        if (!$recruiter->update()) {
            \LogLib::warning('Can not approve user', __METHOD__, $param);
            return false;
        }
        return true;
    }
    
    /**
     * Enable/disable property isApproved.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disableIsApproved($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $recruiter = self::find($id);
            if (empty($recruiter)) {
                return false;
            }
            $recruiter->set('is_approved', $param['is_approved']);
            if (!$recruiter->update()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get user detail for mobile.
     *
     * @author tuancd
     * @param array $param Input data.
     * @return array|bool Return the array or the boolean.
     */
    public static function login($param) {
        // Check user by Email is register but not active
        $options['where'] = array(
            array('disable', "=", 0),
            array('regist_type', "=", \Config::get('user_activations_type')['register_recruiter']),
            array('email', "=", $param['email'])
        );
        $useractivation = \Model_User_Activation::find('first', $options);
        if (!empty($useractivation)) {
            // check expried date 
            if ($useractivation['expire_date'] < time()) {
                self::errorOther(static::ERROR_CODE_OTHER_1, 'email', 'Recruiter not active and has been expired register.'); // User register has expried date
                return false;
            } else {
                self::errorOther(static::ERROR_CODE_OTHER_2, 'email', 'Recruiter is  not active.'); // User is not active
                return false;
            }
        } else {
            // Check user with email

            $param['is_company'] = 1;
            $param['disable'] = 0;
            $data = \Model_User::get_detail($param);
            if (!empty($data)) {
                //Write log login
                if (isset($param['os'])) {
                    if ($param['os'] == \Config::get('os')['ios']) {
                        $l_device = 1;
                    } elseif ($param['os'] == \Config::get('os')['android']) {
                        $l_device = 2;
                    } else {
                        $l_device = 3;
                    }
                }
                $param = array(
                    'user_id' => $data['id'],
                    'login_device' => $l_device
                );
                \Model_Login_Log::add($param);
                return $data;
            } else {
                self::errorNotExist('Recruiter_information', 'information_of_recruiter');
                return false;
            }
        }
    }

    /**
     * Get company user list for tada copy (API 4).
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function get_company_user_list_for_tada_copy($param)
    {
        $query = DB::select(
            array(self::$_table_name . '.id', 'id'),
            array(self::$_table_name . '.company_id', 'company_id'),
            array('users.name', 'name'),
            array(self::$_table_name . '.introduction_text', 'introduce'),
            array(self::$_table_name . '.thumbnail_img', 'image')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->where(self::$_table_name . '.id', '=', $param['recruiter_id'])
            ->where(self::$_table_name . '.disable', '=', '0');

        $data = $query->execute()->as_array();
        if (!empty($data[0])) {
            $query = DB::select(
                array('news_comments.news_feed_id', 'id'),
                array('news_feeds.detail_url', 'image'),
                array('news_comments.comment_short', 'text'),
                array('news_comments.like_count', 'nice'),
                DB::expr("
                    FROM_UNIXTIME(news_feeds.created) AS created
                ")
            )
                ->from('news_comments')
                ->join('news_feeds', 'LEFT')
                ->on('news_comments.news_feed_id', '=', 'news_feeds.id')
                ->join('news_sites', 'LEFT')
                ->on('news_feeds.news_site_id', '=', 'news_sites.id')
                ->join(self::$_table_name)
                ->on(self::$_table_name . '.user_id', '=', 'news_comments.user_id')
                ->where(self::$_table_name . '.id', '=', $param['recruiter_id'])
                ->where('news_comments.disable', '=', '0')
                ->where('news_feeds.is_tadacopy', '=', '1')
                ->where(DB::expr(
                    "(IFNULL(news_feeds.news_site_id,0) = 0 OR (news_feeds.news_site_id > 0 AND news_sites.disable = 0 AND news_sites.is_tadacopy = 1))"
                ));
            $data[0]['comments'] = $query->execute()->as_array();
            // set image url
            $data[0]['comments'] = \Model_Setting::set_image_url($data[0]['comments'], array('image'));
        }

        $param['called_api_name'] = \Config::get('tadacopy_company_user_list');
        if (!\Model_External_Relation_Log::insert($param)) {
            return false;
        }

        // set image url
        $data = \Model_Setting::set_image_url($data, array('image'));

        return !empty($data[0]) ? $data[0] : array();
    }
}
