<?php

use Orm\Model;
use Lib\Cache;
/**
 * Model_Abstract - Model to create common functions or constants.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Model_Abstract extends Model {

    public $disable = 0;     
    public static $error_code_validation = array();
    const ERROR_CODE_INVALED_PARAMETER = 400;
    const ERROR_CODE_AUTH_ERROR = 403;
    const ERROR_CODE_FIELD_NOT_EXIST = 1010;
    const ERROR_CODE_FIELD_DUPLICATE = 1011;
    const ERROR_CODE_OTHER_1 = 1021;
    const ERROR_CODE_OTHER_2 = 1022;
    const ERROR_CODE_OTHER_3 = 1023;
    const ERROR_CODE_OTHER_4 = 1024;
    const ERROR_CODE_OTHER_5 = 1025;
    
     /**
     * <init - function to inital properties>   
     *
     * @author thailh 
     */
    public static function _init() 
    {
        if (\Lib\Util::os() != \Config::get('os')['webos'] && !empty(static::$_mobile_properties)) 
        {
            static::$_properties = static::$_mobile_properties;
        }
    }
    
    /**
     * Function to set value for error_code cause INVALED_PARAMETER.
     * @param string $name Field of data (or not use this argument).
     * @param string $value The value of field.
     * @author thailh 
     */
    public static function errorParamInvalid($field = '', $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_INVALED_PARAMETER,
            'field' => $field,
            'value' => $value,
        );
    }
    
     /**
     * Function to set value for error_code cause FIELD_NOT_EXIST.
     * @param string $name Field of data.
     * @param string $value The value of field (or not use this argument).
     * @author thailh 
     */
    public static function errorNotExist($field, $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_FIELD_NOT_EXIST,
            'field' => $field,
            'value' => $value,
        );
    }
    /**
     * Function to set value for error_code cause FIELD_DUPLICATE.
     * @param string $name Field of data.
     * @param string $value The value of field (or not use this argument).
     * @author thailh 
     */
    public static function errorDuplicate($field, $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_FIELD_DUPLICATE,
            'field' => $field,
            'value' => $value,
        );
    }
     /**
     * Function to set value for error_code cause others.
     * @param string $code Input code.
     * @param string $name Field of data (or not use this argument).
     * @param string $value The value of field (or not use this argument).
     * @author thailh 
     */
    public static function errorOther($code, $field = null, $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => $code,
            'field' => $field,
            'value' => $value,
        );
    }
     /**
     * Function to set value for error_code_validation.
     *
     * @author thailh 
     * @return array Returns the array.
     */
    public static function error() {
        return static::$error_code_validation;
    } 
     /**
     * Function to format date.
     * @param int $date Input date.
     * @author thailh 
     * @return int Returns integer.
     */
    public static function date_from_val($date) {
        return strtotime($date);
    }
     /**
     * Function to format date time.
     * @param int $date Input date.
     * @author thailh 
     * @return int Returns the integer.
     */
    public static function date_to_val($date) {
        return strtotime($date . '23:59:00');
    }
}
