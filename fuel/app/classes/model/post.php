<?php

/**
 * Any query in Model Post
 *
 * @package Model
 * @created 2015-08-21
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Post extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'post_content',
        'post_description',
        'created',
        'updated',
        'disable',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'posts';

    /**
     * Add info for Post
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Post id or false if error
     */
    public static function add($param)
    {
        $self = new self;
        if (isset($param['post_content'])) {
            $self->set('post_content', $param['post_content']);
        }
        if (isset($param['post_description'])) {
            $self->set('post_description', $param['post_description']);
        }
        // save to database
        if ($self->create()) {
            $self->id = self::cached_object($self)->_original['id'];
            if (!empty($self->id)) {
                $param = array(
                    'news_site_id'      => \Config::get('news_site_oceanize_id', 99),
                    'title'             => $param['post_title'],
                    'image_url'         => !empty($param['image_url']) ? $param['image_url'] : '',
                    'detail_url'        => \Config::get('adm_url').'posts/detail/'.$self->id,
                    'likes_count'       => 0,
                    'comments_count'    => 0,
                    'post_id'           => $self->id,
                    'distribution_date' => time(),
                );
                if (!Model_News_Feed::add_update($param)) {
                    return false;
                }
            }
            return !empty($self->id) ? $self->id : 0;
        }
        return false;
    }

    /**
     * Add or update info for Post
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Post id or false if error
     */
    public static function set_update($param)
    {
        $self = self::find($param['id']);
        if (empty($self)) {
            self::errorNotExist('post_id');
            return false;
        }
        $option['where'] = array(
            'id'      => $param['news_feed_id'],
            'post_id' => $param['id'],
            'disable' => '0',
        );
        $newsFeed = Model_News_Feed::find('first', $option);
        if (!$newsFeed) {
            self::errorNotExist('news_feed_id');
            return false;
        }
        if (isset($param['post_content'])) {
            $self->set('post_content', $param['post_content']);
        }
        if (isset($param['post_description'])) {
            $self->set('post_description', $param['post_description']);
        }
        if ($self->save()) {
            if (isset($param['post_title'])) {
                $newsFeed->set('title', $param['post_title']);
            }
            if (isset($param['is_public']) && $param['is_public'] != '') {
                $newsFeed->is_public = $param['is_public'];
            }
            if (!empty($param['image_url']) && is_string($param['image_url'])) {
                $newsFeed->set('image_url', $param['image_url']);
            }
            if (!$newsFeed->save()) {
                return false;
            }
            return $self->id;
        }
        return false;
    }

    /**
     * Get list Post (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List Post
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name.'.id',
            self::$_table_name.'.post_description',
            self::$_table_name.'.created',
            self::$_table_name.'.updated',
            self::$_table_name.'.disable',
            array('news_feeds.title', 'post_title'),
            array('news_feeds.id', 'news_feed_id'),
            DB::expr(
                "IFNULL(IF(news_feeds.image_url='',NULL,news_feeds.image_url),'".\Config::get(
                    'no_image_other'
                )."') AS image_url"
            ),
            'news_feeds.is_public',
            'news_feeds.detail_url',
            'news_feeds.short_url',
            'news_feeds.comments_count',
            'news_feeds.likes_count'
        )
            ->from('news_feeds')
            ->join(self::$_table_name)
            ->on('news_feeds.post_id', '=', self::$_table_name.'.id')
            ->where('news_feeds.disable', '=', '0');
        // filter by keyword
        if (!empty($param['post_title'])) {
            $query->where('news_feeds.title', 'LIKE', "%{$param['post_title']}%");
        }
        if (!empty($param['post_description'])) {
            $query->where(self::$_table_name.'.post_description', 'LIKE', "%{$param['post_description']}%");
        }
        if (isset($param['is_public']) && $param['is_public'] != '') {
            $query->where('news_feeds.is_public', '=', $param['is_public']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name.'.'.$sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name.'.id', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        foreach ($data as &$row) {
            if (\Lib\Str::is_japanese($row['post_description'])) {
                 $row['post_description'] = mb_substr($row['post_description'], 0, 10, "UTF-8");
             } else {
                 $row['post_description'] = \Lib\Str::truncate($row['post_description'], 50);
             }
        }
        unset($row);
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Disable/Enable list Post
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $self = self::find($id);
            if ($self) {
                $self->set('disable', $param['disable']);
                if (!$self->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('post_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Post
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail Post or false if error
     */
    public static function get_detail($param)
    {
        $query = DB::select(
            self::$_table_name.'.*',
            array('news_feeds.title', 'post_title'),
            array('news_feeds.id', 'news_feed_id'),
            'news_feeds.image_url',
            'news_feeds.is_public',
            'news_feeds.detail_url'
        )
            ->from('news_feeds')
            ->join(self::$_table_name)
            ->on('news_feeds.post_id', '=', self::$_table_name.'.id')
            ->where('news_feeds.disable', '=', '0')
            ->where(self::$_table_name.'.id', '=', $param['id']);
        if (!isset($param['from_admin'])) {
            $query->where('news_feeds.is_public', '=', '1');
        }
        $data = $query->execute()->offsetGet(0);
        // get data
        if (empty($data)) {
            static::errorNotExist('post_id');
            return false;
        }
        return $data;
    }

    /**
     * Public/Private a post
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function is_public($param)
    {
        $options = array(
            'relate' => array(
                'posts' => array(
                    'join_type' => 'inner',
                    'where'     => array(
                        array('news_feeds.post_id', '=', 'posts.id'),
                    ),
                ),
            ),
            'where'  => array(
                'post_id' => $param['id'],
            ),
        );
        $self = Model_News_Feed::find('first', $options);
        if ($self) {
            $self->set('is_public', $param['is_public']);
            if ($self->update()) {
                return true;
            }
        }
        static::errorNotExist('post_id');
        return false;
    }

}
