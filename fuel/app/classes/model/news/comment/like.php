<?php

/**
 * Any query in Model News Comment Like.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Comment_Like extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'news_site_id',
        'news_feed_id',
        'news_comment_id',
        'user_id',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'news_comment_likes';

    /**
     * Get list news comment like by news feed id, news comment id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('users.nickname', 'user_nickname'),
            DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username"),
            array('user_profiles.email', 'email'),
            DB::expr("IF(users.id > 0, IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "'),'" . \Config::get('no_image_guest') . "') AS image_url"),
            DB::expr("count('users.name') as like_count")
        )
            ->from(self::$_table_name)
            ->join('user_guest_ids')
            ->on(self::$_table_name . '.user_id', '=', 'user_guest_ids.id')
            ->join('users', 'LEFT')
            ->on('user_guest_ids.id', '=', 'users.id')
            ->join('user_profiles', 'LEFT')
            ->on('users.id', '=', 'user_profiles.user_id')
            ->join('news_comments')
            ->on(self::$_table_name . '.news_comment_id', '=', 'news_comments.id');
        if (!empty($param['news_feed_id'])) {
            $query->where('news_feed_id', '=', $param['news_feed_id']);
        }
        if (!empty($param['news_comment_id'])) {
            $query->where('news_comment_id', '=', $param['news_comment_id']);
        }
        if (!empty($param['news_site_id'])) {
            $query->where('news_site_id', '=', $param['news_site_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where(DB::expr("(users.nickname LIKE  '%{$param['user_name']}%' OR users.name LIKE '%{$param['user_name']}%')"));
        }
        if (isset($param['email']) && $param['email'] != '') {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (isset($param['date_from']) && $param['date_from'] != '') {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (isset($param['date_to']) && $param['date_to'] != '') {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $query->group_by('users.name');
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data  = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any news comment likes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $query = DB::select('id')
            ->from(self::$_table_name)
            ->where('news_comment_id', '=', $param['news_comment_id'])
            ->where('user_id', '=', $param['user_id'])
            ->where('disable', '=', '0');
        $data  = $query->execute()->as_array();
        if ($data) {
            $likes = self::find($data[0]['id']);
            $likes->set('disable', '1');
            if ($likes->update()) {
                //have trigger to update like_count for this comment.
                return true;
            }
        } else {
            static::errorNotExist('news_comment_id', $param['news_comment_id']);
            return false;
        }
    }

    /**
     * Add info for news comment likes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function add($param)
    {
        $query = DB::select(
            'news_comments.news_feed_id',
            'news_comments.news_site_id',
            array('news_comments.id', 'news_comment_id'),
            array('news_comments.like_count', 'like_count'),
            array('news_comment_likes.id', 'likes_id'),
            array('news_comment_likes.user_id', 'user_id')
        )
            ->from('news_comments')
            ->join(DB::expr("(SELECT * FROM news_comment_likes WHERE user_id={$param['user_id']} AND disable=0) AS news_comment_likes"),
                'LEFT')
            ->on('news_comments.id', '=', 'news_comment_likes.news_comment_id')
            ->where('news_comments.id', '=', $param['news_comment_id'])
            ->where('news_comments.disable', '=', '0');
        $data  = $query->execute()->as_array();
        if (empty($data[0]['news_comment_id']) || empty($data[0]['news_feed_id'])) {
            static::errorNotExist('news_comment_id', $param['news_comment_id']);
            return false;
        }
        if (count($data) >= Config::get('news_comment_count_like')) {
            static::errorDuplicate('user_id', $param['user_id']);
            return false;
        }
        $likes = new self;
        $likes->set('news_comment_id', $param['news_comment_id']);
        $likes->set('user_id', $param['user_id']);
        $likes->set('news_feed_id', $data[0]['news_feed_id']);
        $likes->set('news_site_id', $data[0]['news_site_id']);
        if ($likes->create()) {
            $likes->id = self::cached_object($likes)->_original['id'];
            if ($likes->id) {
                //have trigger to update like_count for this comment.
                return array(
                    'id'           => $likes->id,
                    'clap_count'   => (\Lib\Arr::count($data, 'user_id', $param['user_id']) + 1),
                    'like_count'   => $data[0]['like_count'] + 1,
                    'news_feed_id' => $data[0]['news_feed_id']
                );
            }
        }
        return false;
    }

    /**
     * Count news comment likes by news comment id.
     *
     * @author Le Tuan Tu
     * @param int $newsCommentId Input data newsCommentId or not input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function count_by_news_comment_id($newsCommentId = null)
    {
        if (empty($newsCommentId)) {
            return false;
        }
        $options['where'][] = array(
            'news_comment_id' => $newsCommentId,
            'disable'         => 0
        );
        $count              = self::count($options);
        return $count;
    }

    /**
     * Get news comment likes report by date.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_like_report_by_date($param)
    {
        $query = DB::select(
            DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),
            DB::expr("COUNT(*) AS like_count")
        )
            ->from(self::$_table_name);
        if (!empty($param['news_comment_id'])) {
            $query->where(self::$_table_name . '.news_comment_id', '=', $param['news_comment_id']);
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        $query->group_by('date');
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get news comment likes report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_news_comment_like_report($param)
    {
        $query = DB::select(
            DB::expr("COUNT(*) AS news_comment_like_count")
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $data = $query->execute()->as_array();

        return $data ? $data[0] : array();
    }

    /**
     * Get news comment like for tada copy.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function get_news_comment_like_for_tada_copy($param)
    {
        $like = self::add(array(
            'news_comment_id' => $param['news_comment_id'],
            'user_id'         => $param['user_id']
        ));
        if ($like) {
            return array(
                'news_feed_id' => $like['news_feed_id'],
                'news_comment_id' => $param['news_comment_id'],
                'news_comment_like_count' => $like['clap_count'],
                'news_comment_like_total' => $like['like_count']
            );
        }
        return false;
    }
}
