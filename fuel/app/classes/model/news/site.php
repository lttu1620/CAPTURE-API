<?php

/**
 * Any query in Model News Site.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Site extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'name',
		'introduction_text',
		'thumbnail_img',
		'feed_url',
        'rss_count',
        'feed_count',
        'is_tadacopy',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_sites';

    /**
     * Get list news sites by name.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
                    static::$_table_name . ".*",
                    DB::expr("IFNULL(IF(news_sites.thumbnail_img='',NULL,news_sites.thumbnail_img),'" . \Config::get('no_image_other') . "') AS newssites_thumbnail_img")
                )
                ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['feed_url'])) {
            $query->where('feed_url', 'LIKE', "%{$param['feed_url']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('disable', '=', $param['disable']);
        }
        if (isset($param['is_tadacopy']) && $param['is_tadacopy'] != '') {
            $query->where('is_tadacopy', '=', $param['is_tadacopy']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Gnable/disable a or any news sites.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $site = self::find($id);
            $site->set('disable', $param['disable']);
            if (!$site->save()) {
                return false;
            }
            Model_Tag::update_counter_by_site($id);
        }
        return true;
    }

    /**
     * Add or update info for news sites.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Return the integer or the boolean.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $company = new self;
        if (!empty($id)) {
            $company = self::find($id);
            if (empty($company)) {
                return false;
            }
        }
        if (!empty($param['name'])) {
            $company->set('name', $param['name']);
        }
        if (!empty($param['introduction_text'])) {
            $company->set('introduction_text', $param['introduction_text']);
        }
        if (!empty($param['thumbnail_img'])) {
            $company->set('thumbnail_img', $param['thumbnail_img']);
        }
        if (!empty($param['feed_url'])) {
            $company->set('feed_url', $param['feed_url']);
        }
        if (isset($param['is_tadacopy']) && $param['is_tadacopy'] != '') {
            $company->set('is_tadacopy', $param['is_tadacopy']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $company->set('disable', $param['disable']);
        }
        if ($company->save()) {
            if (empty($company->id)) {
                $company->id = self::cached_object($company)->_original['id'];
            }
            return !empty($company->id) ? $company->id : 0;
        }
        return false;
    }
    
    /**
     * Update_counter - rss_count and feed_count news sites.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_counter($param)
    {  
        if (empty($param['news_site_id']))
        {
            return false;
        }
        $site_ids = explode(',', $param['news_site_id']);
        $rssCountArr = Model_News_Sites_Rss::get_rss_count($param['news_site_id']);
        $feedCountArr = Model_News_Feed::get_feed_count($param['news_site_id']);
        foreach ($site_ids as $id)
        {
            $rssCount = 0;
            $feedCount = 0;
            foreach ($rssCountArr as $rssCnt)
            {
                if ($id == $rssCnt['news_site_id'])
                {
                    $rssCount = $rssCnt['rss_count'];
                    break;
                }
            }
            foreach ($feedCountArr as $feedCnt)
            {
                if ($id == $feedCnt['news_site_id'])
                {
                    $feedCount = $feedCnt['feed_count'];
                    break;
                }
            }

            $site = self::find($id);
            if (!empty($site))
            {
                $site->set('rss_count', $rssCount);
                $site->set('feed_count', $feedCount);
                if (!$site->update())
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Set public for tadacopy
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function is_tadacopy($param)
    {
        $options['where'] = array(
            'id' => $param['id'],
        );
        $self = self::find('first', $options);
        if ($self) {
            $self->set('is_tadacopy', $param['is_tadacopy']);
            if ($self->update()) {
                return true;
            }
        }
        static::errorNotExist('news_site_id');
        return false;
    }
}
