<?php
/**
 * Model to operate to News_Feed_Category's functions.
 * 
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_News_Feed_Category extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'category_id',
		'news_feed_id',
		'disable',
		'source',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_feed_categories';
    protected static $_source_comment = 1;
    protected static $_source_tag = 2;
    
    /**
     * Add/Update for news comments.
     *
     * @author Thai Lai
     * @param array $param Input data.
     * @return bool Success or otherwise
     *
     */
    public static function add_update_for_comment($param, $new = true)
    { 
        if (empty($param['company_id']) || empty($param['news_feed_id'])) {            
            return false;
        }         
        $options['where'] = array(
            'company_id' => $param['company_id'],           
        );
        $categories = \Model_Company_Category::find('all', $options);       
        foreach ($categories as $category) {    
            if ($new) {
                if (!self::_add(array(
                    'news_feed_id' => $param['news_feed_id'],
                    'category_id' => $category->get('category_id'),
                    'source' => self::$_source_comment,
                ))) {
                    return false;
                }               
            } else {
                if (!self::_update(array(
                    'news_feed_id' => $param['news_feed_id'],
                    'category_id' => $category->get('category_id'),
                    'source' => self::$_source_comment,
                ))) {
                    return false;
                }   
            }              
        }
        return true; 
    }
    
    /**
     * Add/Update for tag.
     *
     * @author Thai Lai
     * @param array $param Input data.
     * @return bool Success or otherwise
     *
     */
    public static function add_update_for_tag($param, $new = true)
    { 
        $param['source'] = self::$_source_tag;
        if ($new) {
            if (!self::_add($param)) {
                return false;
            }  
        } else {
            if (!self::_update($param)) {
                return false;
            }
        }  
        return true;
    }    
    
    /**
     * For disable/enable comment/tag
     *
     * @author Thai Lai
     * @param array $param Input data.
     * @return bool Success or otherwise
     *
     */
    private static function _update($param)
    { 
        if (!empty($param['tag_id']) && empty($param['category_id'])) {
            $tag = Model_Tag::find($param['tag_id']);
            if (!empty($tag) && empty($tag->get('category_id'))) {
                return true;
            }
            $param['category_id'] = $tag->get('category_id');
        }
        if (empty($param['category_id']) || empty($param['news_feed_id'])) {           
            return false;
        }
        $options['where'] = array(
            'category_id' => $param['category_id'],           
            'news_feed_id' => $param['news_feed_id'],           
        );
        $self = self::find('first', $options);       
        $source = $param['source'];
        if (!empty($self)) {
            if ($source == self::$_source_comment) {
                //$news_feeds = Model_News_Feed::find($param['news_feed_id']);
                $options['where'] = array(                         
                    'news_feed_id' => $param['news_feed_id'],           
                    'disable' => '0',           
                );                            
                if (!empty(Model_News_Comment::count($options))) {                
                    return true;
                }              
            }
            if (($self->get('source')|$source) == $source) {
                $self->set('disable', '1');
            } else {
                $self->set('source', ($source^$self->get('source')));
            }
            if (!$self->update()) {
                \LogLib::warning("Can not update news_feed_categories", __METHOD__, $param);
                return false;
            }    
        }
        return true;
    }
    
    /**
     * For add comment/tag
     *
     * @author Thai Lai
     * @param array $param Input data.
     * @return bool Success or otherwise
     *
     */
    private static function _add($param)
    { 
        if (!empty($param['tag_id']) && empty($param['category_id'])) {
            $tag = Model_Tag::find($param['tag_id']);
            if (!empty($tag) && empty($tag->get('category_id'))) {
                return true;
            }
            $param['category_id'] = $tag->get('category_id');
        }
        if (empty($param['category_id']) || empty($param['news_feed_id'])) {
            self::errorParamInvalid('category_id_or_news_feed_id');
            return false;
        }
        $options['where'] = array(
            'category_id' => $param['category_id'],           
            'news_feed_id' => $param['news_feed_id'],           
        );      
        $self = self::find('first', $options);       
        $source = $param['source'];
        if (!empty($self)) {            
            if ($self->get('disable') == 0) {
                $source = ($self->get('source')|$source);
            }
            $self->set('news_feed_id', $param['news_feed_id']);
            $self->set('category_id', $param['category_id']);             
            $self->set('source', $source);
            $self->set('disable', '0');
            if (!$self->update())
            {
                \LogLib::warning("Can not update news_feed_categories", __METHOD__, $param);
                return false;
            }
        } else {
            $self = new self;
            $self->set('news_feed_id', $param['news_feed_id']);
            $self->set('category_id', $param['category_id']);             
            $self->set('source', $source);         
            if (!$self->create())
            {
                \LogLib::warning("Can not insert news_feed_categories", __METHOD__, $param);
                return false;
            }
        }
        return true;
    }
    
}
