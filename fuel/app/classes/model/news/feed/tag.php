<?php

/**
 * Any query in Model News Feed Tag,
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Feed_Tag extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'tag_id',
        'news_feed_id',
        'disable',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'news_feed_tags';

    /**
     * Add a tag for news feed.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool|int Return the boolean or the integer.
     *
     */
    public static function add($param)
    {
        $options['where'] = array(
            'tag_id' => $param['tag_id'],
            'news_feed_id' => $param['news_feed_id']
        );
        $tag = self::find('first', $options);
        $tagId = $param['tag_id'];
        if ($tag) {
            $tag->set('disable', '0');
            if (!$tag->update()){
                \LogLib::warning("Can not update news_feed_tag", __METHOD__, $param);
                return false;
            }
        } else {
            $tag = new self;
            $tag->set('tag_id', $param['tag_id']);
            $tag->set('news_feed_id', $param['news_feed_id']);
            if (!$tag->create()){
                \LogLib::warning("Can not insert news_feed_tag", __METHOD__, $param);
                return false;
            }            
            $tagId = self::cached_object($tag)->_original['id'];            
        }
        if (!empty($param['tag_id'])) {
            if (Model_News_Feed_Category::add_update_for_tag(array(
                     'tag_id' => $param['tag_id'],
                     'news_feed_id' => $param['news_feed_id'],
                 )
            )) {
                $sql = "UPDATE news_feeds SET updated = UNIX_TIMESTAMP() WHERE id = {$param['news_feed_id']}";
                DB::query($sql)->execute();
            }
        }
        return true;
    }
    
    /**
     * Update news_feed_categories and news_feeds.
     * 
     * @param array $param Input array.
     * @return boolean True if update successfully or otherwise.
     */
    public static function update_data($param) {
        $conditions = array();
        $conditions['where'][] = array(
            'id' => $param['tag_id']
        );
        $category = \Model_Tag::find('first', $conditions);
        if (!empty($category)) {
            $result = \Model_News_Feed_Category::add_by_admin(array(
                        'category_id' => $category['category_id'],
                        'news_feed_id' => $param['news_feed_id'],
            ));
            if (!$result)
                return false;
        }
        
        //Update news_feeds.updated.
        $sql = "UPDATE news_feeds"
                . " SET updated = UNIX_TIMESTAMP()"
                . " WHERE id = {$param['news_feed_id']}";
        $rs = DB::query($sql)->execute();
        if (!$rs)
            return false;
    }

    /**
     * Disable/enable a news feed tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Returns the boolean or integer.
     *
     */
    public static function disable($param)
    {
        $options['where'] = array(
            'tag_id' => $param['tag_id'],
            'news_feed_id' => $param['news_feed_id']
        );
        $tag = self::find('first', $options);
        if ($tag) {
            $tag->set('disable', $param['disable']);
            if ($tag->update()) {
                Model_News_Feed_Category::add_update_for_tag(array(
                        'tag_id' => $tag->get('tag_id'),
                        'news_feed_id' => $param['news_feed_id'],                        
                    ),
                    (empty($param['disable']) ? 1 : 0) // new or update
                );
                //enable news_feed_tag
                if (empty($param['disable'])) {
                    $sql = "UPDATE news_feeds SET updated = UNIX_TIMESTAMP() WHERE id = {$param['news_feed_id']}";
                    DB::query($sql)->execute();
                }
                
                /*      
                if($param['disable'] == 1){
                    if (Model_News_Feed_Category::add_update_for_tag(array(
                            'tag_id' => $tagId,
                            'news_feed_id' => $param['news_feed_id'],
                        )
                   )) {
                       
                   }
            
                     //Delete news_feed_category.
                    \Model_News_Feed::update_news_feed_categories(array(
                            'tag_id' => $param['tag_id'],
                            'news_feed_id' => $param['news_feed_id']
                        ));
                }  else {
                    //Update news_feed_category and news_feed_tags.
                    self::add($param);
                } */
                return true;
            }
        } else {
            static::errorNotExist('news_feed_tag_id', $param);
            return false;
        }
    }

    /**
     * Get all tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_all($param = null)
    {
        $query = DB::select(
            array('tags.id', 'id'),
            array('tags.name', 'tag_name'),
            array('tags.color', 'tag_color'),
            array('tags.type', 'tag_type'),    
            'news_feed_id'
        )
            ->from(self::$_table_name)
            ->join('tags')
            ->on(self::$_table_name . '.tag_id', '=', 'tags.id')
            ->where(self::$_table_name . '.disable', '0');
        if (!empty($param['news_feed_id'])) {
            $query->where('news_feed_id', '=', $param['news_feed_id']);
        }

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get list tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['color'])) {
            $query->where(self::$_table_name . '.color', 'LIKE', "%{$param['color']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }
}
