<?php

/**
 * Any query in Model News Feed Favorite.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Feed_Favorite extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'news_site_id',
        'news_feed_id',
        'user_id',
        'disable',
        'created',
        'updated',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'news_feed_favorites';

    /**
     * Get list news feed favorite by news feed id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . ".*",
            array('news_feeds.title', 'feeds_title'),
            array('users.name', 'user_name'),
            array('users.nickname', 'user_nickname'),
            DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username"),
            array('user_profiles.email', 'email'),
            DB::expr(
                "IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get(
                    'no_image_user'
                ) . "') AS image_url"
            ),
            array('news_sites.name', 'site_name')
        )
            ->from(self::$_table_name)
            ->join('news_sites', 'LEFT')
            ->on(self::$_table_name . ".news_site_id", '=', 'news_sites.id')
            ->join('news_feeds', 'LEFT')
            ->on(self::$_table_name . ".news_feed_id", '=', 'news_feeds.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . ".user_id", '=', 'users.id')
            ->join('user_profiles', 'LEFT')
            ->on("user_profiles.user_id", '=', 'users.id');

        if (!empty($param['user_name'])) {
            $query->where('users.name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['user_nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['user_nickname']}%");
        }
        if (!empty($param['site_name'])) {
            $query->where('news_sites.name', 'LIKE', "%{$param['site_name']}%");
        }

        if (!empty($param['email'])) {
            $query->where('user_profiles.email', '=', "{$param['email']}");
        }
        if (!empty($param['title'])) {
            $query->where('news_feeds.title', 'LIKE', "%{$param['title']}%");
        }
        if (!empty($param['news_site_id'])) {
            $query->where('news_site_id', '=', $param['news_site_id']);
        }
        if (!empty($param['news_feed_id'])) {
            $query->where('news_feeds.id', '=', $param['news_feed_id']);
        }
       
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $tableOrder = $sortExplode[0] == 'email' ? "user_profiles" : ($sortExplode[0] == 'created' ? static::$_table_name : "users");
            $query->order_by("$tableOrder.$sortExplode[0]", $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any news feed favorites.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     *
     */
    public static function disable($param)
    {
        try {
            if (!isset($param['disable'])) {
                $param['disable'] = '1';
            }
            if (!empty($param['id'])) {
                $options['where'][] = array(
                    'id' => $param['id'],
                );
            } else {
                $options['where'][] = array(
                    'news_feed_id' => $param['news_feed_id'],
                    'user_id'      => $param['user_id'],
                    'disable'      => '0'
                );
            }
            $data = self::find('first', $options);
            if ($data) {
                $data->set('disable', $param['disable']);
                if ($data->update()) {
                    $param['news_feed_id'] = $data->_data['news_feed_id'];
                    $param['user_id'] = $data->_data['user_id'];
                    if (!Model_News_Feed::update_favorite_count($param)) {
                        return false;
                    }

                    return true;
                }

                return false;
            } else {
                if (!empty($param['id'])) {
                    static::errorNotExist('id', $param['id']);
                } else {
                    static::errorNotExist('news_feed_id', $param['news_feed_id']);
                }

                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
    /**
     * Count newfeed by id.
     *
     * @author Le Tuan Tu
     * @param int $newFeedId Input newFeedId or not input data.
     * @return int Returns the integer.
     */
    public static function count_by_news_feeds_id($newsFeedId = null)
    {
        if (empty($newsFeedId)) {
            return false;
        }
        $options['where'][] = array(
            'news_feed_id' => $newsFeedId,
            'disable'      => 0
        );
        $count = self::count($options);

        return $count;
    }

    /**
     * Add info for news feed favorites.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Return the integer or boolean.
     *
     */
    public static function add($param)
    {
        $query = DB::select(
                DB::expr("IFNULL(news_feeds.news_site_id, 0) AS news_site_id"),               
                array('news_feeds.id', 'news_feed_id'),
                array('news_feed_favorites.id', 'favorites_id'),
                array('news_feed_favorites.user_id', 'user_id'),
                array('news_feed_favorites.disable', 'favorite_disable')
            )
            ->from('news_feeds')
            ->join(
                DB::expr(
                    "(SELECT * FROM news_feed_favorites WHERE user_id={$param['user_id']}) AS news_feed_favorites"
                ),
                'LEFT'
            )
            ->on('news_feeds.id', '=', 'news_feed_favorites.news_feed_id')
            ->where('news_feeds.id', '=', $param['news_feed_id'])
            ->where('news_feeds.disable', '=', '0')
            ->limit(1)
            ->offset(0);
        $data = $query->execute()->as_array();
        $data = !empty($data[0]) ? $data[0] : array();
        if (empty($data['news_feed_id'])) {
            static::errorNotExist('news_feed_id', $param['news_feed_id']);
            return false;
        }
        if (!empty($data['user_id']) && $data['favorite_disable'] == 0) {
            static::errorDuplicate('user_id', $param['user_id']);
            return false;
        }
        $new = false;
        if (!empty($data['user_id']) && $data['favorite_disable'] == 1) {
            $dataUpdate = array(
                'id' => $data['favorites_id'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'news_feed_id' => $data['news_feed_id'],
                'news_site_id' => $data['news_site_id'],
                'user_id' => $param['user_id']
            );
        }
        $favorites = new self($dataUpdate, $new);
        if ($favorites->save()) {
            if ($new == true) {
                $favorites->id = self::cached_object($favorites)->_original['id'];
            }
            return $favorites->id;
        }
        return false;      
    }

    /**
     * Get list data to report news_feed_favorites.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_countFavorite($param)
    {
        $query = DB::select(
            DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),
            DB::expr("COUNT(id) AS favorite_count ")
        )
            ->from(self::$_table_name);
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $query->group_by('date');
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get news feed favorites report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_news_feed_favorite_report($param)
    {
        $query = DB::select(
            DB::expr("COUNT(*) AS news_feed_favorite_count")
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $data = $query->execute()->as_array();

        return $data ? $data[0] : array();
    }

    /**
     * Get list news feed favorite by user id for mobile.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     *
     */
    public static function get_list_for_mobile($param)
    {
        $param['id'] = $param['user_id'];
        list($total, $data) = \Model_News_Feed::get_news_feed_for_user_mobile($param);

        return array('total' => $total, 'data' => $data);
    }
}
