<?php

/**
 * Model to operate to News_Feed_View_Logs's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_News_Feed_View_Log extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'user_id',
        'news_feed_id',
        'share_type',
        'created',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'news_feed_view_logs';

    /**
     * Function to get a list of News_Feed_View_Logs.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        //$query = DB::select(static::$_table_name . '.*')->from(static::$_table_name);
        $query = DB::select(static::$_table_name . ".*"
                        , array('news_feeds.title', 'news_feeds_title')
                        , array('users.name', 'user_name')
                        , array('users.nickname', 'nickname')
                        , array('user_profiles.email', 'email')
                )
                ->from(static::$_table_name)
                ->join('users')
                ->on(static::$_table_name . ".user_id", '=', 'users.id')
                ->join('user_profiles')
                ->on("user_profiles.user_id", '=', 'users.id')
                ->join('news_feeds')
                ->on('news_feeds.id', '=', static::$_table_name . '.news_feed_id');

        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }

        if (!empty($param['title'])) {
            $query->where("news_feeds.title", 'LIKE', "%{$param['title']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }

        if (!empty($param['user_id'])) {
            $query->where(static::$_table_name . ".user_id", $param['user_id']);
        }

        if (!empty($param['news_feed_id'])) {
            $query->where(static::$_table_name . ".news_feed_id", $param['news_feed_id']);
        }

        if (!empty($param['share_type'])) {
            $query->where(static::$_table_name . ".share_type", $param['share_type']);
        }

        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }

        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }

        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to add or update a News_Feed_View_Logs.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or boolean.
     */
    public static function add_update($param) {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $newsfeedviewlog = new self;
        if (!empty($id)) {
            $newsfeedviewlog = self::find($id);
            if (empty($newsfeedviewlog)) {
                static::errorNotExist('news_feed_view_log_id', $param['id']);
                return false;
            }
        }

        if (empty($param['user_id']) || empty($param['news_feed_id'])) {
            \LogLib::warning('News Feed Id | UserId is empty', __METHOD__, $param);
            static::errorParamInvalid('news_feed_id_or_user_id');
            return false;
        }
        
        // set information for subcategory if having        
        $newsfeedviewlog->set('user_id', $param['user_id']);       
        $newsfeedviewlog->set('news_feed_id', $param['news_feed_id']);       

        if (isset($param['share_type'])) {
            $newsfeedviewlog->set('share_type', $param['share_type']);
        }

        //check id for adding new or updating
        if ($newsfeedviewlog->save()) {
            if (empty($newsfeedviewlog->id)) {
                $newsfeedviewlog->id = self::cached_object($newsfeedviewlog)->_original['id'];
            }
            return !empty($newsfeedviewlog->id) ? $newsfeedviewlog->id : 0;
        }
        return false;
    }

    /**
     * Get news feed view report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_view_report($param)
    {
        $query = DB::select(
            DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),
            DB::expr("COUNT(*) AS view_count")
        )
            ->from(self::$_table_name);
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        $query->group_by('date');
        $data = $query->execute()->as_array();

        return $data;
    }
    
    /**
     * Get statistical data from news feeds.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_pv_report($param)
    {
        $query = DB::select(
                "news_feed_id",
                DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),    
                DB::expr("COUNT(news_feed_id) AS pv")
            )
            ->from(self::$_table_name);
        if (!empty($param['date_from']))
        {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to']))
        {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['news_feed_id']))
        {
            $query->where('news_feed_id', '=', $param['news_feed_id']);
        }
        $query->group_by('news_feed_id')
              ->group_by('date')
              ->order_by('created');
        $data = $query->execute()->as_array();
        return $data;
    }
    
    /**
     * Get statistical data from news feeds.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_uu_report($param)
    {
        $date_from = "";
        $date_to = "";
        if (!empty($param['date_from']))
        {
            $date_from = " AND created >= ".self::date_from_val($param['date_from'])." ";
        }
        if (!empty($param['date_to']))
        {
            $date_to = " AND created <= ".self::date_from_val($param['date_to'])." ";
        }
        $query = DB::select(
                "news_feed_id", 
                "date",                
                DB::expr("COUNT(user_id) AS uu")
            )
            ->from(DB::expr("(SELECT DISTINCT user_id, news_feed_id, DATE(FROM_UNIXTIME(created)) AS date
                                FROM news_feed_view_logs
                                WHERE 1=1 {$date_from} {$date_to}
                                ) AS news_feed_view_logs"));

        if (!empty($param['news_feed_id']))
        {
            $query->where('news_feed_id', '=', $param['news_feed_id']);
        }
        $query->group_by('news_feed_id')
              ->group_by('date')
              ->order_by('date');
        $data = $query->execute()->as_array();
        return $data;
    }

}
