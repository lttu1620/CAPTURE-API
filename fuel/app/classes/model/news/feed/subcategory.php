<?php
/**
 * Model to operate to News_Feed_Subcategory's functions.
 * 
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_News_Feed_Subcategory extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'subcategory_id',
		'news_feed_id',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_feed_subcategories';

}
