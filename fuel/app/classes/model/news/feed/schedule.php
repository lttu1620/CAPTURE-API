<?php
/**
 * Model to operate to News_Feed_Schedule's functions.
 * 
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_News_Feed_Schedule extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'news_feed_id',
		'start_date',
		'finish_date',
		'created',
		'updated',
        'flag'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_feed_schedules';

    /**
     * Function to add or update a user.
     *
     * @author Tuanc cao
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param, $encodePwd = true) {
         //check id if existing
        $id     = !empty($param['id']) ? $param['id'] : 0;
        $flag   = 0;
        $nfc    = new self;
        if (!empty($id)) {
            $nfc = self::find($id);
            if (empty($nfc)) {
                static::errorNotExist('news_feed_schedule_id', $param['id']);
                return false;
            }
            $flag = !empty($nfc->flag) ? $nfc->flag : $flag;
        }
        if (!empty($param['news_feed_id'])) {
            $options['where'] = array(
                'news_feed_id'=> $param['news_feed_id']
            );
            $nfc = self::find('first', $options);
            if (!empty($nfc)) {
                $flag = !empty($nfc->flag) ? $nfc->flag : $flag;
                //static::errorDuplicate('news_feed_id', $param['news_feed_id']);
                //return false;
            }else
                $nfc = new self;
        }
        $nfc->set('flag', $flag);
        // set information for subcategory if having
        if (!empty($param['news_feed_id'])) {
            $nfc->set('news_feed_id', $param['news_feed_id']);
        }

        if (isset($param['start_date'])) {
            $nfc->set('start_date', $param['start_date']);
        }

        if (isset($param['finish_date'])) {
            $nfc->set('finish_date', $param['finish_date']);
        }

        //check id for adding new or updating
        if ($nfc->save()) {
            if (empty($nfc->id)) {
                $nfc->id = self::cached_object($nfc)->_original['id'];
            }            
            return !empty($nfc->id) ? $nfc->id : 0;
        }
        return false;
    }
}
