<?php

/**
 * odel to operate to News_Feed_View_Logs's functions
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_News_Feed_Import_Log extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'news_feed_id',
        'detail_url',
        'detail_url_md5',
        'status',
        'created',
    );
    protected static $_table_name = 'news_feed_import_logs';

    /**
     * Function to get a list of News_Feed_View_Logs
     *
     * @author diennvt
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        //$query = DB::select(static::$_table_name . '.*')->from(static::$_table_name);
        $query = DB::select(static::$_table_name . ".*"
                , array('news_feeds.title', 'news_feeds_title')
                , array('users.name', 'user_name')
                , array('users.nickname', 'nickname')
                , array('user_profiles.email', 'email')
            )
            ->from(static::$_table_name)
            ->join('news_feeds')
            ->on('news_feeds.id', '=', static::$_table_name . '.news_feed_id');

        if (!empty($param['title'])) {
            $query->where("news_feeds.title", 'LIKE', "%{$param['title']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }

        if (!empty($param['news_feed_id'])) {
            $query->where(static::$_table_name . ".news_feed_id", $param['news_feed_id']);
        }

        if (!empty($param['detail_url'])) {
            $query->where(static::$_table_name . ".detail_url", $param['detail_url']);
        }

        if (!empty($param['detail_url_md5'])) {
            $query->where(static::$_table_name . ".detail_url_md5", $param['detail_url_md5']);
        }

        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }

        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }

        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Function to add or update a News_Feed_View_Logs.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param) {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $newsfeedimportlog = new self;
        if (!empty($id)) {
            $newsfeedimportlog = self::find($id);
            if (empty($newsfeedimportlog)) {
                static::errorNotExist('news_feed_import_log_id', $param['id']);
                return false;
            }
        }

        // set information for subcategory if having
        if (!empty($param['detail_url'])) {
            $newsfeedimportlog->set('detail_url', $param['detail_url']);
        }
        if (!empty($param['detail_url_md5'])) {
            $newsfeedimportlog->set('detail_url_md5', $param['detail_url_md5']);
        }
        if (!empty($param['news_feed_id'])) {
            $newsfeedimportlog->set('news_feed_id', $param['news_feed_id']);
        }
        if (!empty($param['status'])) {
            $newsfeedimportlog->set('status', $param['status']);
        } 

        //check id for adding new or updating
        if ($newsfeedimportlog->save()) {
            if (empty($newsfeedimportlog->id)) {
                $newsfeedimportlog->id = self::cached_object($newsfeedimportlog)->_original['id'];
            }
            return !empty($newsfeedimportlog->id) ? $newsfeedimportlog->id : 0;
        }
        return false;
    }

}
