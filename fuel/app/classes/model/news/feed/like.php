<?php

/**
 * Any query in Model News Feed Like.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Feed_Like extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'news_site_id',
		'news_feed_id',
		'user_id',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_feed_likes';

    /**
     * Get list news feed like by news feed id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Return array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select(static::$_table_name . ".*"
            , array('news_feeds.title', 'feeds_title')
            , array('users.name', 'user_name')
            , array('users.nickname', 'user_nickname')
            , DB::expr("IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username")
            , array('user_profiles.email', 'email')
            , DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url")
            , array('news_sites.name', 'site_name')
        )
            ->from(self::$_table_name)
            ->join('news_sites', 'LEFT')
                ->on(static::$_table_name . ".news_site_id", '=', 'news_sites.id')
            ->join('news_feeds', 'LEFT')
                ->on(static::$_table_name . ".news_feed_id", '=', 'news_feeds.id')
            ->join('users', 'LEFT')
                ->on(static::$_table_name . ".user_id", '=', 'users.id')
            ->join('user_profiles', 'LEFT')
                ->on("user_profiles.user_id", '=', 'users.id');
        if (!empty($param['news_site_id'])) {
            $query->where('news_site_id', '=', $param['news_site_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where('users.name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (isset($param['news_feed_id']) && $param['news_feed_id'] != '') {
            $query->where('news_feed_id', '=', $param['news_feed_id']);
        }
        if (!empty($param['user_id'])) {
           $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any news feed likes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $likes = self::find($id);
            $likes->set('disable', $param['disable']);
            if (!$likes->save()) {
                return false;
            }
            Model_News_Feed::update_like_count(array(
                'news_feed_id' => $likes->_data['news_feed_id'],
            ));
        }

        return true;
    }

    /**
     * Add info for news feed likes.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return int|bool Returns the integer or boolean.
     */
    public static function add($param)
    {      
        $query = DB::select(
                DB::expr("IFNULL(news_feeds.news_site_id, 0) AS news_site_id"),               
                array('news_feeds.id', 'news_feed_id'),
                array('news_feed_likes.id', 'likes_id'),
                array('news_feed_likes.user_id', 'user_id'),
                array('news_feed_likes.disable', 'likes_disable')
            )
            ->from('news_feeds')
            ->join(
                DB::expr(
                    "(SELECT * FROM news_feed_likes WHERE user_id={$param['user_id']}) AS news_feed_likes"
                ),
                'LEFT'
            )
            ->on('news_feeds.id', '=', 'news_feed_likes.news_feed_id')
            ->where('news_feeds.id', '=', $param['news_feed_id'])
            ->where('news_feeds.disable', '=', '0')
            ->limit(1)
            ->offset(0);
        $data = $query->execute()->as_array();
        $data = !empty($data[0]) ? $data[0] : array();
        if (empty($data['news_feed_id'])) {
            static::errorNotExist('news_feed_id', $param['news_feed_id']);
            return false;
        }
        if (!empty($data['user_id']) && $data['likes_disable'] == 0) {
            static::errorDuplicate('user_id', $param['user_id']);
            return false;
        }
        $new = false;
        if (!empty($data['user_id']) && $data['likes_disable'] == 1) {
            $dataUpdate = array(
                'id' => $data['likes_id'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'news_feed_id' => $data['news_feed_id'],
                'news_site_id' => $data['news_site_id'],
                'user_id' => $param['user_id']
            );
        }
        $likes = new self($dataUpdate, $new);
        if ($likes->save()) {
            if ($new == true) {
                $likes->id = self::cached_object($likes)->_original['id'];
            }
            return $likes->id;
        }
        return false;
    }

    /**
     * <get list data to report news_feed_likes>
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_countLike($param) 
    {        
        $query = DB::select(
            DB::expr("DATE(FROM_UNIXTIME(created)) AS date"),
            DB::expr("COUNT(id) AS like_count")
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
            $query->group_by('date');
        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get news feed likes report.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_news_feed_like_report($param)
    {
        $query = DB::select(
            DB::expr("COUNT(*) AS news_feed_like_count")
        )
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        $data = $query->execute()->as_array();

        return $data ? $data[0] : array();
    }
}
