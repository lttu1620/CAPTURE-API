<?php

use Fuel\Core\DB;

/**
 * Any query in Model News Sites Rss.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Sites_Rss extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'news_site_id',
        'name',
        'description',
        'url',
        'disable',
        'tag_item_title',
        'tag_item_link',
        'tag_item_description',
        'tag_item_date',
        'tag_item_image',
        'created',
        'updated',
        
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'news_sites_rss';

    /**
     * Get list news site rss.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select(
            array('news_sites.name', 'news_site_name'),
            self::$_table_name . '.*'
        )
            ->from('news_sites')
            ->join(self::$_table_name)
            ->on('news_sites.id', '=', self::$_table_name . '.news_site_id');
        if (!empty($param['news_site_id'])) {
            $query->where('news_site_id', '=', $param['news_site_id']);
        }
        if (!empty($param['name'])) {
            $query->where('news_sites_rss.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['description'])) {
            $query->where('description', 'LIKE', "%{$param['description']}%");
        }
        if (!empty($param['url'])) {
            $query->where('url', 'LIKE', "%{$param['url']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['time'])) {
            $param['time'] = trim($param['time']);
            $query->where(self::$_table_name .'.updated', '<=', $param['time']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any news site rss.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $rss = self::find($id);
            if ($rss) {
                $rss->set('disable', $param['disable']);
                if (!$rss->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('news_site_rss_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Add or update info for news sites rss.
     *
     * @author tuancd
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int Return the boolean or the integer.
     */
    public static function add_update($param)
    {
        try {
            //check id if existing
            $id = !empty($param['id']) ? $param['id'] : 0;
            $rss = new self;
            if (!empty($id)) {
                $rss = self::find($id);
                if (empty($rss)) {
                    static::errorNotExist('news_site_rss_id', $id);
                    return false;
                }
            }
            if (!empty($param['news_site_id'])) {
                $rss->set('news_site_id', $param['news_site_id']);
            }
            if (!empty($param['url'])) {
                $rss->set('url', $param['url']);
            }
            if (!empty($param['name'])) {
                $rss->set('name', $param['name']);
            }
            if (!empty($param['description'])) {
                $rss->set('description', $param['description']);
            }
            if (!empty($param['tag_item_title'])) {
                $rss->set('tag_item_title', $param['tag_item_title']);
            }
            if (!empty($param['tag_item_link'])) {
                $rss->set('tag_item_link', $param['tag_item_link']);
            }
            if (!empty($param['tag_item_description'])) {
                $rss->set('tag_item_description', $param['tag_item_description']);
            }
            if (!empty($param['tag_item_date'])) {
                $rss->set('tag_item_date', $param['tag_item_date']);
            }
            if (!empty($param['tag_item_image'])) {
                $rss->set('tag_item_image', $param['tag_item_image']);
            }
            if ($rss->save()) {
                if (empty($rss->id)) {
                    $rss->id = self::cached_object($rss)->_original['id'];
                }
                return !empty($rss->id) ? $rss->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    /**
     * Get rss count from news sites rss.
     *
     * @author diennvt
     * @param list $site_ids Input site_ids
     * @return array Returns the array.
     */
    public static function get_rss_count($site_ids)
    { 
        $query = DB::select(
            self::$_table_name . ".news_site_id", 
            DB::expr("COUNT(" . self::$_table_name . ".id) AS rss_count")
            )
            ->from(self::$_table_name)
            ->where(self::$_table_name . ".disable", 0)
            ->group_by(self::$_table_name . ".news_site_id");
        if(!empty($site_ids)){
             $query->where(DB::expr("EXISTS ( SELECT id 
                                            FROM news_sites
                                            WHERE news_sites_rss.news_site_id = news_sites.id  AND disable = 0 AND news_sites_rss.news_site_id IN ({$site_ids}))"));
        }
        
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }

}
