<?php

/**
 * Any query in Model News Comment.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_News_Comment extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'news_site_id',
		'news_feed_id',
		'user_id',
                'company_id',
		'comment',
		'comment_short',
                'like_count',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'news_comments';

    /**
     * Get list news comments by news feed id.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        if (empty($param['page'])) {
            $param['page'] = 1;
        }
        if (empty($param['user_id'])) {
            $param['user_id'] = 0;
        }
        $no_image_user = \Config::get('no_image_user');
        $no_image_company = \Config::get('no_image_company');
        $sql = "
            SELECT  DISTINCT users.name AS name,
                    users.nickname AS nickname,                 
                    users.is_company AS is_company,
                    news_feeds.title AS news_feed_title,
                    IF(IFNULL(news_feeds.news_site_id,0) = 0, news_feeds.news_site_name, news_sites.name) AS news_site_name, 
                    IFNULL(IF(users.image_url='',NULL,users.image_url),'{$no_image_user}') AS image_url,                   
                    IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'{$no_image_company}') AS thumbnail_img, 
                    IF(ISNULL(news_comment_likes.news_comment_id), 0, 1) AS is_like,
                    IFNULL(IF(users.nickname='',NULL,users.nickname), users.name) AS display_username, 
                    IF(news_comments.company_id > 0,
                        IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'{$no_image_company}'),
                        IFNULL(IF(users.image_url='',NULL,users.image_url),'{$no_image_user}')) AS display_image, 
                    news_comments.*,
                    IFNULL(clap_count,0) AS clap_count
            FROM news_comments
            JOIN users ON news_comments.user_id = users.id
            LEFT JOIN news_sites ON news_comments.news_site_id = news_sites.id
            LEFT JOIN companies ON news_comments.company_id = companies.id
            JOIN news_feeds ON news_comments.news_feed_id = news_feeds.id
            LEFT JOIN
            (   SELECT  user_id, news_comment_id, COUNT(*) AS clap_count
                FROM    news_comment_likes
                WHERE   disable = 0
                        AND user_id = {$param['user_id']}
                GROUP BY user_id, news_comment_id
            ) news_comment_likes ON news_comments.id=news_comment_likes.news_comment_id                 
            WHERE 1 = 1
        ";
        if ($param['user_id'] != 0) {
            $sql .= "AND news_comments.disable = 0 ";
        }

        if (!empty($param['user_id']) && empty($param['news_feed_id'])) {
            $userReruiter = Model_User_Recruiter::find($param['user_id']);
            if (!empty($userReruiter)) {
                $sql .= " AND news_comments.company_id IN (
                    SELECT company_id
                    FROM user_recruiters
                    WHERE user_id = {$param['user_id']}
                )";
            } else {
                $sql .= "AND (news_comments.user_id={$param['user_id']} OR news_comment_likes.user_id={$param['user_id']})";
            }
        }
        if (!empty($param['comment'])) {
            $sql .= " AND news_comments.comment LIKE ". '"' . "%{$param['comment']}%" . '"';
        }
        if (!empty($param['user_name'])) {
            $sql .= " AND (users.nickname LIKE  '%{$param['user_name']}%' OR users.name LIKE '%{$param['user_name']}%')";
        }
        if (!empty($param['news_feed_title'])) {
            $sql .= " AND news_feeds.title LIKE ". '"' . "%{$param['news_feed_title']}%" . '"';
        }
        if (!empty($param['news_feed_id'])) {
            $sql .= " AND news_feeds.id = {$param['news_feed_id']} ";
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $sql .= " AND news_comments.disable = {$param['disable']} ";
        }
        if (!empty($param['date_from'])) {
            $param['date_from'] = self::date_from_val($param['date_from']);
            $sql .= " AND news_comments.created >= {$param['date_from']} ";
        }
        if (!empty($param['date_to'])) {
            $param['date_to'] = self::date_to_val($param['date_to']);
            $sql .= " AND news_comments.created <= {$param['date_to']} ";
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = "news_comments.created";
            }
            $sql .= " ORDER BY {$sortExplode[0]} {$sortExplode[1]} ";
        } else {
            $sql .= " ORDER BY news_comments.created DESC ";
        }
        if (!empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $sql .= " LIMIT {$offset}, {$param['limit']} ";
        }
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Enable/disable a or any news comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);  
        $newsFeedId = array();
        foreach ($ids as $id) {
            $comment = self::find($id);
            if (empty($comment)) {
                return false;
            }
            $comment->set('disable', $param['disable']);
            if (!$comment->save()) {
                return false;
            }            
            Model_News_Feed_Category::add_update_for_comment(array(
                    'company_id' => $comment->get('company_id'),
                    'news_feed_id' => $comment->get('news_feed_id'),                        
                ),
                (empty($param['disable']) ? 1 : 0) // 1:new - 0:update
            );             
            //enable news_comments
            if (empty($param['disable']) && !isset($newsFeedId[$comment->get('news_feed_id')])) {
                $sql = "UPDATE news_feeds SET updated = UNIX_TIMESTAMP() WHERE id = {$comment->get('news_feed_id')}";
                DB::query($sql)->execute();
            }
            $newsFeedId[$comment->get('news_feed_id')] = $comment->get('news_feed_id'); 
        }   
        return true;
    }

    /**
     * Ddd info for news comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add($param)
    {            
        $newsFeed = Model_News_Feed::find($param['news_feed_id']);
        if ($newsFeed) {
            $comment = new self;
            $comment->set('news_feed_id', $param['news_feed_id']);
            $comment->set('news_site_id', !empty($newsFeed->_data['news_site_id']) ? $newsFeed->_data['news_site_id'] : 0);
            $comment->set('user_id', $param['user_id']);
            $recruiter = DB::select('company_id')
                ->from('user_recruiters')
                ->where('user_id', '=', $param['user_id'])
                ->execute()->as_array();
            if ($recruiter) {
                $comment->set('company_id', $recruiter[0]['company_id']);
            }
            $comment->set('comment', $param['comment']);
            $comment_short = Fuel\Core\Str::truncate($param['comment'], Config::get('short_comment_length'));
            $comment->set('comment_short', $comment_short); 
            $comment->set('like_count', '0');
            if ($comment->create())
            {
                $comment->id = self::cached_object($comment)->_original['id'];
                if ($comment->id)
                { 
                    $newsFeed->set('is_public', '1');
                    if (!$newsFeed->update()) {
                        return false;
                    }
                    if (!empty($recruiter[0]['company_id'])) 
                    { 
                        Model_News_Feed_Category::add_update_for_comment(array(
                                'company_id' => $recruiter[0]['company_id'],
                                'news_feed_id' => $param['news_feed_id'],
                            )
                        );
//                        Model_News_Feed_Category::add(array(
//                                'company_id' => $recruiter[0]['company_id'],
//                                'news_feed_id' => $param['news_feed_id'],
//                            )
//                        );
                    }
                    $data = self::get_detail($comment->id);
                    $data['is_like'] = '0';                       
                    \Lib\Cache::delete("mobile_newsfeeds_my_timeline_{$param['user_id']}");
                    return $data;
                }
            }
        } else {
            static::errorNotExist('news_feed_id', $param['news_feed_id']);           
        }       
        return false;
    }

    /**
     * Update like count for news comment.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_like_count($param)
    {
        $cnt = Model_News_Comment_Like::count_by_news_comment_id($param['news_comment_id']);
        $newsComment = self::find($param['news_comment_id']);
        $newsComment->set('id', $param['news_comment_id']);
        $newsComment->set('like_count', $cnt);
        if (!$newsComment->update())
        {
            return false;
        }
        return true;
    }

    /**
     * Count news comment by news feed id.
     *
     * @author Le Tuan Tu
     * @param int $newsFeedId Input data newFeedId or not input data.
     * @return bool|int Returns the boolean or integer.
     */
    public static function count_by_news_feeds_id($newsFeedId = null)
    {
        if (empty($newsFeedId))
        {
            return false;
        }
        $options['where'][] = array(
            'news_feed_id' => $newsFeedId,
            'disable' => 0
        );
        $count = self::count($options);
        return $count;
    }

    /**
     * Get detail of news comment.
     *
     * @author Le Tuan Tu
     * @param int $id Input id
     * @return array Return the array.
     */
    public static function get_detail($id)
    {
        $query = DB::select(
            array('users.name', 'name'),
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url),'" . \Config::get('no_image_user') . "') AS image_url"),
            array('users.is_company', 'is_company'),
            array('news_feeds.title', 'news_feed_title'),
            array('news_sites.name', 'news_site_name'),
            self::$_table_name . '.*'
        )
            ->from(self::$_table_name)
                ->join('users', 'LEFT')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
                ->join('news_sites', 'LEFT')
            ->on(self::$_table_name . '.news_site_id', '=', 'news_sites.id')
                ->join('news_feeds', 'LEFT')
            ->on(self::$_table_name . '.news_feed_id', '=', 'news_feeds.id')
                ->where(self::$_table_name . '.id', '=', $id);
        $data = $query->execute()->as_array();       
        $data = !empty($data[0]) ? $data[0] : array();
        if (empty($data)) {
            static::errorNotExist('id', $id);
        }
        if (!empty($data['company_id'])) {
            $company = \Model_Company::find($data['company_id']);
            $data['thumbnail_img'] = $company->_data['thumbnail_img'];
        } elseif (!empty($data['image_url'])) {
            $data['thumbnail_img'] = $data['image_url'];
        }
        return $data;
    }

    /**
     * Update info for news comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return int|bool Returns the boolean or integer.
     */
    public static function edit($param)
    {
        try {
            $comment = self::find($param['id']);
            if ($comment) {
                $comment->set('comment', $param['comment']);
                $comment_short = Fuel\Core\Str::truncate($param['comment'], Config::get('short_comment_length'));
                $comment->set('comment_short', $comment_short); 
                if ($comment->update())
                {
                    return $comment->_data['id'];
                }
            } else {
                static::errorNotExist('news_comment_id', $param['id']);
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * get news feed that have been commented by company, sort by comment date
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return mixed
     *
     */
    public static function get_news_feed_newer_by_company($param)
    {
        // set page and limit, get default from Config if not set param
        $page = !empty($param['page']) ? $param['page'] : \Config::get('page_default');
        $limit = !empty($param['limit']) ? $param['limit'] : \Config::get('limit_default');
        $offset = ($page - 1) * $limit;
        $sql = "
            SELECT DISTINCT news_feeds.title,
                   news_feeds.detail_url,
                   IFNULL(IF(news_feeds.image_url='',NULL,news_feeds.image_url),IFNULL(IF(news_sites.thumbnail_img='',NULL,news_sites.thumbnail_img), '" . \Config::get('mobile_no_image_other') . "')) AS image_url,
                   news_sites.name AS site_name,
                   news_feeds.distribution_date,
                   IF (ISNULL(news_feed_favorites.news_feed_id),
                       0,
                       1) AS is_favorite,
                      news_comments.*
            FROM news_feeds
            JOIN news_sites ON news_feeds.news_site_id = news_sites.id
            LEFT JOIN
                (SELECT *
                 FROM news_feed_favorites
                 WHERE disable = 0
                     AND user_id = {$param['user_id']}) news_feed_favorites ON news_feeds.id=news_feed_favorites.news_feed_id
            JOIN
                (SELECT A.news_feed_id,
                        A.comment_id,
                        A.comment_created,
                        A.like_count,
                        A.is_like,
                        A.thumbnail_img,
                        B.user_id,
                        B.comment_short,
                        B.comment,
                        (SELECT COUNT(news_comment_id)
                         FROM news_comment_likes 
                         WHERE disable = 0 AND news_comment_id = A.comment_id AND user_id = {$param['user_id']} 
                         GROUP BY user_id) AS clap_num
                 FROM
                     (SELECT news_comments.news_feed_id,
                             IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'" . \Config::get('mobile_no_image_company') . "') AS thumbnail_img,
                             max(news_comments.id) AS comment_id,
                             max(like_count) AS like_count,
                             max(news_comments.created) AS comment_created,
                             IF (ISNULL(news_comment_likes.news_comment_id),
                                 0,
                                 1) AS is_like
                      FROM news_comments
                      LEFT JOIN
                          (SELECT *
                           FROM news_comment_likes
                           WHERE disable = 0
                               AND user_id = {$param['user_id']}) news_comment_likes ON news_comments.id = news_comment_likes.news_comment_id
                      JOIN companies ON news_comments.company_id = companies.id
                      WHERE news_comments.disable = 0 AND news_comments.company_id = {$param['id']}
                      GROUP BY news_feed_id) A
                 JOIN news_comments B ON A.comment_id = B.id) news_comments ON news_feeds.id = news_comments.news_feed_id
                 WHERE news_sites.disable = 0
                 AND news_feeds.disable = 0
            ORDER BY comment_created DESC
            LIMIT {$offset},{$limit}
        ";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * get news feed that have been commented by company, sort by like count
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return mixed
     *
     */
    public static function get_news_feed_popular_by_company($param)
    {
        // set page and limit, get default from Config if not set param
        $page = !empty($param['page']) ? $param['page'] : \Config::get('page_default');
        $limit = !empty($param['limit']) ? $param['limit'] : \Config::get('limit_default');
        $offset = ($page - 1) * $limit;
        $sql = "
            SELECT DISTINCT news_feeds.title,
                   news_feeds.detail_url,
                   IFNULL(IF(news_feeds.image_url='',NULL,news_feeds.image_url),IFNULL(IF(news_sites.thumbnail_img='',NULL,news_sites.thumbnail_img), '" . \Config::get('mobile_no_image_other') . "')) AS image_url,
                   news_sites.name AS site_name,
                   news_feeds.distribution_date,
                   IF (ISNULL(news_feed_favorites.news_feed_id),
                       0,
                       1) AS is_favorite,
                      news_comments.*
            FROM news_feeds
            JOIN news_sites ON news_feeds.news_site_id = news_sites.id
            LEFT JOIN
                (SELECT *
                 FROM news_feed_favorites
                 WHERE disable = 0
                     AND user_id = {$param['user_id']}) news_feed_favorites ON news_feeds.id=news_feed_favorites.news_feed_id
            JOIN
                ( SELECT A.news_feed_id,
                         A.comment_id,
                         A.comment_created,
                         A.like_count,
                         A.is_like,
                         A.thumbnail_img,
                         B.user_id,
                         B.comment_short,
                         B.comment,
                         (SELECT COUNT(news_comment_id)
                         FROM news_comment_likes 
                         WHERE disable = 0 AND news_comment_id = A.comment_id AND user_id = {$param['user_id']} 
                         GROUP BY user_id) AS clap_num
                 FROM
                     ( SELECT news_comments.news_feed_id,
                              IFNULL(IF(companies.thumbnail_img='',NULL,companies.thumbnail_img),'" . \Config::get('mobile_no_image_company') . "') AS thumbnail_img,
                              max(news_comments.id) AS comment_id,
                              max(like_count) AS like_count,
                              max(news_comments.created) AS comment_created,
                              IF (ISNULL(news_comment_likes.news_comment_id),
                                  0,
                                  1) AS is_like
                      FROM news_comments
                      LEFT JOIN
                          (SELECT *
                           FROM news_comment_likes
                           WHERE disable = 0
                               AND user_id = {$param['user_id']}) news_comment_likes ON news_comments.id=news_comment_likes.news_comment_id
                      JOIN companies ON news_comments.company_id = companies.id
                      WHERE news_comments.disable = 0 AND news_comments.company_id = {$param['id']}
                      GROUP BY news_feed_id ) A
                 JOIN news_comments B ON A.comment_id = B.id ) news_comments ON news_feeds.id = news_comments.news_feed_id
                 WHERE news_sites.disable = 0
                 AND news_feeds.disable = 0
            ORDER BY news_comments.like_count DESC
            LIMIT {$offset},{$limit}
        ";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * update like count for news comment by news feed id
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool
     *
     */
    public static function update_counter_by_news_feeds($param)
    {
        $idArray = explode(',', $param['news_feed_id']);
        foreach ($idArray as $id) {
            $query = DB::select('id')
                ->from('news_comments')
                ->where('news_feed_id', '=', $id)
                ->where('disable', '=', '0');
            $data = $query->execute()->as_array();
            foreach ($data as $item) {
                $sql = "
                    UPDATE news_comments
                    SET    like_count = (SELECT COUNT(*)
                                         FROM   news_comment_likes
                                         WHERE  news_comment_id = {$item['id']} AND disable = 0
                                                AND EXISTS ( SELECT id 
                                                                FROM news_feeds
                                                                WHERE news_comment_likes.news_feed_id = news_feeds.id  AND disable = 0) )
                    WHERE  id = {$item['id']}
                           AND disable = 0
                ";
                DB::query($sql)->execute();
            }
        }

        return true;
    }

    /**
     * update like count for news comment by id
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool
     *
     */
    public static function update_counter($param)
    {
        $idArray = explode(',', $param['news_comment_id']);
        foreach ($idArray as $id) {
            $sql = "
                UPDATE news_comments
                    SET    like_count = (SELECT COUNT(*)
                                         FROM   news_comment_likes
                                         WHERE  news_comment_id = {$id}
                                                AND disable = 0)
                    WHERE  id = {$id}
                           AND disable = 0
            ";
            DB::query($sql)->execute();
        }

        return true;
    }
    
    /**
     * check if user commented or not for a feed
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool
     *
     */
    public static function check_user_commented_for_feed($param)
    {
        $query = DB::select('id')
                ->from(self::$_table_name)
                ->where(self::$_table_name.".news_feed_id", '=', $param['feed_id'])
                ->where(self::$_table_name.".user_id", '=', $param['user_id'])
                ->where(self::$_table_name.".disable", '=', 0);
        
        $data = $query->execute()->as_array();
        
        $total = !empty($data) ? DB::count_last_query() : 0;
        
        if($total > 0) {
            return true;
        }
        return false;
    }
}



