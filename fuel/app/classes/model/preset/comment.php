<?php

/**
 * Any query for model Preset Comments.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Preset_Comment extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'comments',
		'created',
		'updated',
        'disable'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'preset_comments';

    /**
     * Get list preset comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Return array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);

        if (!empty($param['comments'])) {
            $query->where('comments', 'LIKE', "%{$param['comments']}%");
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Get detail preset comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     *
     */
    public static function get_detail($param)
    {
        $options['where'][] = array(
            'id' => $param['id']
        );

        return self::find('first', $options);
    }

    /**
     * Add or update info for preset comments.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool|int Return the boolean or the integer.
     */
    public static function add_update($param)
    {
        try {
            //check id if existing
            $id = !empty($param['id']) ? $param['id'] : 0;
            $comment = new self;
            if (!empty($id)) {
                $comment = self::find($id);
                if (empty($comment)) {
                    static::errorNotExist('preset_comments_id', $id);
                    return false;
                }
            }

            if (!empty($param['comments'])) {
                $comment->set('comments', $param['comments']);
            }
            if ($comment->save()) {
                if (empty($comment->id)) {
                    $comment->id = self::cached_object($comment)->_original['id'];
                }
                return !empty($comment->id) ? $comment->id : 0;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
