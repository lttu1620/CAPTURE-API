<?php

use Fuel\Core\DB;
/**
 * Model to operate Group_Setting's functions.
 *
 * @package Model
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Model_Group_Setting extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'setting_id',
		'group',
		'value',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'group_settings';
        protected static $_setting_type = 'group';
        protected static $_group = array('user','admin','company');

     /**
     * Function to add or update a group_settings.   
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function add_update($param) {
        if(!in_array($param['group'], self::$_group)){
            static::errorNotExist('group', $param['group']);
            return false;
        }
        $values = json_decode($param['value']);
        foreach ($values as $val) {
            $options['where'] = array(
                'group' => $param['group'],
                'setting_id' => $val->setting_id
            );
            //check if update or insert
            $group_setting = self::find('first', $options);
            if(!empty($group_setting)){//if exist then update
                $group_setting->set('group', $param['group']);
                $group_setting->set('setting_id', $val->setting_id);
                $group_setting->set('value', $val->value);
                $group_setting->update();
            }else{//if not exist then insert
                $cs = new self;
                $cs->set('group', $param['group']);
                $cs->set('setting_id', $val->setting_id);
                $cs->set('value', $val->value);
                $cs->save();
            }
        }
        return true;
    }

    /**
     * Function to disable or enable a group_setting.
     *
     * @author diennvt
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id']))
        {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id)
        {
            $groupsetting = self::find($id);
            if (empty($groupsetting))
            {
                return false;
            }
            $groupsetting->set('disable', $param['disable']);
            if (!$groupsetting->update())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to get all user_setting.
     *
     * @author diennvt 
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_all($param) {
        if(!in_array($param['group'], self::$_group)){
            static::errorNotExist('group', $param['group']);
            return false;
        }
        $query = DB::select(
                'settings.name', 
                'settings.description',
                'settings.data_type',
                'settings.default_value', 
                self::$_table_name . '.setting_id', 
                self::$_table_name . '.value'
                )
                ->from('settings')
                ->join(DB::expr("(SELECT * FROM group_settings WHERE group = {$param['group']}) AS group_settings"), 'LEFT')
                ->on('settings.id', '=', self::$_table_name . '.setting_id')
                ->where('settings.type', self::$_setting_type);

            if (!empty($param['name']))
            {
                $query->where('settings.name','LIKE',"{$param['name']}%" );
            }
            if (!empty($param['date_type']))
            {
                $query->where('settings.data_type',$param['data_type']);
            }
            if (isset($param['disable']) && $param['disable'] != '')
            {
                $query->where( self::$_table_name . '.disable',$param['disable'] );
            }
            if (!empty($param['sort'])) {
                $sortExplode = explode('-', $param['sort']);
                if ($sortExplode[0] == 'created') {
                    $sortExplode[0] = self::$_table_name . '.created';
                }
                $query->order_by($sortExplode[0], $sortExplode[1]);
            } else {
                $query->order_by(self::$_table_name . '.created', 'DESC');
            }
            if (!empty($param['page']) && !empty($param['limit'])) 
            {
                $offset = ($param['page'] - 1) * $param['limit'];
                $query->limit($param['limit'])->offset($offset);
            }
            
            $data = $query->execute()->as_array();
            $total = !empty($data) ? DB::count_last_query() : 0;
            return array($total, $data);
    }
}
