<?php

namespace Bus;

/**
 * <UserRecruiters_Detail - API to get detail of Recruiters>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class UserRecruiters_Login extends BusAbstract
{

    protected $_required = array(
        'email',
        'password'
    );
    
    protected $_length = array(
        'password' => array(6, 40),
    );
    
    protected $_email_format = array(
        'email',
    );
   
    public function operateDB($data)
    {
        try {
            $result = \Model_User_Recruiter::login($data);
            if (!empty($result)) {  
                $result['token'] = \Model_Authenticate::addupdate(array(
                    'user_id' => $result['id'],
                    'regist_type' => 'user'
                ));
            }
            $this->_response = $result;
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

