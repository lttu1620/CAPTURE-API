<?php

namespace Bus;

/**
 * get detail campus
 *
 * @package Bus
 * @created 2014-12-01
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_Detail extends BusAbstract
{
    protected $_number_format = array(
        'id'
    );

    /**
     * get detail campus by id
     *
     * @created 2014-12-01
     * @updated 2014-12-01
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campuse::get_detail($data);//find($data['id']);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

