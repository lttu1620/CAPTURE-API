<?php

namespace Bus;

/**
 * get news feed newer of company for mobile
 *
 * @package Bus
 * @created 2014-12-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Companies_NewsFeedNewer extends BusAbstract
{

    protected $_required = array(
        'id',
        'user_id'
    );
    //check length
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'id',
        'user_id',
        'page',
        'limit'
    );

    /**
     * get news feed newer of company for mobile
     *
     * @created 2014-12-25
     * @updated 2014-12-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            list($total, $data) = \Model_News_Comment::get_news_feed_newer_by_company($data);
            $this->_response = array(
                'total' => $total,
                'data' => $data
            );
            return $this->result(\Model_Company::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

