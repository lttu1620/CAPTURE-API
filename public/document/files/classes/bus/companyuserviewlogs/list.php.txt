<?php

namespace Bus;

/**
 * <CompanyUserViewLogs_List - API to get list of CompanyUserViewLogs>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author <truongnn>
 * @copyright Oceanize INC
 */
class CompanyUserViewLogs_List extends BusAbstract
{
    protected $_email_format = array(
        'email'
    );

    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    protected $_number_format = array(
        'page',
        'limit',
        'user_id',
        'company_id',
        'share_type'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_User_View_Log::get_list($data);
            return $this->result(\Model_Company_User_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

