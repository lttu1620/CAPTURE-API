<?php

namespace Bus;

/**
 * Enable/Disable News feed like
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class NewsFeedLikes_Disable extends BusAbstract
{
    
    protected $_length = array(
        'disable' => 1,
    );
    
    protected $_number_format = array(
        'disable'
    );
    /**
     * call function disable() from model News Feed Like
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Feed_Like::disable($data);
            return $this->result(\Model_News_Feed_Like::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

