<?php

namespace Bus;

/**
 * <NewsFeeds_Disable - API to disable or enable a NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class NewsFeeds_Disable extends BusAbstract
{
    //check require
    protected $_required = array(
        'id',
        'disable',
    );

    //check length
    protected $_length = array(        
        'disable' => 1,
    );
    
    //check number
    protected $_number_format = array(       
        'disable'
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::disable($data);
            return $this->result(\Model_News_Feed_Favorite::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

