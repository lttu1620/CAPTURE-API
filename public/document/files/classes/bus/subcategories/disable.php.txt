<?php

namespace Bus;

/**
 * <SubCategories_Disable - API to disable or enable a SubCategory>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class SubCategories_Disable extends BusAbstract
{

    protected $_required = array(
        'id',
        'disable',
    );
    //check length
    protected $_length = array(
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Subcategory::disable($data);
            return $this->result(\Model_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

