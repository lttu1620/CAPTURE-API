<?php

namespace Bus;

/**
 * <SubCategories_List - API to get list of SubCategories>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class SubCategories_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'category_id' => array(1, 11),
        'name' => array(1, 40),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'category_id',
        'disable',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Subcategory::get_list($data);
            return $this->result(\Model_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

