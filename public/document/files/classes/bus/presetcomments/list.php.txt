<?php

namespace Bus;

/**
 * get list preset comments
 *
 * @package Bus
 * @created 2014-12-03
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PresetComments_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'comments' => array(1, 40),
    );
    //check number
    protected $_number_format = array(
        'id',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model Preset Comments
     *
     * @created 2014-12-03
     * @updated 2014-12-03
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Preset_Comment::get_list($data);
            return $this->result(\Model_Preset_Comment::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

