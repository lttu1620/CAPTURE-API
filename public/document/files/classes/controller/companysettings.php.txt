<?php

/**
 * Controller for actions on CompanySettings
 *
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author Dien Nguyen
 * @copyright Oceanize INC
 */
class Controller_CompanySettings extends \Controller_App {

    /**
     * Get all of CompanySetting by action POST
     *  
     * @return boolean
     */
    public function post_all() {
        return \Bus\CompanySettings_All::getInstance()->execute();
    }
    
    /**
     * Get all of CompanySetting by action GET
     *  
     * @return boolean
     */
    public function get_all(){
        return $this->post_all();
    }

    /**
     * Update disable field for CompanySetting  by action POST
     *  
     * @return boolean
     */
    public function post_disable() {
        return \Bus\CompanySettings_Disable::getInstance()->execute();
    }
    
    /**
     * Update disable field for CompanySetting  by action GET
     *  
     * @return boolean
     */
    public function get_disable(){
        return $this->post_disable();
    }

    /**
     * Update or add new information for CompanySetting  by action POST
     *  
     * @return boolean
     */
    public function post_addupdate() {
        return \Bus\CompanySettings_AddUpdate::getInstance()->execute();
    }
    
    /**
     * Update or add new information for CompanySetting  by action GET
     *  
     * @return boolean
     */
    public function get_addupdate() {
        return $this->post_addupdate();
    }
    
    /**
     * Update Multifield for CompanySetting  by action POST
     *  
     * @return boolean
     */
    public function post_multiupdate() {
        return \Bus\CompanySettings_MultiUpdate::getInstance()->execute();
    }

    /**
     * Update Multifield for CompanySetting  by action GET
     *  
     * @return boolean
     */
    public function get_multiupdate() {
        return $this->post_addupdate();
    }

}

