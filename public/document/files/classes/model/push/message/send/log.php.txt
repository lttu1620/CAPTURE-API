<?php

/**
 * Any query for model push message send Logs.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Push_Message_Send_Log extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'message_id',
        'user_id',
        'created',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'push_message_send_logs';

    /**
     * Get list push message send logs.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('push_messages.message', 'push_message'),
            array('users.nickname', 'user_nickname'),
            array('user_profiles.email', 'email')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('push_messages', 'LEFT')
            ->on(self::$_table_name.'.message_id', '=', 'push_messages.id')
            ->join('user_profiles','LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'user_profiles.user_id');

        if (!empty($param['message_id'])) {
            $query->where('message_id', '=', $param['message_id']);
        }
        if (!empty($param['message'])) {
            $query->where('push_messages.message', 'LIKE', "%{$param['message']}%");
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Add info for push message send logs.
     *
     * @author Le Tuan Tu
     * @param $param
     * @throws Exception array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add($param)
    {
        try {
            $log = new self;
            $log->set('user_id', $param['user_id']);
            $log->set('message_id', $param['message_id']);
            if ($log->create()) {
                $log->id = self::cached_object($log)->_original['id'];
                return !empty($log->id) ? $log->id : 0;
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get detail of push message send log.
     *
     * @author Le Tuan Tu
     * @param int $id Input id.
     * @return array Returns the array.
     */
    public static function get_detail($id)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('push_messages.message', 'push_message')
        )
            ->from(self::$_table_name)
            ->join('users', 'LEFT')
            ->on(self::$_table_name.'.user_id', '=', 'users.id')
            ->join('push_messages', 'LEFT')
            ->on(self::$_table_name.'.message_id', '=', 'push_messages.id')
            ->where(self::$_table_name . '.id', '=', $id);
        $data = $query->execute()->as_array();

        return $data ? $data[0] : array();
    }
}

