<?php
/**
 * The production database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=capture',
			'username'   => 'root',
			'password'   => '',
		),
	),
);

//return array(
//	'default' => array(
//		'connection'  => array(
//			'dsn'        => 'mysql:host=sv4.evolable-asia.z-hosts.com;dbname=capture',
//			'username'   => 'journal_dev',
//			'password'   => 'journal_dev',
//		),
//	),
//);
